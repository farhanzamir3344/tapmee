package com.nfc.tapmee.common.models

data class LoginResponseDTO(
    val access_token: String,
    val business_info: BusinessInfo?,
    val user: CustomerDTO?
)