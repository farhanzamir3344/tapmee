package com.nfc.tapmee.common.utils.dialog.profiledialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.nfc.tapmee.R
import com.nfc.tapmee.arch.providers.profile.ProfileProviderFactory
import com.nfc.tapmee.arch.repositories.profile.ProfileRepository
import com.nfc.tapmee.arch.viewmodels.profile.ProfileViewModel
import com.nfc.tapmee.common.models.Profile
import com.nfc.tapmee.common.utils.KeyboardOp
import com.nfc.tapmee.common.utils.alerts.SnackBarUtil
import com.nfc.tapmee.common.utils.dialog.AlertOP
import com.nfc.tapmee.common.utils.dialog.GeneralDialogListener
import com.nfc.tapmee.common.utils.generics.Resource
import com.nfc.tapmee.common.utils.profile.HandleOpenProfile
import com.nfc.tapmee.databinding.DialogSocialProfileBinding


class SocialProfileBottomSheet : BottomSheetDialogFragment() {

    private lateinit var binding: DialogSocialProfileBinding
    private lateinit var viewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_social_profile,
            container,
            false
        )
        return binding.root
    }

    override fun getTheme(): Int {
        return R.style.CustomBottomSheetDialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listeners()
        renderProfile()
        initViewModel()
        handleResponse()
    }

    //region VIEW-MODEL
    private fun initViewModel() {

        val viewModelProviderFactory =
            ProfileProviderFactory(requireActivity().application, ProfileRepository())
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory).get(ProfileViewModel::class.java)
    }
    //endregion


    //region HELPING METHODS
    private fun renderProfile() {

        Glide.with(requireContext())
            .load(profileDTO!!.icon)
            .into(
                binding.ivProfileThumb

            )

        Glide.with(requireContext())
            .load(profileDTO!!.icon)
            .into(
                binding.ivProfileLinkThumb

            )

        binding.tvTitle.text = profileDTO!!.title
        binding.tvProProfileTitle.text = profileDTO!!.title
        binding.tvProfileNameDesc.text = profileDTO!!.title
        val usernameGuide = getString(R.string.msg_profile_username_guide).replace(
            Regex("[#]"),
            profileDTO!!.title.toString()
        )
        binding.tvProfileUsernameGuide.text = usernameGuide


        if (profileDTO!!.profile_code == "contact-card")
            binding.etProfileLink.setText(profileDTO!!.profile_link)
        else if (profileDTO!!.profile_code == "text" ||
            profileDTO!!.profile_code == "call" ||
            profileDTO!!.profile_code == "whatsapp"
        ) {

            binding.etProfileLink.visibility = View.GONE
            binding.profileNumberContainer.visibility = View.VISIBLE
            binding.ccpProfileNumber.fullNumber = profileDTO!!.profile_link_value
        } else
            binding.etProfileLink.setText(profileDTO!!.profile_link_value)

    }
    //endregion

    private fun listeners() {
        binding.ccpProfileNumber.registerPhoneNumberTextView(binding.etLinkProfileNumber)

        binding.ivClearText.setOnClickListener {

            try {
                binding.etLinkProfileNumber.setText("")
                binding.etProfileLink.setText("")
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }

        binding.ivCloseScreen.setOnClickListener {
            mListener?.onProfileUpdate(false)
            dismiss()
        }

        binding.btnOpenProfile.setOnClickListener {
            if (profileDTO != null) {
                HandleOpenProfile.openProfileBaseURL(
                    requireContext(),
                    profileDTO!!
                )
            }

        }

        binding.btnSaveProfile.setOnClickListener {

            var link = ""

            link = if (profileDTO!!.profile_code == "text" ||
                profileDTO!!.profile_code == "call" ||
                profileDTO!!.profile_code == "whatsapp"
            ) {
                binding.ccpProfileNumber.fullNumberWithPlus
            } else
                binding.etProfileLink.text.toString()


            if (link.isNotEmpty()) {
                KeyboardOp.hide(requireActivity(), binding.etProfileLink)

                viewModel.updateSocialProfile(
                    link = link,
                    profileID = profileDTO!!.id.toString(),
                    isBusiness = "no"
                )
            }
        }

        binding.btnDeleteProfile.setOnClickListener {
            AlertOP.showAlert(requireContext(),
                getString(R.string.lbl_delete_link),
                "${getString(R.string.lbl_delete_msg)} ${profileDTO!!.title}?",
                getString(R.string.lbl_dialog_yes),
                getString(R.string.cancel),
                object : GeneralDialogListener {
                    override fun onPositiveClick() {
                        viewModel.deleteSocialProfile(profileID = profileDTO!!.id.toString())
                    }

                    override fun onNegativeClick() {

                    }
                })
        }
    }

    //region HANDLE API RESPONSE
    private fun handleResponse() {


        viewModel.updateSocialProfileLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    response.data?.let { response ->

                        hideProgressBar()

                        //Dismiss Dialog and update Dashboard
                        dismiss()
                        mListener?.onProfileUpdate(true)
                    }

                }
                is Resource.Error -> {
                    hideProgressBar()
                    SnackBarUtil.showSnackBar(requireContext(), response.message!!, true)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })

        viewModel.deleteSocialProfileLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    response.data?.let { response ->

                        hideProgressBar()

                        //Dismiss Dialog and update Dashboard
                        dismiss()
                        mListener?.onProfileUpdate(true)
                    }

                }
                is Resource.Error -> {
                    hideProgressBar()
                    SnackBarUtil.showSnackBar(requireContext(), response.message!!, true)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })
    }
    //endregion


    //region PROGRESS BAR
    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }
    //endregion

    companion object {

        private var mListener: ProfileUpdateListener? = null
        private var profileDTO: Profile? = null

        fun newInstance(
            profileDTO: Profile,
            listener: ProfileUpdateListener?
        ): SocialProfileBottomSheet? {
            this.mListener = listener
            this.profileDTO = profileDTO

            return SocialProfileBottomSheet()
        }
    }
}