package com.nfc.tapmee.common.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class BusinessInfo(
    @PrimaryKey(autoGenerate = false)
    var unique_id: Long = 0,

    val bio: String? = null,
    val created_at: String? = null,
    val created_by: Int? = null,
    @SerializedName("id")
    val businessID: Int? = null,
    val is_public: Int? = null,
    val logo: String? = null,
    val updated_at: String? = null,
    val updated_by: Int? = null,
    val user_id: Int? = null
) : Serializable