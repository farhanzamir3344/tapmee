package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.GenericResponseDto
import com.nfc.tapmee.common.models.UpdateProfileResponseDTO
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface UpdateBasicProfileService {
    @FormUrlEncoded
    @POST("profile")
    suspend fun updateBasicProfile(
        @Field("is_public") isPublic: Int,
        @Field("name") name: String
    ): Response<GenericResponseDto<UpdateProfileResponseDTO>>
}