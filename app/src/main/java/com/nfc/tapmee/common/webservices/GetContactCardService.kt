package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.ContactCardResponseDTO
import com.nfc.tapmee.common.models.GenericResponseDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface GetContactCardService {
    @GET("contact_card")
    suspend fun contactCards(@Query("is_business") isBusiness: String): Response<GenericResponseDto<ContactCardResponseDTO>>
}