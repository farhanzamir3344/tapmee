package com.nfc.tapmee.common.utils.generics

sealed class Resource<T>(
    val data: T? = null,
    val message: String? = null,
    val success: Boolean? = null
) {
    class Success<T>(data: T) : Resource<T>(data)
    class Error<T>(message: String, data: T? = null) : Resource<T>(data, message)
    class Loading<T> : Resource<T>()
}