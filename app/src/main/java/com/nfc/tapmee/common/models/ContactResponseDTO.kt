package com.nfc.tapmee.common.models


import com.google.gson.annotations.SerializedName

data class ContactResponseDTO(
    @SerializedName("user_notes")
    val userNotes: List<UserNote>?
)