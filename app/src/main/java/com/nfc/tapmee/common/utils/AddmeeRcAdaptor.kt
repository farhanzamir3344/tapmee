package com.nfc.tapmee.common.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

//Generic Adaptor only responsible for providing data to the view
class AddmeeRcAdaptor<T>(
    private val context: Context,
    private val itemLayoutManger: ItemLayoutManger<T>
) :
    RecyclerView.Adapter<MViewHolder<T>>() {

    private var itemList = mutableListOf<T>()
    private var recyclerview: RecyclerView? = null


    /**
     * Attaching to recycler view
     * @param RecyclerView
     * @param Recycler view Layout manager
     */
    fun bindRecyclerView(
        recyclerview: RecyclerView,
        layoutManger: RecyclerView.LayoutManager = LinearLayoutManager(
            context,
            RecyclerView.VERTICAL,
            false
        )
    ) {

        this.recyclerview = recyclerview
        this.recyclerview!!.adapter = this
        this.recyclerview!!.layoutManager = layoutManger
    }

    /**
     * Detaching from recyclerView to avoid memory leak
     */
    fun detachRecyclerView() {
        this.recyclerview?.adapter = null
        this.recyclerview = null
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MViewHolder<T> {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(viewType, parent, false)
        return MViewHolder(view, itemLayoutManger)
    }

    override fun onBindViewHolder(holder: MViewHolder<T>, position: Int) {
        holder.bind(getItem(position),position)
    }

    override fun getItemViewType(position: Int): Int {
        return itemLayoutManger.getRowLayoutId(position)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    fun setItems(newItemList: List<T>) {
        if (newItemList.isNullOrEmpty()) return
        itemList.clear()
        itemList.addAll(newItemList)
        this.notifyDataSetChanged()
    }


    fun addItemsAt(newItemList: List<T>, position: Int) {
        if (newItemList.isNullOrEmpty() || position < 0) return
        this.itemList.addAll(position, newItemList)
        this.notifyItemRangeInserted(position, position + newItemList.count())
    }

    fun addItems(items: List<T>) {
        if (items.isNullOrEmpty()) return
        this.itemList.addAll(items)
        this.notifyItemRangeInserted(itemList.indices.last, items.count())
    }

    fun addItem(item: T) {
        this.itemList.add(item)
        this.notifyItemInserted(itemList.indexOf(item))
    }

    fun addItem(item: T, position: Int = -1) {
        this.itemList.add(position, item)
        this.notifyItemInserted(position)
    }

    fun getItem(position: Int): T {
        return this.itemList[position]
    }

    fun getItems(): List<T> {
        return this.itemList
    }

    fun removeItem(position: Int) {
        if (position < 0) return
        this.itemList.removeAt(position)
        this.notifyItemRemoved(position)
    }

    fun removeItem(item: T) {
        this.itemList.remove(item)
        this.notifyItemRemoved(itemList.indexOf(item))
    }
}

class MViewHolder<T>(
    private val view: View,
    private val layoutManger: ItemLayoutManger<T>
) : RecyclerView.ViewHolder(view) {

    internal fun bind(item: T, position: Int) {
        // The view position is used to retrieved the data w.r.t position  of the view
        // Adaptor position is not used to make the code more readable
        view.tag = position
        layoutManger.bindRowView(view, item)
    }
}


interface ItemLayoutManger<T> {
    fun getRowLayoutId(position: Int): Int
    fun bindRowView(view: View, item: T)
}