package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.CustomerDTO
import com.nfc.tapmee.common.models.GenericResponseDto
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST


interface ForgotPasswordService {
    @Headers("No-Authorization: true")
    @FormUrlEncoded
    @POST("send_pincode")
    suspend fun sendOtp(@Field("email") email: String): Response<GenericResponseDto<CustomerDTO>>
}