package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.SuccessDTO
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface UpdateProfileBanner {
    @Multipart
    @POST("update_banner")
    suspend fun uploadBanner(
        @Part file: MultipartBody.Part
    ): Response<SuccessDTO>
}