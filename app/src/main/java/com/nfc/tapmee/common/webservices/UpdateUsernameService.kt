package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.GenericResponseDto
import com.nfc.tapmee.common.models.UsernameResponseDTO
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST


interface UpdateUsernameService {
    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("update_username")
    suspend fun updateUsername(@Field("username") username: String): Response<GenericResponseDto<UsernameResponseDTO>>
}