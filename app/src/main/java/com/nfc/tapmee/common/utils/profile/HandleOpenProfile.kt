package com.nfc.tapmee.common.utils.profile

import android.app.DownloadManager
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Context.DOWNLOAD_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import com.nfc.tapmee.common.models.Profile
import com.nfc.tapmee.common.webview.TITLE
import com.nfc.tapmee.common.webview.WEB_URL
import com.nfc.tapmee.common.webview.WebActivity


object HandleOpenProfile {
    private const val EMAIL = "email"
    private const val INSTAGRAM = "instagram"
    private const val SNAPCHAT = "snapchat"
    private const val TIKTOK = "tiktok"
    private const val FACEBOOK = "facebook"
    private const val LINKEDIN = "linkedin"
    private const val TWITTER = "twitter"
    private const val YOUTUBE = "youtube"
    private const val TEXT = "text"
    private const val CALL = "call"
    private const val ADDRESS = "address"
    private const val WHATSAPP = "whatsapp"
    private const val TELEGRAM = "telegram"
    private const val CONTACT = "contact-card"

    fun openProfileBaseURL(context: Context, profile: Profile) {
        when (profile.profile_code) {
            EMAIL -> {
                val mIntent = Intent(Intent.ACTION_SEND)
                mIntent.data = Uri.parse("mailto:")
                mIntent.type = "text/plain"
                mIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(profile.profile_link_value))

                try {
                    //start email intent
                    startActivity(
                        context,
                        Intent.createChooser(mIntent, "Choose Email Client..."),
                        null
                    )
                } catch (e: Exception) {
                    //if any thing goes wrong for example no email client application or any exception
                    //get and show exception message
                    Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
                }
            }
            INSTAGRAM -> {
                val uri = Uri.parse("http://instagram.com/_u/${profile.profile_link_value}")
                val insta = Intent(Intent.ACTION_VIEW, uri)
                insta.setPackage("com.instagram.android")

                try {
                    startActivity(context, insta, null)
                } catch (e: ActivityNotFoundException) {

                    gotoWeb(context, profile.title.toString(), uri.toString())
                }
            }
            SNAPCHAT -> {
                try {
                    val intent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse(profile.profile_link)
                    )
                    intent.setPackage("com.snapchat.android")
                    startActivity(context, intent, null)
                } catch (e: ActivityNotFoundException) {

                    gotoWeb(context, profile.title!!, profile.profile_link.toString())

                }
            }
            TIKTOK -> {
                try {
                    val uri: Uri = Uri.parse(profile.profile_link)
                    val intent = Intent(Intent.ACTION_VIEW, uri)

                    intent.setPackage("com.zhiliaoapp.musically")
                    startActivity(context, intent, null)
                } catch (e: ActivityNotFoundException) {
                    gotoWeb(context, profile.title.toString(), profile.profile_link.toString())

                }

            }
            FACEBOOK -> {
                gotoWeb(context, profile.title.toString(), profile.profile_link.toString())
            }
            LINKEDIN -> {

                gotoWeb(context, profile.title.toString(), profile.profile_link.toString())

            }
            TWITTER -> {
                try {
                    val uri: Uri = Uri.parse(profile.profile_link)
                    val intent = Intent(Intent.ACTION_VIEW, uri)

                    intent.setPackage("com.twitter.android")
                    startActivity(context, intent, null)
                } catch (e: ActivityNotFoundException) {
                    gotoWeb(context, profile.title.toString(), profile.profile_link.toString())

                }
            }
            YOUTUBE -> {

                try {

                    val uri: Uri = Uri.parse(profile.profile_link)
                    val intent = Intent(Intent.ACTION_VIEW, uri)

                    intent.setPackage("com.google.android.youtube")
                    startActivity(context, intent, null)
                } catch (e: ActivityNotFoundException) {

                    gotoWeb(context, profile.title.toString(), profile.profile_link.toString())

                }
            }
            TEXT -> {
                try {
                    val smsIntent = Intent(Intent.ACTION_SENDTO)
                    smsIntent.data =
                        Uri.parse("smsto:${profile.profile_link_value!!.trim().replace("+", "")}")
                    smsIntent.addCategory(Intent.CATEGORY_DEFAULT)
                    startActivity(context, smsIntent, null)
                } catch (e: Exception) {
                    // Handle Error
                    e.printStackTrace()
                }
            }
            CALL -> {
                try {
                    val intent = Intent(Intent.ACTION_DIAL)
                    intent.data =
                        Uri.parse("tel:${profile.profile_link_value!!.trim().replace("+", "")}")
                    startActivity(context, intent, null)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            ADDRESS -> {

                try {
                    val pm: PackageManager = context.packageManager
                    pm.getPackageInfo("com.google.android.apps.maps", PackageManager.GET_ACTIVITIES)
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(profile.profile_link!!)
                    startActivity(context, intent, null)
                } catch (e: PackageManager.NameNotFoundException) {

                    gotoWeb(
                        context,
                        profile.title!!,
                        profile.profile_link!!
                    )
                }

//                try {
//                    val gmmIntentUri =
//                        Uri.parse(profile.profile_link)
//                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
//                    mapIntent.setPackage("com.google.android.apps.maps")
//                    if (mapIntent.resolveActivity(context.packageManager) != null) {
//                        startActivity(context, mapIntent, null)
//                    }
//                } catch (ex: Exception) {
//                    ex.printStackTrace()
//                }
            }
            WHATSAPP -> {

                try {
                    val pm: PackageManager = context.packageManager
                    pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES)
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(profile.profile_link!!.trim().replace("+", ""))
                    startActivity(context, intent, null)
                } catch (e: PackageManager.NameNotFoundException) {

                    gotoWeb(
                        context,
                        profile.title!!,
                        profile.profile_link!!.trim().replace("+", "")
                    )
                }
            }
            TELEGRAM -> {
                val url = "http://t.me/${profile.profile_link_value!!.trim()}"
                try {

                    val pm: PackageManager = context.packageManager
                    pm.getPackageInfo("org.telegram.messenger", PackageManager.GET_ACTIVITIES)
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(url)
                    startActivity(context, intent, null)
                } catch (e: PackageManager.NameNotFoundException) {
                    gotoWeb(context, profile.title!!, Uri.parse(url).toString())
                }
            }
            CONTACT -> {
//                val browserIntent = Intent(
//                    Intent.ACTION_VIEW,
//                    Uri.parse(profile.profile_link)
//                )
//                startActivity(context, browserIntent, null)

                val downloadmanager = context.getSystemService(DOWNLOAD_SERVICE) as DownloadManager?
                val uri =
                    Uri.parse(profile.profile_link)

                val request = DownloadManager.Request(uri)
                request.setTitle(profile.name)
                request.setDescription("Downloading")
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                downloadmanager!!.enqueue(request)
            }

            else -> {

                gotoWeb(
                    context,
                    profile.title.toString(),
                    Uri.parse(profile.profile_link).toString()
                )
            }
        }
    }

    fun openProfile(link: String, context: Context, title: String) {
        when (title.toLowerCase()) {
            EMAIL -> {
                val mIntent = Intent(Intent.ACTION_SEND)
                mIntent.data = Uri.parse("mailto:")
                mIntent.type = "text/plain"
                mIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(link))

                try {
                    //start email intent
                    startActivity(
                        context,
                        Intent.createChooser(mIntent, "Choose Email Client..."),
                        null
                    )
                } catch (e: Exception) {
                    //if any thing goes wrong for example no email client application or any exception
                    //get and show exception message
                    Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
                }
            }
            INSTAGRAM -> {
                val uri = Uri.parse("http://instagram.com/_u/$link")
                val insta = Intent(Intent.ACTION_VIEW, uri)
                insta.setPackage("com.instagram.android")

                try {
                    startActivity(context, insta, null)
                } catch (e: ActivityNotFoundException) {

                    gotoWeb(context, title, "http://instagram.com/_u/$link")
                }
            }
            SNAPCHAT -> {
                try {
                    val intent = Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://snapchat.com/add/$link")
                    )
                    intent.setPackage("com.snapchat.android")
                    startActivity(context, intent, null)
                } catch (e: ActivityNotFoundException) {

                    gotoWeb(context, title, "https://snapchat.com/add/$link")

                }
            }
            TIKTOK -> {
                try {
                    val uri: Uri = Uri.parse("https://www.tiktok.com/@$link")
                    val intent = Intent(Intent.ACTION_VIEW, uri)

                    intent.setPackage("com.zhiliaoapp.musically")
                    startActivity(context, intent, null)
                } catch (e: ActivityNotFoundException) {
                    gotoWeb(context, title, "https://www.tiktok.com/@$link")

                }

            }
            FACEBOOK -> {
                gotoWeb(context, title, link)
            }
            LINKEDIN -> {

                gotoWeb(context, title, link)

            }
            TWITTER -> {
                try {
                    val uri: Uri = Uri.parse("https://twitter.com/$link")
                    val intent = Intent(Intent.ACTION_VIEW, uri)

                    intent.setPackage("com.twitter.android")
                    startActivity(context, intent, null)
                } catch (e: ActivityNotFoundException) {

                    gotoWeb(context, title, "https://twitter.com/$link")

                }
            }
            YOUTUBE -> {

                val urlOrChannelName = if (link.contains("youtube.com")) {
                    link
                } else {
                    "https://www.youtube.com/$link"
                }

                try {

                    val uri: Uri = Uri.parse(urlOrChannelName)
                    val intent = Intent(Intent.ACTION_VIEW, uri)

                    intent.setPackage("com.google.android.youtube")
                    startActivity(context, intent, null)
                } catch (e: ActivityNotFoundException) {

                    gotoWeb(context, title, urlOrChannelName)
//                    startActivity(
//                        context,
//                        Intent(
//                            Intent.ACTION_VIEW,
//                            Uri.parse(urlOrChannelName)
//                        ),
//                        null
//                    )
                }
            }
            TEXT -> {
                try {
                    val smsIntent = Intent(Intent.ACTION_SENDTO)
                    smsIntent.data =
                        Uri.parse("smsto:" + link.trim())
                    smsIntent.addCategory(Intent.CATEGORY_DEFAULT)
                    startActivity(context, smsIntent, null)
                } catch (e: Exception) {
                    // Handle Error
                    e.printStackTrace()
                }
            }
            CALL -> {
                try {
                    val intent = Intent(Intent.ACTION_DIAL)
                    intent.data = Uri.parse("tel:${link.trim()}")
                    startActivity(context, intent, null)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            ADDRESS -> {
                try {
                    val gmmIntentUri =
                        Uri.parse("https://www.google.com/maps/search/?api=1&query=$link")
                    val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                    mapIntent.setPackage("com.google.android.apps.maps")
                    if (mapIntent.resolveActivity(context.packageManager) != null) {
                        startActivity(context, mapIntent, null)
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }
            }
            WHATSAPP -> {
                val url = "https://api.whatsapp.com/send?phone=${link.trim()}"
                try {
                    val pm: PackageManager = context.packageManager
                    pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES)
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(url)
                    startActivity(context, intent, null)
                } catch (e: PackageManager.NameNotFoundException) {
                    gotoWeb(context, title, url)
                }
            }
            TELEGRAM -> {
                val url = "https://telegram.me/${link.trim()}"
                try {

                    val pm: PackageManager = context.packageManager
                    pm.getPackageInfo("org.telegram.messenger", PackageManager.GET_ACTIVITIES)
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(url)
                    startActivity(context, intent, null)
                } catch (e: PackageManager.NameNotFoundException) {
                    gotoWeb(context, title, url)
                }
            }
            CONTACT -> {
//                val browserIntent = Intent(
//                    Intent.ACTION_VIEW,
//                    Uri.parse(link)
//                )
//                startActivity(context, browserIntent, null)

                val downloadmanager = context.getSystemService(DOWNLOAD_SERVICE) as DownloadManager?
                val uri =
                    Uri.parse(link)

                val request = DownloadManager.Request(uri)
                request.setTitle(title)
                request.setDescription("Downloading")
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                downloadmanager!!.enqueue(request)
            }


            else -> {

                gotoWeb(context, title, link)
            }
        }
    }


    private fun isIntentAvailable(ctx: Context, intent: Intent): Boolean {
        val packageManager = ctx.packageManager
        val list = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
        return list.size > 0
    }

    private fun gotoWeb(ctx: Context, title: String, link: String) {

        startActivity(
            ctx,
            Intent(
                ctx,
                WebActivity::class.java
            ).putExtra(WEB_URL, link).putExtra(
                TITLE, title
            ), null
        )
    }
}