package com.nfc.tapmee.common.models

data class UpdateProfileResponseDTO(
    val user: CustomerDTO? = null
)
