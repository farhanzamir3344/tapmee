package com.nfc.tapmee.common.models

data class ErrorDto(
    val errors: ArrayList<ApiErrorDTO>,
    val code: Int,
    val success: Boolean,
    val message: String
)
