package com.nfc.tapmee.common.utils.viewpager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager

class AddmeePagerAdaptor(fragmentManager: FragmentManager, val pagerCallBack: OnPageScrolled) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT),
    ViewPager.OnPageChangeListener {

    private var mFragmentList: ArrayList<Fragment> = ArrayList()
    private var mFragmentTitleList: ArrayList<String> = ArrayList()


    fun addFragment(
        fragment: Fragment,
        title: String?
    ) {
        mFragmentList.add(fragment)
        title?.let { mFragmentTitleList.add(it) }
    }

    override fun getItem(position: Int): Fragment {
        return mFragmentList[position]
    }

    override fun getCount(): Int {
        return mFragmentList.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mFragmentTitleList[position]
    }


    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        var currentPoSition = position
        if (positionOffset > 0.5f)
            currentPoSition += 1
        pagerCallBack.onPageScrolled(currentPoSition)

    }

    override fun onPageSelected(position: Int) {

    }
}

interface OnPageScrolled {
    fun onPageScrolled(position: Int)
}