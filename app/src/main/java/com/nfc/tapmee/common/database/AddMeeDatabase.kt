package com.nfc.tapmee.common.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.nfc.tapmee.common.database.dao.BusinessDAO
import com.nfc.tapmee.common.database.dao.CustomerDAO
import com.nfc.tapmee.common.models.BusinessInfo
import com.nfc.tapmee.common.models.CustomerDTO

@Database(entities = [CustomerDTO::class, BusinessInfo::class], version = 1)
abstract class AddMeeDatabase : RoomDatabase() {

    abstract val customerDAO: CustomerDAO
    abstract val businessInfoDAO: BusinessDAO

    companion object {
        @Volatile
        private var instance: AddMeeDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: createDatabase(context).also { instance = it }
        }

        private fun createDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AddMeeDatabase::class.java,
                "addmee_db"
            ).fallbackToDestructiveMigration().build()
    }


}