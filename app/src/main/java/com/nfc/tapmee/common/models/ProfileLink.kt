package com.nfc.tapmee.common.models


import com.google.gson.annotations.SerializedName

data class ProfileLink(
    @SerializedName("created_at")
    val createdAt: String?,
    @SerializedName("created_by")
    val createdBy: Int?,
    @SerializedName("file_image")
    val fileImage: String?,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("is_business")
    val isBusiness: Int?,
    @SerializedName("is_direct")
    val isDirect: Int?,
    @SerializedName("profile_code")
    val profileCode: String?,
    @SerializedName("profile_link")
    val profileLink: String?,
    @SerializedName("sequence")
    val sequence: Int?,
    @SerializedName("status")
    val status: Int?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("updated_at")
    val updatedAt: String?,
    @SerializedName("updated_by")
    val updatedBy: Int?,
    @SerializedName("user_id")
    val userId: Int?
)