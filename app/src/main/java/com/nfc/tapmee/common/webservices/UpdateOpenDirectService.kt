package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.GenericResponseDto
import com.nfc.tapmee.common.models.UpdateOpenDirectResponseDTO
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface UpdateOpenDirectService {
    @FormUrlEncoded
    @POST("open_direct")
    suspend fun updateOpenDirect(
        @Field("my_profile_id") profileID: Int,
        @Field("is_direct") isDirect: Int
    ): Response<GenericResponseDto<UpdateOpenDirectResponseDTO>>
}