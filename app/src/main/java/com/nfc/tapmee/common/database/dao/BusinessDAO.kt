package com.nfc.tapmee.common.database.dao

import androidx.room.*
import com.nfc.tapmee.common.models.BusinessInfo

@Dao
interface BusinessDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveBusinessDto(businessInfo: BusinessInfo): Long

    @Update(onConflict = OnConflictStrategy.IGNORE)
    fun updateBusinessInfoDTO(businessInfo: BusinessInfo)

    @Query("Select * from BusinessInfo")
    suspend fun getBusinessInfo(): BusinessInfo
}