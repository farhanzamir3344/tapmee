package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.GenericResponseDto
import com.nfc.tapmee.common.models.ProfileResponseDTO
import retrofit2.Response
import retrofit2.http.GET

interface AllProfilesService {
    @GET("list_all_profiles")
    suspend fun getAllProfiles(): Response<GenericResponseDto<ProfileResponseDTO>>
}