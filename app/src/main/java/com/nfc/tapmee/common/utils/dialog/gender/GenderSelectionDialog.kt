package com.nfc.tapmee.common.utils.dialog.gender

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.nfc.tapmee.R
import com.nfc.tapmee.databinding.DialogGenderSelectionBinding

class GenderSelectionDialog : BottomSheetDialogFragment() {

    private lateinit var binding: DialogGenderSelectionBinding

    // region LIFECYCLE

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_gender_selection,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog!!.setCancelable(false)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))



        when (selectedGender) {
            getString(R.string.lbl_option_male) -> {
                binding.genderSelection.check(R.id.choiceMale)
            }
            getString(R.string.lbl_option_female) -> {
                binding.genderSelection.check(R.id.choiceFemale)
            }
            getString(R.string.lbl_option_prefer_not_to_say) -> {
                binding.genderSelection.check(R.id.choiceNoSay)
            }
            getString(R.string.lbl_option_custom) -> {
                binding.genderSelection.check(R.id.choiceCustom)
            }
            else -> {

            }
        }

        binding.btnDone.setOnClickListener {

            when (binding.genderSelection.checkedId) {
                R.id.choiceMale -> {
                    mListener?.onGenderSelected(getString(R.string.lbl_option_male))
                }
                R.id.choiceFemale -> {
                    mListener?.onGenderSelected(getString(R.string.lbl_option_female))
                }
                R.id.choiceNoSay -> {
                    mListener?.onGenderSelected(getString(R.string.lbl_option_prefer_not_to_say))
                }
                R.id.choiceCustom -> {
                    mListener?.onGenderSelected(getString(R.string.lbl_option_custom))
                }
            }

            //dismiss dialog
            dialog!!.dismiss()
        }

    }


    // endregion


    companion object {

        private var mListener: GenderListener? = null
        private var selectedGender: String? = null

        fun newInstance(
            selectedGender: String,
            listener: GenderListener?
        ): GenderSelectionDialog? {
            this.selectedGender = selectedGender
            this.mListener = listener
            return GenderSelectionDialog()
        }
    }
}