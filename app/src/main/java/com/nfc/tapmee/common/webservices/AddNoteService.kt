package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.SuccessDTO
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AddNoteService {
    @FormUrlEncoded
    @POST("user_note")
    suspend fun addNote(
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("phone_no") phoneNo: String,
        @Field("note") note: String,
        @Field("user_id") userId: Int
    ): Response<SuccessDTO>
}