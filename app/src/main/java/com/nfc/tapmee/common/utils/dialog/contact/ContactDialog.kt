package com.nfc.tapmee.common.utils.dialog.contact

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nfc.tapmee.R
import com.nfc.tapmee.arch.providers.contact.ContactProviderFactory
import com.nfc.tapmee.arch.repositories.contact.ContactRepository
import com.nfc.tapmee.arch.viewmodels.contact.ContactViewModel
import com.nfc.tapmee.common.models.CustomerDTO
import com.nfc.tapmee.common.models.MyProfilesResponseDTO
import com.nfc.tapmee.common.utils.alerts.SnackBarUtil
import com.nfc.tapmee.common.utils.dialog.AlertOP
import com.nfc.tapmee.common.utils.dialog.GeneralDialogListener
import com.nfc.tapmee.common.utils.generics.Resource
import com.nfc.tapmee.databinding.DialogContactBinding
import kotlinx.android.synthetic.main.dialog_contact.*

class ContactDialog : DialogFragment() {

    private lateinit var binding: DialogContactBinding
    private lateinit var viewModel: ContactViewModel

    // region LIFECYCLE

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_contact,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViewModel()
        handleResponse()

        dialog!!.setCancelable(false)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        setValues()

        binding.btnConnect.setOnClickListener {

            if (validate()) {

                val name = binding.etName.text.toString()
                val email = binding.etEmail.text.toString()
                val number = binding.etNumber.text.toString()
                val note = binding.etMessage.text.toString()

                viewModel.addNote(
                    userId = contactPersonID!!,
                    name = name,
                    email = email,
                    phoneNo = number,
                    note = note
                )
            }

        }

        binding.btnNegative.setOnClickListener {

            //dismiss dialog
            dialog!!.dismiss()
        }
    }

    // endregion

    //region VIEW-MODEL
    private fun initViewModel() {

        val viewModelProviderFactory =
            ContactProviderFactory(requireActivity().application, ContactRepository())
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory).get(ContactViewModel::class.java)


    }
    //endregion

    //region HELPING METHODS
    private fun validate(): Boolean {

        var isValid = true

        val name = binding.etName.text.toString()
        val email = binding.etEmail.text.toString()
        val number = binding.etNumber.text.toString()
        val note = binding.etMessage.text.toString()

        if (name.isEmpty()) {
            isValid = false
            binding.tilName.error = "Enter your Name"
        } else {
            binding.tilName.error = ""
        }

        if (email.isEmpty()) {
            isValid = false
            binding.tilEmail.error = "Enter your Email"
        } else {
            binding.tilEmail.error = ""
        }

        if (number.isEmpty()) {
            isValid = false
            binding.tilNumber.error = "Enter your Number"
        } else {
            binding.tilNumber.error = ""
        }

        if (note.isEmpty()) {
            isValid = false
            binding.tilMessage.error = "Enter your Note"
        } else {
            binding.tilMessage.error = ""
        }

        return isValid
    }

    private fun setValues() {

        try {

            dialog!!.setCancelable(false)
            dialog!!.setCanceledOnTouchOutside(false)

            binding.tvHeader.text =
                "${requireActivity().getString(R.string.lbl_connect_with)} $personName"
            binding.tvSubtitle.text =
                "${requireActivity().getString(R.string.lbl_connect_subtitle)} $personName"

            if (customerDTO != null) {
                binding.etName.setText(customerDTO!!.name)
                binding.etEmail.setText(customerDTO!!.email)

            }

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    //endregion

    //region HANDLE RESPONSE
    private fun handleResponse() {
        viewModel.addNoteLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    response.data?.let { response ->

                        hideProgressBar()

                        AlertOP.showResponseAlertOK(
                            requireContext(),
                            getString(R.string.lbl_success),
                            getString(R.string.lbl_msg_note_success),
                            object : GeneralDialogListener {
                                override fun onPositiveClick() {
                                    //dismiss dialog
                                    dialog!!.dismiss()
                                }

                                override fun onNegativeClick() {

                                }
                            })

                    }

                }
                is Resource.Error -> {
                    hideProgressBar()
                    SnackBarUtil.showSnackBar(requireContext(), response.message!!, true)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })
    }
    //endregion

    //region PROGRESS BAR
    private fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }
    //endregion

    companion object {

        private var customerDTO: CustomerDTO? = null
        private var personName: String? = null
        private var contactPersonID: Int? = null

        fun newInstance(
            customerDTO: CustomerDTO?,
            contactPersonName: String,
            otherPersonID: Int
        ): ContactDialog? {
            this.customerDTO = customerDTO
            this.personName = contactPersonName
            this.contactPersonID = otherPersonID

            return ContactDialog()
        }
    }
}