package com.nfc.tapmee.common.preferences

import com.nfc.tapmee.common.utils.Constants.IS_DIRECT_ON
import com.nfc.tapmee.common.utils.Constants.IS_LOGIN_CUSTOMER
import com.nfc.tapmee.common.utils.Constants.IS_NEW_USER
import com.nfc.tapmee.common.utils.Constants.IS_REGISTER_CUSTOMER
import com.nfc.tapmee.common.utils.Constants.TOKEN

class PreferenceUtils {
    companion object {

        @JvmStatic
        fun saveAuthToken(token: String) {
            SharedPreferences.getInstance().save(TOKEN, token)
        }

        @JvmStatic
        fun getAuthToken(): String? {
            return SharedPreferences.getInstance().read(TOKEN, "")
        }

        @JvmStatic
        fun saveIsRegisterCustomer() {
            SharedPreferences.getInstance().save(IS_REGISTER_CUSTOMER, true)
        }


        @JvmStatic
        fun isRegisterCustomer(): Boolean {
            return SharedPreferences.getInstance().read(IS_REGISTER_CUSTOMER, false)!!
        }

        @JvmStatic
        fun saveIsLoginCustomer() {
            SharedPreferences.getInstance().save(IS_LOGIN_CUSTOMER, true)
        }


        @JvmStatic
        fun isLoginCustomer(): Boolean {
            return SharedPreferences.getInstance().read(IS_LOGIN_CUSTOMER, false)!!
        }

        @JvmStatic
        fun saveIsDirectOn(isDirectOn: Boolean) {
            SharedPreferences.getInstance().save(IS_DIRECT_ON, isDirectOn)
        }


        @JvmStatic
        fun isDirectOn(): Boolean {
            return SharedPreferences.getInstance().read(IS_DIRECT_ON, false)!!
        }

        @JvmStatic
        fun saveNewUser(isNewUser: Boolean) {
            SharedPreferences.getInstance().save(IS_NEW_USER, isNewUser)
        }

        @JvmStatic
        fun isNewUser(): Boolean {
            return SharedPreferences.getInstance().read(IS_NEW_USER, false)!!
        }
    }

}
