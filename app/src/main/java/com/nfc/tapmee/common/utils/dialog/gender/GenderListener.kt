package com.nfc.tapmee.common.utils.dialog.gender

interface GenderListener {
    fun onGenderSelected(gender: String)
}