package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.GenericResponseDto
import com.nfc.tapmee.common.models.LoginResponseDTO
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST

interface SocialLoginService {
    @Headers("No-Authorization: true")
    @FormUrlEncoded
    @POST("social_login")
    suspend fun doSocialLogin(
        @Field("email") email: String,
        @Field("provider") provider: String,
        @Field("provider_id") providerID: String,
        @Field("fcm_token") fcm_token: String = "fcm_token"
    ): Response<GenericResponseDto<LoginResponseDTO>>
}