package com.nfc.tapmee.common.models

data class UserDTO(
    val created_at: String,
    val email: String,
    val fcm_token: String,
    val id: Int,
    val status: Int,
    val updated_at: String
)