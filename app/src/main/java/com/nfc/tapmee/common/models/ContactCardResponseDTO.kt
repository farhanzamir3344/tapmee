package com.nfc.tapmee.common.models


import com.google.gson.annotations.SerializedName

data class ContactCardResponseDTO(
    @SerializedName("contact_card")
    val contactCard: List<ContactCard>?
)