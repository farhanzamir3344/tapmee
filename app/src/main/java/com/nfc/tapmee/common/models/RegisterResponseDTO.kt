package com.nfc.tapmee.common.models


data class RegisterResponseDTO(

    val `data`: RegisterDTO,
    val message: String,
    val success: Boolean
)