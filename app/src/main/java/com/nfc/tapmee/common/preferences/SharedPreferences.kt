package com.nfc.tapmee.common.preferences

import android.content.Context
import com.nfc.tapmee.common.utils.Constants.APP_PREFERENCE_NAME
import com.nfc.tapmee.common.utils.Constants.PRIVATE_MODE
import java.util.*

class SharedPreferences
/**
 * private constructor
 *
 * @param context [Context]
 */
private constructor(context: Context) {

    private val editor: android.content.SharedPreferences.Editor
    val sharedPreferences: android.content.SharedPreferences = context.getSharedPreferences(
        (APP_PREFERENCE_NAME).lowercase(Locale.ROOT), Context.MODE_PRIVATE
    )

    init {
        editor = sharedPreferences.edit()
        editor.apply()
    }

    /**
     * Clear all values in [SharedPreferences]
     */
    fun clearPreferences() {
        editor.clear()
        editor.commit()
    }

    /**
     * Read string value from [SharedPreferences]
     *
     * @param valueKey     key
     * @param valueDefault default value
     * @return string value
     */
    fun read(valueKey: String, valueDefault: String?): String? {
        return sharedPreferences.getString(valueKey, valueDefault)
    }

    /**
     * Save string value in [SharedPreferences]
     *
     * @param valueKey key
     * @param value    default value
     */
    fun save(valueKey: String, value: String) {
        editor.putString(valueKey, value)
        editor.commit()
    }

    /**
     * Read integer value from [SharedPreferences]
     *
     * @param valueKey     key
     * @param valueDefault default value
     * @return integer value
     */
    fun read(valueKey: String, valueDefault: Int): Int? {
        return sharedPreferences.getInt(valueKey, valueDefault)
    }

    /**
     * Save integer value in [SharedPreferences]
     *
     * @param valueKey key
     * @param value    default value
     */
    fun save(valueKey: String, value: Int) {
        editor.putInt(valueKey, value)
        editor.commit()
    }

    /**
     * Save boolean value in [SharedPreferences]
     *
     * @param valueKey     key
     * @param valueDefault default value
     */
    fun read(valueKey: String, valueDefault: Boolean): Boolean? {
        return sharedPreferences.getBoolean(valueKey, valueDefault)
    }

    /**
     * Save boolean value in [SharedPreferences]
     *
     * @param valueKey key
     * @param value    default value
     */
    fun save(valueKey: String, value: Boolean) {
        editor.putBoolean(valueKey, value)
        editor.commit()
    }

    /**
     * Save long value in [SharedPreferences]
     *
     * @param valueKey     key
     * @param valueDefault default value
     */
    fun read(valueKey: String, valueDefault: Long): Long? {
        return sharedPreferences.getLong(valueKey, valueDefault)
    }

    /**
     * Save long value in [SharedPreferences]
     *
     * @param valueKey key
     * @param value    default value
     */
    fun save(valueKey: String, value: Long) {
        editor.putLong(valueKey, value)
        editor.commit()
    }


    /**
     * Read float value from [SharedPreferences]
     *
     * @param valueKey     key
     * @param valueDefault default value
     */
    fun read(valueKey: String, valueDefault: Float): Float? {
        return sharedPreferences.getFloat(valueKey, valueDefault)
    }

    /**
     * Save float value in [SharedPreferences]
     *
     * @param valueKey key
     * @param value    default value
     */
    fun save(valueKey: String, value: Float) {
        editor.putFloat(valueKey, value)
        editor.commit()
    }

    fun delete(valueKey: String) {
        if (sharedPreferences.contains(valueKey))
            editor.remove(valueKey).apply()
    }

    companion object {

        private var INSTANCE: SharedPreferences? = null

        /**
         * There must be only one INSTANCE of [SharedPreferences] or [SharedPreferences]
         * If you will try to create another INSTANCE exception arises
         *
         * @param context [Context]
         */

        @JvmStatic
        fun initialize(context: Context) {
            synchronized(android.content.SharedPreferences::class.java) {
                if (INSTANCE == null)
                    INSTANCE = SharedPreferences(context)
                else
                    throw IllegalStateException("Singleton INSTANCE already exists.")
            }
        }

        @JvmStatic
        fun getInstance(): SharedPreferences {
            return INSTANCE!!
        }
    }
}