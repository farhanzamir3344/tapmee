package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.SuccessDTO
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface UpdateSocialProfileService {
    @FormUrlEncoded
    @POST("update_profile")
    suspend fun updateSocialProfile(
        @Field("my_profile_id") profileID: String,
        @Field("profile_link") profileLink: String,
        @Field("is_business") businessProfile: String
    ): Response<SuccessDTO>
}