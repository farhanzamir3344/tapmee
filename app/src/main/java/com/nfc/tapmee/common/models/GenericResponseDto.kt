package com.nfc.tapmee.common.models

import com.google.gson.annotations.SerializedName

data class GenericResponseDto<T>(
    @SerializedName("success")
    var success: Boolean? = null,

    @SerializedName("message")
    var message: String? = null,

    @SerializedName("data")
    var data: T? = null
)
