package com.nfc.tapmee.common.utils.dialog.profiledialog

interface ProfileUpdateListener {
    fun onProfileUpdate(isUpdateRequired: Boolean)
}