package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.GenericResponseDto
import com.nfc.tapmee.common.models.MyProfilesResponseDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MyProfilesService {
    @GET("my_profiles")
    suspend fun getMyProfiles(
        @Query("type") type: String
    ): Response<GenericResponseDto<MyProfilesResponseDTO>>
}