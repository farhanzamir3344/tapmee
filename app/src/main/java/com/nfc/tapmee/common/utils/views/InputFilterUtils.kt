package com.nfc.tapmee.common.utils.views

import android.text.InputFilter
import android.widget.EditText
import java.util.regex.Matcher
import java.util.regex.Pattern

object InputFilterUtils {
    /**
     * Prevent EditText from entering Special characters
     *
     * @param editText
     */
    fun setEditTextInhibitInputSpaChat(editText: EditText) {

        val filterSpecialCharacter =
            InputFilter { charSequence, i, i1, spanned, i2, i3 ->
//                val speChat = "[` ~!@#_$%^&*()+=|{}':;,\\[\\].<>/?！￥…‘；：”“’。，、？]"
                val speChat = "[`~!@#$%^&*()+=|{}':;,\\[\\].<>/?！￥…（）—-【】‘；：”“’。，、？_]"
                val pattern: Pattern = Pattern.compile(speChat)
                val matcher: Matcher = pattern.matcher(charSequence.toString())
                if (matcher.find() || charSequence.equals(" ")) "" else null
            }


        editText.filters = arrayOf(filterSpecialCharacter)


    }
}