package com.nfc.tapmee.common.models

import com.google.gson.annotations.SerializedName

data class SuccessDTO(
    @SerializedName("success")
    var success: Boolean? = null,

    @SerializedName("message")
    var message: String? = null
)
