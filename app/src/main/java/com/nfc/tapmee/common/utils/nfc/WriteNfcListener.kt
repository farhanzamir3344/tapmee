package com.nfc.tapmee.common.utils.nfc

interface WriteNfcListener {
    fun onWriteSuccess()
    fun onWriteFailed()
}