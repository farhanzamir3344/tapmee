package com.nfc.tapmee.common.utils.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.nfc.tapmee.R
import com.nfc.tapmee.databinding.DialogGeneralAlertBinding

class GeneralAlertDialogFragment : DialogFragment() {

    private lateinit var binding: DialogGeneralAlertBinding

    // region LIFECYCLE

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_general_alert,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog!!.setCancelable(false)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        setValues()

        binding.btnPositive.setOnClickListener {

            mListener?.onPositiveClick()

            //dismiss dialog
            dialog!!.dismiss()
        }

        binding.btnNegative.setOnClickListener {
            mListener?.onNegativeClick()
            //dismiss dialog
            dialog!!.dismiss()
        }
    }


    // endregion

    //region HELPING METHODS

    private fun setValues() {

        try {

            dialog!!.setCancelable(false)
            dialog!!.setCanceledOnTouchOutside(false)

            if (!message.isNullOrEmpty()) {
                binding.tvMessage.text = message
            } else binding.tvMessage.visibility = View.GONE


            if (!title.isNullOrEmpty()) {
                binding.tvHeader.text = title
            }

            if (!positiveText.isNullOrEmpty()) {
                binding.btnPositive.text = positiveText
            } else binding.btnPositive.visibility = View.GONE

            if (!negativeText.isNullOrEmpty()) {
                binding.btnNegative.text = negativeText
            } else binding.btnNegative.visibility = View.GONE

        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    //endregion

    companion object {

        private var mListener: GeneralDialogListener? = null
        private var message: String? = null
        private var title: String? = null
        private var positiveText: String? = null
        private var negativeText: String? = null


        fun newInstance(
            title: String?,
            message: String?,
            positiveBtnText: String?,
            negativeBtnText: String?,
            listener: GeneralDialogListener?
        ): GeneralAlertDialogFragment? {
            this.mListener = listener
            this.title = title
            this.message = message
            this.positiveText = positiveBtnText
            this.negativeText = negativeBtnText
            return GeneralAlertDialogFragment()
        }
    }
}