package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.SuccessDTO
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface DeleteRequestService {
    @FormUrlEncoded
    @POST("delete-account")
    suspend fun deleteRequest(
        @Field("reason") reason: String
    ): Response<SuccessDTO>
}