package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.GenericResponseDto
import com.nfc.tapmee.common.models.LoginResponseDTO
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST


interface LoginService {
    @Headers("No-Authorization: true")
    @FormUrlEncoded
    @POST("login")
    suspend fun doLogin(
        @Field("email") username: String,
        @Field("password") password: String,
        @Field("fcm_token") fcm_token: String = "fcm_token"
    ): Response<GenericResponseDto<LoginResponseDTO>>
}