package com.nfc.tapmee.common.webview

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.webkit.*
import androidx.databinding.DataBindingUtil
import com.nfc.tapmee.R
import com.nfc.tapmee.base.BaseActivity
import com.nfc.tapmee.databinding.ActivityWebviewBinding
import java.net.URISyntaxException


const val WEB_URL = "web_url"
const val TITLE = "title"

class WebActivity : BaseActivity() {

    private lateinit var binding: ActivityWebviewBinding
    private var extraURL: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Binding view with activity
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_webview
        )

        // toolbar is defined in the layout file
        setSupportActionBar(findViewById(R.id.toolbar))

        // Get a support ActionBar corresponding to this toolbar and enable the Up button
        supportActionBar?.setDisplayShowHomeEnabled(true);
        supportActionBar?.setDisplayHomeAsUpEnabled(true);
        supportActionBar?.setHomeButtonEnabled(true);

        if (intent.hasExtra(WEB_URL)) {
            extraURL = intent.getStringExtra(WEB_URL)
            binding.title.text = intent.getStringExtra(TITLE)
        }

        renderWebView()

        configureWebview()

        if (!extraURL.isNullOrEmpty())
            binding.webView.loadUrl(extraURL!!)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    // Handle back button press in web view
    override fun onBackPressed() {
        if (binding.webView.canGoBack()) {
            // If web view have back history, then go to the web view back history
            binding.webView.goBack()

        } else {
            // Ask the user to exit the app or stay in here
            super.onBackPressed()
        }
    }

    private fun configureWebview() {
        // Get the web view settings instance
        val settings = binding.webView.settings


        // Enable java script in web view
        settings.javaScriptEnabled = true

        settings.cacheMode = WebSettings.LOAD_NO_CACHE

        // Enable zooming in web view
        settings.setSupportZoom(true)
        settings.builtInZoomControls = true
        settings.displayZoomControls = false


        // Enable disable images in web view
        settings.blockNetworkImage = false
        // Whether the WebView should load image resources
        settings.loadsImagesAutomatically = true


        // More web view settings
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            settings.safeBrowsingEnabled = true  // api 26
//        }
        //settings.pluginState = WebSettings.PluginState.ON
        settings.useWideViewPort = true
        settings.loadWithOverviewMode = true
        settings.javaScriptCanOpenWindowsAutomatically = true

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            settings.mediaPlaybackRequiresUserGesture = false
        }


        // More optional settings, you can enable it by yourself
        settings.domStorageEnabled = true
//        settings.setSupportMultipleWindows(true)
        settings.loadWithOverviewMode = true
        settings.allowContentAccess = true
        settings.allowUniversalAccessFromFileURLs = true
        settings.allowFileAccess = true

        // WebView settings
        binding.webView.fitsSystemWindows = true


        /*
            if SDK version is greater of 19 then activate hardware acceleration
            otherwise activate software acceleration
        */
        binding.webView.setLayerType(View.LAYER_TYPE_HARDWARE, null)

        binding.webView.setDownloadListener(DownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        })
    }

    private fun renderWebView() {

        binding.webView.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {
                binding.progressBar.progress = newProgress
            }
        }

        binding.webView.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                if (url!!.startsWith("http") || url.startsWith("https")) {
                    return false;
                }
                if (url.startsWith("intent")) {

                    try {
                        val intent: Intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);

                        val fallbackUrl: String =
                            intent.getStringExtra("browser_fallback_url").toString()
                        if (fallbackUrl != null) {
                            binding.webView.loadUrl(fallbackUrl);
                            return true;
                        }
                    } catch (e: URISyntaxException) {
                        //not an intent uri
                    }

                }

                return true //do nothing in other cases
            }
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                // Page loading started
                // Do something
                binding.progressBar.visibility = View.VISIBLE
            }

            override fun onPageFinished(view: WebView, url: String) {
                // Page loading finished
                // Display the loaded page title in a toast message
                binding.progressBar.visibility = View.GONE


            }
        }
    }

    fun getQueryString(url: String?, tag: String?): String? {
        try {
            val uri: Uri = Uri.parse(url)
            return uri.getQueryParameter(tag)
        } catch (e: Exception) {
            Log.e("Webview", "getQueryString() " + e.message)
        }
        return ""
    }
}
