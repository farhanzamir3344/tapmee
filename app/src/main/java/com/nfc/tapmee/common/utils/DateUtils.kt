package com.nfc.tapmee.common.utils

import android.util.Log
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    const val DATE_FORMAT_1 = "EEEE MMM dd, yyyy"
    const val DATE_FORMAT_2 = "dd/MM/yyyy"
    const val DATE_FORMAT_3 = "yyyyMMdd"
    const val DATE_FORMAT_4 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" //2020-09-01T07:54:19.000+01:00
    const val DATE_FORMAT_5 = "d/M/yyyy"
    const val DATE_FORMAT_6 = "dd/MM/yyyy"
    const val DATE_FORMAT_7 = "hh:mm"
    const val DATE_FORMAT_8 = "yyyy-MM-dd HH:mm:ss"
    const val DATE_FORMAT_9 = "hh:mm a"
    const val DATE_FORMAT_10 = "dd MMM yyyy"
    const val DATE_FORMAT_11 = "dd-MM-yyyy"
    const val DATE_FORMAT_12 = "yyyy-MM-dd"

    @JvmStatic
    fun getLocale(): Locale = Locale.US

    @JvmStatic
    fun toFormat(date: String, dateFormatFrom: String, dateFormatTo: String): String {
        val formatter: DateFormat = SimpleDateFormat(dateFormatFrom, getLocale())
        val myDate: Date = formatter.parse(date)
        return SimpleDateFormat(dateFormatTo, getLocale()).format(myDate)

    }

    @JvmStatic
    fun dateStringFromTimestamp(ts: Long): String? {
        val d = Date(ts)
        val formatString = "hh:mm a"
        val format = SimpleDateFormat(formatString)
        return format.format(d)
    }

    @JvmStatic
    fun timeStringFromTimestamp(ts: Long): String? {
        val d = Date(ts)
        val formatString = "dd/MM/yyyy"
        val format = SimpleDateFormat(formatString)
        return format.format(d)
    }

    @JvmStatic
    fun convertToRFC3339Date(ts: Long): String? {
        //2019-07-26T07:19:33.000Z
        val d = Date(ts)
        val format =
            SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'") //spec for RFC3339 with a 'Z' and fractional seconds
        format.timeZone = TimeZone.getTimeZone("GMT")
        Log.i("formated", format.format(d))
        return format.format(d)
    }

    @JvmStatic
    fun getCalculatedDate(dateFormat: String?, days: Int): String? {
        val cal = Calendar.getInstance()
        val s = SimpleDateFormat(dateFormat)
        cal.add(Calendar.DAY_OF_YEAR, days)
        return s.format(Date(cal.timeInMillis))
    }
}