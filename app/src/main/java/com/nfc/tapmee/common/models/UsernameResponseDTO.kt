package com.nfc.tapmee.common.models

data class UsernameResponseDTO(
    val user: CustomerDTO
)
