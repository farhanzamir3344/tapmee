package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.SuccessDTO
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface UpdateContactCardService {
    @FormUrlEncoded
    @POST("contact_card")
    suspend fun updateContactCard(
        @Field("is_business") isBusiness: String,
        @Field("profile_ids") profileIDs: String
    ): Response<SuccessDTO>
}