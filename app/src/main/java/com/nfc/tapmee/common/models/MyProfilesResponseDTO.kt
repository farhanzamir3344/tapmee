package com.nfc.tapmee.common.models

data class MyProfilesResponseDTO(
    val profiles: List<Profile>,
    val user_profile_link: String? = null,
    val user: CustomerDTO? = null,
    val profile: CustomerDTO? = null

)