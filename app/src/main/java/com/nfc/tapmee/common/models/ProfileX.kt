package com.nfc.tapmee.common.models


import com.google.gson.annotations.SerializedName

data class ProfileX(
    @SerializedName("icon")
    val icon: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("is_added")
    val isAdded: Int?,
    @SerializedName("is_pro")
    val isPro: Int?,
    @SerializedName("profile_code")
    val profileCode: String?,
    @SerializedName("hint")
    val hint: String?,
    @SerializedName("profile_links")
    val profileLinks: List<ProfileLink>?,
    @SerializedName("profile_type_id")
    val profileTypeId: Int?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("title_de")
    val titleDe: String?
)