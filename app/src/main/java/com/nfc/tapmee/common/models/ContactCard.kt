package com.nfc.tapmee.common.models


import com.google.gson.annotations.SerializedName

data class ContactCard(
    @SerializedName("added_to_contact_card")
    val addedToContactCard: String?,
    @SerializedName("icon")
    val icon: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("is_business")
    val isBusiness: Int?,
    @SerializedName("profile_code")
    val profileCode: String?,
    @SerializedName("profile_link")
    val profileLink: String?,
    @SerializedName("profile_value")
    val profileValue: String?,
    @SerializedName("sequence")
    val sequence: Int?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("title_de")
    val titleDe: String?,
    @SerializedName("user_id")
    val userId: Int?
)