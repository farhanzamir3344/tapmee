package com.nfc.tapmee.common.utils

import android.content.Context
import android.graphics.Rect
import android.view.View
import android.view.animation.*

object AnimationUtil
{
     fun createScaleAnim(fromX:Float, toX:Float, fromY:Float, toY:Float, duration: Long, offset:Long): ScaleAnimation {
        val anim = ScaleAnimation(fromX, toX, fromY, toY, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        anim.duration = duration
        anim.startOffset = offset
        return anim
    }

     fun createTranslateAnim(fromX: Float, toX: Float, fromY: Float, toY: Float, duration: Long, offset: Long): Animation {
        val anim = TranslateAnimation(fromX, toX,fromY, toY) //TranslateAnimation(fromX,toX,fromY,toY)
        anim.duration = duration
        anim.startOffset = offset
        anim.fillAfter = true
        return anim
    }
     fun createAlphaAnim(fromAlpha:Float, toAlpha:Float, duration:Long, starOffset:Long): Animation {
        val anim = AlphaAnimation(fromAlpha,toAlpha)
        anim.duration = duration
        anim.startOffset = starOffset
        anim.fillAfter = true
        return anim
    }

    fun getAnimSet(context:Context): AnimationSet {
        return AnimationSet(context,null)
    }



    fun getDimension(view: View): Rect {
        val rectF = Rect()
        //For coordinates location relative to the parent
        view.getLocalVisibleRect(rectF)
        //For coordinates location relative to the screen/display
        view.getGlobalVisibleRect(rectF)
        return rectF
    }
}