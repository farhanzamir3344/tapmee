package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.SuccessDTO
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST


interface PasswordResetService {
    @Headers("No-Authorization: true")
    @FormUrlEncoded
    @POST("reset_password")
    suspend fun doReset(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("password_confirmation") confirmPassword: String
    ): Response<SuccessDTO>

}