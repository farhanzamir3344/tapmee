package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.GenericResponseDto
import com.nfc.tapmee.common.models.UpdateProfileResponseDTO
import retrofit2.Response
import retrofit2.http.GET

interface GetUserProfileService {
    @GET("user_profile")
    suspend fun userProfile(): Response<GenericResponseDto<UpdateProfileResponseDTO>>
}