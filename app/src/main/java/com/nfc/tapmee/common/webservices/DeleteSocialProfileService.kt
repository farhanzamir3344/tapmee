package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.SuccessDTO
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface DeleteSocialProfileService {
    @FormUrlEncoded
    @POST("delete-profile")
    suspend fun deleteSocialProfile(
        @Field("my_profile_id") profileID: String
    ): Response<SuccessDTO>
}