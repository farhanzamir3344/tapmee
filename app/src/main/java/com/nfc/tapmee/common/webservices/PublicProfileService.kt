package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.GenericResponseDto
import com.nfc.tapmee.common.models.MyProfilesResponseDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface PublicProfileService {
    @Headers("No-Authorization: true")
    @GET("public_profile")
    suspend fun getProfile(
        @Query("username") username: String
    ): Response<GenericResponseDto<MyProfilesResponseDTO>>
}