package com.nfc.tapmee.common.models

data class RegisterDTO(
    val access_token: String,
    val user: UserDTO
)