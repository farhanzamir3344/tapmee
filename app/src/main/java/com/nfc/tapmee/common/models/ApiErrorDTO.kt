package com.nfc.tapmee.common.models

data class ApiErrorDTO(
    val error: String,
    val `field`: String
)