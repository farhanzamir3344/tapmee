package com.nfc.tapmee.common.models


import com.google.gson.annotations.SerializedName

data class ProfileResponseDTO(
    @SerializedName("profile_types")
    val profileTypes: List<ProfileType>?
)