package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.RegisterResponseDTO
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST

interface RegisterService {
    @Headers("No-Authorization: true")
    @FormUrlEncoded
    @POST("signup")
    suspend fun doRegister(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("password_confirmation") confirmPassword: String,
        @Field("fcm_token") fcm_token: String = "fcm_token",
        @Field("allow_data_usage") allowDataUsage: Int,
        @Field("device_id") deviceID: String,
        @Field("device_type") deviceType: String,
    ): Response<RegisterResponseDTO>
}