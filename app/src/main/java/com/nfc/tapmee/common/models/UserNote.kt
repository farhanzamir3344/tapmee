package com.nfc.tapmee.common.models


import com.google.gson.annotations.SerializedName

data class UserNote(
    @SerializedName("created_at")
    val createdAt: String?,
    @SerializedName("created_by")
    val createdBy: Int?,
    @SerializedName("email")
    val email: String?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("note")
    val note: String?,
    @SerializedName("phone_no")
    val phoneNo: String?,
    @SerializedName("updated_at")
    val updatedAt: String?,
    @SerializedName("updated_by")
    val updatedBy: Any?,
    @SerializedName("user_id")
    val userId: Int?
)