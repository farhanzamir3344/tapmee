package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.GenericResponseDto
import com.nfc.tapmee.common.models.UpdateProfileResponseDTO
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface UpdateProfileService {
    @FormUrlEncoded
    @POST("profile")
    suspend fun updateProfile(
        @Field("name") name: String,
        @Field("bio") bio: String,
        @Field("gender") gender: Int,
        @Field("is_public") isPublic: Int,
        @Field("dob") dob: String
    ): Response<GenericResponseDto<UpdateProfileResponseDTO>>
}