package com.nfc.tapmee.common.network

import android.annotation.SuppressLint
import android.util.Log
import com.google.gson.GsonBuilder
import com.nfc.tapmee.BuildConfig
import com.nfc.tapmee.common.preferences.PreferenceUtils
import com.nfc.tapmee.common.utils.AppOP
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class NetworkApiClient {
    companion object {

        fun getClientInstance(): Retrofit {

            return Retrofit.Builder()
                .baseUrl(BuildConfig.API_SERVER)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .client(getOkHttpClient())
                .build()
        }


        private fun getOkHttpClient(): OkHttpClient {
            val sslSocketFactory = getSSLContext().socketFactory

            val builder = OkHttpClient
                .Builder()
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)

            //Headers Interceptor
            addTokenHeaderInterceptor(builder)

            //Add Response Interceptor
            addResponseInterceptor(builder)

            if (BuildConfig.DEBUG) {
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                builder.addInterceptor(interceptor)
            }


            return builder
                .sslSocketFactory(
                    sslSocketFactory,
                    getAllTrustCertificates()[0] as X509TrustManager
                )
                .build()
        }

        /*
        * Add Response Interceptor to check response error code for Invalid
        * Token expiry, in this case we have to redirect user to login screen
         */
        private fun addResponseInterceptor(builder: OkHttpClient.Builder) {

            builder.addInterceptor { chain ->
                val request = chain.request()
                val response = chain.proceed(request)

                if (response.code == 401 &&
                        !request.url.toString().contains("login")) {
                    //Redirect to login
                    AppOP.launchLoginActivityClearAll()
                }

                response
            }

        }

        private fun addTokenHeaderInterceptor(builder: OkHttpClient.Builder) {
            builder.addInterceptor { chain ->
                val original = chain.request()
                val request = original.newBuilder()


                request.addHeader("Accept", "application/json")

                // we are not adding 'x-token' for non-authorized api's
                if (original.header("No-Authorization") == null) {

                    val token = PreferenceUtils.getAuthToken().toString()

                    if (token.isNotEmpty())
                        request.addHeader("Authorization", "Bearer $token")
                    else
                        Log.i("AUTHENTICATION_ERROR", "Token is empty")
                }

                chain.proceed(
                    request
                        .method(original.method, original.body)
                        .build()
                )
            }
        }

        private fun getSSLContext(): SSLContext {
            val sslContext = SSLContext.getInstance("TLS")
            sslContext.init(null, getAllTrustCertificates(), null)
            return sslContext
        }

        private fun getAllTrustCertificates(): Array<TrustManager> {
            return arrayOf(object : X509TrustManager {

                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return arrayOf()
                }

                @SuppressLint("TrustAllX509TrustManager")
                @Throws(CertificateException::class)
                override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
                }

                @SuppressLint("TrustAllX509TrustManager")
                @Throws(CertificateException::class)
                override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
                }
            })
        }
    }

}