package com.nfc.tapmee.common.utils.dialog

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.nfc.tapmee.R
import com.nfc.tapmee.common.models.Profile
import com.nfc.tapmee.common.utils.dialog.consent.DataUsageConsentDialog
import com.nfc.tapmee.common.utils.dialog.profiledialog.ProfileUpdateListener
import com.nfc.tapmee.common.utils.dialog.profiledialog.SocialProfileBottomSheet

object AlertOP {
    /**
     * showAlert static function to show Generic Alert message with
     * Message body
     * Positivie button
     * Negative button
     * Listener to get respective callbacks
     */
    @JvmStatic
    fun showAlert(
        context: Context,
        title: String?,
        message: String?,
        positiveBtnText: String?,
        negativeBtnText: String?,
        listener: GeneralDialogListener?
    ) {

        val alertDialog = GeneralAlertDialogFragment.newInstance(
            title,
            message,
            positiveBtnText,
            negativeBtnText,
            listener
        )

        val ft: FragmentTransaction =
            (context as AppCompatActivity).supportFragmentManager.beginTransaction()
        ft.add(alertDialog!!, "general_alert_dialog")
        ft.commitAllowingStateLoss()

    }


    /**
     * showResponseAlertOK static function to show Generic Alert message with
     * Message body
     * Positivie button as Ok
     * Listener to get respective callbacks
     */
    @JvmStatic
    fun showResponseAlertOK(
        context: Context,
        title: String?,
        message: String?,
        listener: GeneralDialogListener? = null
    ) {
        try {
            if (message != null && message.isNotEmpty()) {

                val activity = context as AppCompatActivity

                val alertDialogFragment: GeneralAlertDialogFragment? =
                    GeneralAlertDialogFragment.newInstance(title, message,
                        context.getString(R.string.lbl_dialog_ok),
                        null,
                        object : GeneralDialogListener {
                            override fun onPositiveClick() { // On Skip btn click
                                listener?.onPositiveClick()
                            }

                            override fun onNegativeClick() { //on Cancel btn key
                            }
                        })

                val ft: FragmentTransaction =
                    activity.supportFragmentManager.beginTransaction()
                ft.add(alertDialogFragment!!, "showResponseAlertOK")
                ft.commitAllowingStateLoss()

            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


    /**
     * showProfileDetailSheet static function to show Profile DetailBottom Sheet on Home screen
     */
    @JvmStatic
    fun showProfileDetailSheet(
        context: Context,
        profileDTO: Profile,
        listener: ProfileUpdateListener
    ) {
        try {
            val activity = context as AppCompatActivity

            activity.supportFragmentManager.executePendingTransactions()

            if (activity.supportFragmentManager.findFragmentByTag("socialProfileBottomSheet") == null) {


                val socialProfileBottomSheet: SocialProfileBottomSheet? =
                    SocialProfileBottomSheet.newInstance(profileDTO, listener)

                val ft: FragmentTransaction =
                    activity.supportFragmentManager.beginTransaction()

                ft.add(socialProfileBottomSheet!!, "socialProfileBottomSheet")
                ft.commitAllowingStateLoss()
            }


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    @JvmStatic
    fun showDataUsageConsentDialog(
        context: Context,
        listener: GeneralDialogListener
    ) {
        try {
            val activity = context as AppCompatActivity

            activity.supportFragmentManager.executePendingTransactions()

            if (activity.supportFragmentManager.findFragmentByTag("dataUsageDialog") == null) {


                val dataUsageDialog: DataUsageConsentDialog? =
                    DataUsageConsentDialog.newInstance(listener)

                val ft: FragmentTransaction =
                    activity.supportFragmentManager.beginTransaction()

                ft.add(dataUsageDialog!!, "dataUsageDialog")
                ft.commitAllowingStateLoss()
            }


        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}