package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.CustomerDTO
import com.nfc.tapmee.common.models.GenericResponseDto
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface VerifyDeleteOtpService {
    @FormUrlEncoded
    @POST("confirm-delete-account")
    suspend fun verifyDeleteOtp(
        @Field("reason") reason: String,
        @Field("vcode") otp: String
    ): Response<GenericResponseDto<CustomerDTO>>
}