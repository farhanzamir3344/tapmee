package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.SuccessDTO
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AddProfileService {
    @FormUrlEncoded
    @POST("add_profile")
    suspend fun addProfile(
        @Field("profile_link") profileLink: String,
        @Field("business_profile") businessProfile: String,
        @Field("profile_code") profileCode: String
    ): Response<SuccessDTO>
}