package com.nfc.tapmee.common.webservices


import com.nfc.tapmee.common.models.ContactResponseDTO
import com.nfc.tapmee.common.models.GenericResponseDto
import retrofit2.Response
import retrofit2.http.GET

interface ContactsListService {
    @GET("user_notes")
    suspend fun getUserNotes(): Response<GenericResponseDto<ContactResponseDTO>>
}