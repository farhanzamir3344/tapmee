package com.nfc.tapmee.common.utils

import android.media.AudioManager
import android.media.ToneGenerator
import android.os.Handler
import android.os.Looper

object BeepHelper {
    val toneG = ToneGenerator(AudioManager.STREAM_ALARM, 100)

    fun beep(duration: Int) {
        try {
            toneG.startTone(ToneGenerator.TONE_DTMF_S, duration)
            val handler = Handler(Looper.getMainLooper())
            handler.postDelayed({
                toneG.release()
            }, (duration + 50).toLong())
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }
}