package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.SuccessDTO
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface UpdateProfileSequenceService {
    @FormUrlEncoded
    @POST("save_sequence")
    suspend fun updateSequence(@Field("sequence") sequence: String): Response<SuccessDTO>
}