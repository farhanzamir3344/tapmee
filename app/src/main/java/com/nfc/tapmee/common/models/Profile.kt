package com.nfc.tapmee.common.models

data class Profile(
    var added_to_contact_card: String? = null,
    val icon: String? = null,
    val id: Int? = null,
    val is_business: Int? = null,
    val is_pro: Int? = null,
    val open_direct: Int? = null,
    val profile_code: String? = null,
    val profile_link: String? = null,
    val profile_link_value: String? = null,
    val base_url: String? = null,
    val banner: String? = null,
    val bio: String? = null,
    val sequence: Int? = null,
    val title: String? = null,
    val title_de: String? = null,
    val username: String? = null,
    val name: String? = null,
    val profile_view: String? = null,
    val user_id: Int? = null,
    val icon_local: Int? = null,
    var isDisabled: Boolean = false
)