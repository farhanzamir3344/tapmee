package com.nfc.tapmee.common.utils

object Constants {

    const val DEVICE_TYPE = "Android"
    const val PRIVATE_MODE = 0
    const val APP_PREFERENCE_NAME = "tapmee_app"
    const val APP_URL = "tap-mee.com/"
    const val APP_URL_WEB = "https://tap-mee.com/"

    const val IS_LOGIN_CUSTOMER = "is_login_customer"
    const val IS_DIRECT_ON = "is_direct_on"
    const val IS_NEW_USER = "is_new_user"
    const val IS_REGISTER_CUSTOMER = "is_register_customer"
    const val USERNAME = "username"
    const val TOKEN = "authApiToken"
    const val EMAIL = "email"


    const val MALE = "email"
    const val FEMALE = "email"
    const val PREFER_NOT_TO_SHARE = "email"
    const val CUSTOM = "email"

    const val BUY_NFC = "https://tapmee.co/shop/"
}