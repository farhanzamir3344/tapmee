package com.nfc.tapmee.common.utils.dialog

interface GeneralDialogListener {
    fun onPositiveClick()
    fun onNegativeClick()
}