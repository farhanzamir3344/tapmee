package com.nfc.tapmee.common.utils.nfc

import android.nfc.NdefMessage
import android.nfc.NdefRecord
import android.nfc.Tag
import android.nfc.tech.Ndef
import android.nfc.tech.NdefFormatable

class NFCManager() {


    fun writeTag(tag: Tag?, message: NdefMessage?, mListener: WriteNfcListener) {
        if (tag != null) {
            try {
                val ndefTag = Ndef.get(tag)
                if (ndefTag == null) {

                    // Let's try to format the Tag in NDEF
                    val nForm = NdefFormatable.get(tag)
                    if (nForm != null) {
                        nForm.connect()
                        nForm.format(message)
                        nForm.close()

                        mListener.onWriteSuccess()
                    }
                } else {
                    ndefTag.connect()
                    ndefTag.writeNdefMessage(message)
                    ndefTag.close()
                    mListener.onWriteSuccess()
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                mListener.onWriteFailed()
            }

        }

    }

    fun createUriMessage(content: String, type: String): NdefMessage? {
        val record = NdefRecord.createUri(type + content)
        return NdefMessage(arrayOf(record))
    }
}