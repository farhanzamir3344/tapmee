package com.nfc.tapmee.common.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class CustomerDTO(

    @PrimaryKey(autoGenerate = false)
    var unique_id: Long = 0,

    var banner: String? = null,
    var bio: String? = null,
    var created_at: String? = null,
    var created_by: String? = null,
    var device_id: String? = null,
    var device_type: String? = null,
    var dob: String? = null,
    var email: String? = null,
    var email_verified_at: String? = null,
    var fcm_token: String? = null,
    var gender: Int? = null,
    @SerializedName("id")
    var customerID: Int? = null,
    var is_pro: Int? = null,
    var is_public: Int? = null,
    var last_login: String? = null,
    var logo: String? = null,
    var name: String? = null,
    var open_direct: Int? = null,
    var platform: String? = null,
    var profile_view: String? = null,
    var provider: String? = null,
    var status: Int? = null,
    var subscription_date: String? = null,
    var subscription_expires_on: String? = null,
    var updated_at: String? = null,
    var updated_by: Int? = null,
    var user_group_id: String? = null,
    var username: String? = null,
    var vcode_expiry: String? = null
) : Serializable