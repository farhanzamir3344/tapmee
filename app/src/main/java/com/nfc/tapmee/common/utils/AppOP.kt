package com.nfc.tapmee.common.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.provider.Settings
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.nfc.tapmee.common.preferences.SharedPreferences
import com.nfc.tapmee.main.TapMeeApplication
import com.nfc.tapmee.ui.activities.LoginAndRegisterActivity

object AppOP {

    /**
     *
     * launchLoginActivityClearAll is an utility static function helps to clear
     * All Database tables from Room
     * All SharedPreferences
     * All activities in stack
     * And Redirect user to Login screen
     * This function is useful for Redirecting user to login screen if MySmile Token expires anytime
     *
     */
    @JvmStatic
    fun launchLoginActivityClearAll() {

        //clear all preferences
        SharedPreferences.getInstance().clearPreferences()

        val startActivity = Intent()
        startActivity.setClass(
            TapMeeApplication.applicationContext(),
            LoginAndRegisterActivity::class.java
        )
        startActivity.action = LoginAndRegisterActivity::class.java.name
        startActivity.flags =
            (Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        TapMeeApplication.applicationContext().startActivity(startActivity)

    }

    @JvmStatic
    fun launchLoginActivityClearAll(activity: Activity) {

        //clear all preferences
        SharedPreferences.getInstance().clearPreferences()

        val startActivity = Intent()
        startActivity.setClass(activity, LoginAndRegisterActivity::class.java)
        startActivity.action = LoginAndRegisterActivity::class.java.name
        startActivity.flags =
            (Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        activity.startActivity(startActivity)

    }

    fun shareApp(messageToShare: String, appUrl: String, context: Context) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, messageToShare + appUrl)
        context.startActivity(Intent(intent))
    }

    fun Context.copyToClipboard(text: CharSequence) {
        val clipboard = ContextCompat.getSystemService(this, ClipboardManager::class.java)
        val clip = ClipData.newPlainText("label", text)
        clipboard!!.setPrimaryClip(clip)
        Toast.makeText(this, "Link Copied", Toast.LENGTH_SHORT).show()
    }

    @SuppressLint("HardwareIds")
    @JvmStatic
    fun deviceID(mContext: Context): String {
        return Settings.Secure.getString(mContext.contentResolver, Settings.Secure.ANDROID_ID)
    }
}