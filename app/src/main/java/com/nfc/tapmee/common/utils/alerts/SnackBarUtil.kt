package com.nfc.tapmee.common.utils.alerts

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.TextView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.nfc.tapmee.R

object SnackBarUtil {

    /**
     * Snackbar with no action Button and Default Background (Default One)
     */

    fun showSnackBar(context: Context, message: String, isLengthLong: Boolean) {
        try {
            val snack = Snackbar.make(
                (context as Activity).findViewById<View>(android.R.id.content),
                message + "", Snackbar.LENGTH_SHORT
            )
            if (isLengthLong)
                snack.duration = BaseTransientBottomBar.LENGTH_LONG
            val view = snack.view
            val tv = view
                .findViewById(R.id.snackbar_text) as TextView
            tv.setTextColor(Color.WHITE)
            snack.show()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }


    /**
     * Snackbar with no action Button and Custom Background
     */
    fun showSnackBar(
        context: Context, message: String, bgColor: Int,
        isLengthLong: Boolean
    ) {
        try {
            val snack = Snackbar.make(
                (context as Activity).findViewById<View>(android.R.id.content),
                message + "", Snackbar.LENGTH_SHORT
            )
            if (isLengthLong)
                snack.duration = BaseTransientBottomBar.LENGTH_LONG
            val view = snack.view
            view.setBackgroundColor(bgColor)
            val tv = view
                .findViewById(R.id.snackbar_text) as TextView
            tv.setTextColor(Color.WHITE)
            snack.show()
        } catch (ex: Exception) {

        }

    }

    /**
     * Snackbar with action Button and Custom Background
     */
    fun showSnackBar(
        context: Context, message: String,
        bgColor: Int, actionButton: String, isLengthLong: Boolean,
        actionButtonClickListener: View.OnClickListener
    ) {
        try {
            val snack = Snackbar.make(
                (context as Activity).findViewById<View>(android.R.id.content),
                message + "", Snackbar.LENGTH_SHORT
            )
            if (isLengthLong)
                snack.duration = BaseTransientBottomBar.LENGTH_INDEFINITE

            snack.setAction(actionButton, actionButtonClickListener)

            val view = snack.view
            view.setBackgroundColor(bgColor)
            val tv = view
                .findViewById(R.id.snackbar_text) as TextView

            val tvAction = view
                .findViewById(R.id.snackbar_action) as TextView
            tvAction.textSize = 16f
            tvAction.setTextColor(Color.WHITE)

            tv.setTextColor(Color.WHITE)
            snack.show()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    /**
     * Snackbar with action Button and Default Background
     */
    fun showSnackBar(
        context: Context, message: String,
        actionButton: String, isLengthLong: Boolean,
        actionButtonClickListener: View.OnClickListener
    ) {
        try {
            val snack = Snackbar.make(
                (context as Activity).findViewById<View>(android.R.id.content),
                message + "", Snackbar.LENGTH_SHORT
            )
            if (isLengthLong)
                snack.duration = BaseTransientBottomBar.LENGTH_INDEFINITE
            snack.setAction(actionButton, actionButtonClickListener)
            val view = snack.view
            val tv = view
                .findViewById(R.id.snackbar_text) as TextView
            tv.setTextColor(Color.WHITE)

            val tvAction = view
                .findViewById(R.id.snackbar_action) as TextView
            tvAction.textSize = 16f
            tvAction.setTextColor(Color.WHITE)

            snack.show()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }
}