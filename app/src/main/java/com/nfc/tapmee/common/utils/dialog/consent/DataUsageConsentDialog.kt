package com.nfc.tapmee.common.utils.dialog.consent

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.nfc.tapmee.R
import com.nfc.tapmee.common.utils.dialog.GeneralDialogListener
import com.nfc.tapmee.databinding.DataUsageConsentDialogBinding

class DataUsageConsentDialog : DialogFragment() {

    private lateinit var binding: DataUsageConsentDialogBinding

    // region LIFECYCLE

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.data_usage_consent_dialog,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        dialog!!.setCancelable(false)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        setValues()

        binding.btnAccept.setOnClickListener {
            mListener?.onPositiveClick()
            dialog!!.dismiss()
        }

        binding.btnNotNow.setOnClickListener {

            mListener?.onNegativeClick()
            dialog!!.dismiss()
        }
    }

    // endregion

    //region HELPING METHODS

    private fun setValues() {

        try {

            dialog!!.setCancelable(false)
            dialog!!.setCanceledOnTouchOutside(false)


        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    //endregion


    companion object {
        private var mListener: GeneralDialogListener? = null

        fun newInstance(
            listener: GeneralDialogListener?
        ): DataUsageConsentDialog? {
            this.mListener = listener
            return DataUsageConsentDialog()
        }
    }
}