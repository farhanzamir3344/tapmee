package com.nfc.tapmee.common.webservices

import com.nfc.tapmee.common.models.GenericResponseDto
import com.nfc.tapmee.common.models.UsernameResponseDTO
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST


interface VerifyUsernameService {
    @Headers("No-Authorization: true")
    @FormUrlEncoded
    @POST("check_username")
    suspend fun verify(@Field("username") username: String): Response<GenericResponseDto<UsernameResponseDTO>>
}