package com.nfc.tapmee.common.database.dao

import androidx.room.*
import com.nfc.tapmee.common.models.CustomerDTO

@Dao
interface CustomerDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveCustomerDto(customerDTO: CustomerDTO): Long

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun update(customerDTO: CustomerDTO)

    @Query("Select * from CustomerDTO")
    suspend fun getCustomerDto(): CustomerDTO
}