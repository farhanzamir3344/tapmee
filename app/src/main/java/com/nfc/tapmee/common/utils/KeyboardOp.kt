package com.nfc.tapmee.common.utils

import android.app.Activity
import android.app.Service
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

object KeyboardOp {

    fun hide(activity: Activity) {
        try {
            if (activity != null) {
                val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                val focusedView: View? = activity.currentFocus

                if(focusedView != null)
                    imm.hideSoftInputFromWindow(
                        activity.currentFocus!!
                            .windowToken, InputMethodManager.SHOW_FORCED
                    )
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

    fun hide(activity: Activity, et: EditText) {
        try {
            et.clearFocus()
            val imm = activity
                .getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(et.windowToken, 0)
        } catch (e: Exception) {
        }

    }

    fun show(activity: Activity, et: EditText) {
        try {
            et.requestFocus()
            val imm = activity
                .getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(et, 0)
            imm.toggleSoftInput(
                InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY
            )
        } catch (e: Exception) {
        }

    }
}