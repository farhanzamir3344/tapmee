package com.nfc.tapmee.common.models

data class UpdateOpenDirectResponseDTO(
    val profile: CustomerDTO? = null
)