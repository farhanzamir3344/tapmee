package com.nfc.tapmee.common.models


import com.google.gson.annotations.SerializedName

data class ProfileType(
    @SerializedName("created_at")
    val createdAt: String?,
    @SerializedName("created_by")
    val createdBy: Int?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("profiles")
    val profiles: List<ProfileX>?,
    @SerializedName("status")
    val status: Int?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("updated_at")
    val updatedAt: String?,
    @SerializedName("updated_by")
    val updatedBy: Int?
)