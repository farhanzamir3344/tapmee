package com.nfc.tapmee.ui.auth.register


import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.facebook.*
import com.facebook.login.BuildConfig
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.nfc.tapmee.R
import com.nfc.tapmee.arch.providers.auth.LoginViewModelProviderFactory
import com.nfc.tapmee.arch.repositories.auth.LoginRepository
import com.nfc.tapmee.arch.viewmodels.auth.LoginViewModel
import com.nfc.tapmee.base.BaseActivity
import com.nfc.tapmee.base.BaseFragment
import com.nfc.tapmee.base.util.FragmentUtil
import com.nfc.tapmee.common.preferences.PreferenceUtils
import com.nfc.tapmee.common.utils.KeyboardOp
import com.nfc.tapmee.common.utils.alerts.SnackBarUtil
import com.nfc.tapmee.common.utils.dialog.AlertOP
import com.nfc.tapmee.common.utils.dialog.GeneralDialogListener
import com.nfc.tapmee.common.utils.generics.Resource
import com.nfc.tapmee.common.utils.views.TextViewUtils.makeTextLink
import com.nfc.tapmee.databinding.FragmentRegisterBinding
import com.nfc.tapmee.ui.activities.DashboardActivity
import com.nfc.tapmee.ui.activities.UsernameActivity
import com.nfc.tapmee.ui.auth.LoginRegisterHomeFragment
import kotlinx.android.synthetic.main.layout_social_links.view.*
import java.util.*


class RegisterFragment private constructor() : BaseFragment() {

    lateinit var binding: FragmentRegisterBinding
    private lateinit var viewModel: LoginViewModel
    private var isValidInputs = false

    // [START declare_auth]
    private lateinit var auth: FirebaseAuth
    // [END declare_auth]

    private lateinit var googleSignInClient: GoogleSignInClient

    private lateinit var callbackManager: CallbackManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupFirebaseAuth()
        setupFacebookSDK()

        init()
        initViewModel()
        handleRegisterResponse()
    }

    //region VIEW-MODEL
    private fun initViewModel() {

        val viewModelProviderFactory =
            LoginViewModelProviderFactory(requireActivity().application, LoginRepository())
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory).get(LoginViewModel::class.java)
    }
    //endregion

    private fun init() {

        validate()

        binding.btnRegister.setOnClickListener {
            KeyboardOp.hide(requireActivity(), binding.confirmPassword)

            val email = binding.loginEmail.text!!.trim().toString()
            val password = binding.loginPassword.text!!.trim().toString()

            if (isValidInputs) {
                if (binding.cbTermsConditions.isChecked) {
                    viewModel.registerUser(
                        email = email,
                        password = password,
                        allowDataUsage = 1
                    )

                } else {
                    AlertOP.showResponseAlertOK(
                        requireContext(),
                        null,
                        getString(R.string.alert_msg_terms_conditions_checkbox)
                    )
                }
            }


        }

        binding.socialLoginContainer.ivGoogleSignIn.setOnClickListener {
            val signInIntent = googleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }

        binding.socialLoginContainer.ivFacebookSignIn.setOnClickListener {

            LoginManager.getInstance()
                .logInWithReadPermissions(this, listOf("public_profile", "email"))
        }

        // Callback registration
        LoginManager.getInstance().registerCallback(callbackManager, object :
            FacebookCallback<LoginResult?> {
            override fun onSuccess(loginResult: LoginResult?) {
                Log.d("TAG", "Success Login")
                // Get User's Info
                getUserProfile(loginResult?.accessToken, loginResult?.accessToken?.userId)
            }

            override fun onCancel() {
                SnackBarUtil.showSnackBar(requireContext(), "Login Cancelled", true)
            }

            override fun onError(exception: FacebookException) {
                SnackBarUtil.showSnackBar(requireContext(), exception.message.toString(), true)
            }
        })


        makeTextLink(
            binding.tvPrivacyPolicy,
            "Privacy Policy",
            true,
            Color.BLACK
        ) {
            (activity as BaseActivity).gotoWeb(
                getString(R.string.lbl_privacy_policy_url),
                getString(R.string.lbl_privacy_policy)
            )
        }

        makeTextLink(
            binding.tvPrivacyPolicy,
            "Terms & Conditions",
            true,
            Color.BLACK
        ) {
            (activity as BaseActivity).gotoWeb(
                getString(R.string.lbl_terms_url),
                getString(R.string.lbl_terms)
            )
        }

        binding.btnLogin.setOnClickListener {
            FragmentUtil(requireActivity() as AppCompatActivity).goBackFragment()

        }
    }

    //region HELPING METHODS
    private fun validate() {

        binding.loginEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                val email = binding.loginEmail.text!!.trim().toString()
                val password = binding.loginPassword.text.toString()
                val confirmPassword = binding.confirmPassword.text.toString()

                if (Patterns.EMAIL_ADDRESS.matcher(email).matches() &&
                    password.isNotEmpty() &&
                    confirmPassword.isNotEmpty() &&
                    password == confirmPassword
                ) {
                    isValidInputs = true
                    binding.btnRegister.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.bg_btn_enabled)
                } else {
                    isValidInputs = false
                    binding.btnRegister.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.bg_btn_disabled)
                }
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })

        binding.loginPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                val email = binding.loginEmail.text!!.trim().toString()
                val password = binding.loginPassword.text.toString()
                val confirmPassword = binding.confirmPassword.text.toString()

                if (Patterns.EMAIL_ADDRESS.matcher(email).matches() &&
                    password.isNotEmpty() &&
                    confirmPassword.isNotEmpty() &&
                    password == confirmPassword
                ) {

                    isValidInputs = true
                    binding.btnRegister.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.bg_btn_enabled)
                } else {
                    isValidInputs = false
                    binding.btnRegister.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.bg_btn_disabled)
                }
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })


        binding.confirmPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                val email = binding.loginEmail.text!!.trim().toString()
                val password = binding.loginPassword.text.toString()
                val confirmPassword = binding.confirmPassword.text.toString()

                if (Patterns.EMAIL_ADDRESS.matcher(email).matches() &&
                    password.isNotEmpty() &&
                    confirmPassword.isNotEmpty() &&
                    password == confirmPassword
                ) {

                    isValidInputs = true
                    binding.btnRegister.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.bg_btn_enabled)
                } else {
                    isValidInputs = false
                    binding.btnRegister.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.bg_btn_disabled)
                }
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })

    }
    //endregion

    //region API HANDLE RESPONSE
    private fun handleRegisterResponse() {

        viewModel.registerLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    PreferenceUtils.saveAuthToken(response.data!!.access_token)
                    PreferenceUtils.saveIsRegisterCustomer()

                    startActivity(
                        Intent(
                            requireActivity(),
                            UsernameActivity::class.java
                        )
                    )
                    requireActivity().finish()
                }
                is Resource.Error -> {
                    hideProgressBar()

                    val errorMsg =
                        if (response.message!!.contains("The email has already been taken")) {
                            getString(R.string.lbl_error_msg_already_taken)
                        } else
                            response.message

                    AlertOP.run {
                        showResponseAlertOK(requireContext(),
                            getString(R.string.lbl_login_title),
                            errorMsg,
                            object : GeneralDialogListener {
                                override fun onPositiveClick() {
                                    (parentFragment as LoginRegisterHomeFragment?)!!.selectLoginTab()
                                }

                                override fun onNegativeClick() {

                                }
                            })
                    }
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })


        viewModel.userSocialLoginLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {

                    val userResponse = response.data!!

                    PreferenceUtils.saveAuthToken(userResponse.access_token)

                    if (userResponse.user!!.username.isNullOrEmpty()) {
                        PreferenceUtils.saveIsRegisterCustomer()

                        startActivity(
                            Intent(
                                requireActivity(),
                                UsernameActivity::class.java
                            )
                        )
                        requireActivity().finish()
                    } else {


                        PreferenceUtils.saveIsLoginCustomer()

                        hideProgressBar()

                        startActivity(
                            Intent(
                                requireActivity(),
                                DashboardActivity::class.java
                            )
                        )
                        requireActivity().finish()

                    }

                }
                is Resource.Error -> {
                    hideProgressBar()
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })

    }
    //endregion

    //region Setup Facebook
    private fun setupFacebookSDK() {
        callbackManager = CallbackManager.Factory.create()
    }

    @SuppressLint("LongLogTag")
    fun getUserProfile(token: AccessToken?, userId: String?) {

        val parameters = Bundle()
        parameters.putString(
            "fields",
            "email"
        )
        GraphRequest(token,
            "/$userId/",
            parameters,
            HttpMethod.GET,
            GraphRequest.Callback { response ->
                val jsonObject = response.jsonObject

                // Facebook Access Token
                // You can see Access Token only in Debug mode.
                // You can't see it in Logcat using Log.d, Facebook did that to avoid leaking user's access token.
                if (BuildConfig.DEBUG) {
                    FacebookSdk.setIsDebugEnabled(true)
                    FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS)
                }


                // Facebook Email
                if (jsonObject.has("email")) {
                    val facebookEmail = jsonObject.getString("email")
                    viewModel.socialLoginUser(facebookEmail, "facebook", token!!.token)
                } else {
                    SnackBarUtil.showSnackBar(
                        requireContext(),
                        "Something went wrong. Please try again",
                        true
                    )
                }
            }).executeAsync()
    }
    //endregion

    //region Firebase Auth Google/Facebook
    private fun setupFirebaseAuth() {
        // [START config_signin]
        // Configure Google Sign In
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(requireContext(), gso)
        // [END config_signin]


        // [START initialize_auth]
        // Initialize Firebase Auth
        auth = Firebase.auth
        // [END initialize_auth]

    }

    // [START onactivityresult]

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //      super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)!!
                Log.d(TAG, "firebaseAuthWithGoogle:" + account.id)
                firebaseAuthWithGoogle(account.idToken!!)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e)
            }
        }
    }
    // [END onactivityresult]

    // [START auth_with_google]
    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(requireActivity()) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")
                    socialLoginUser(auth.currentUser)

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithCredential:failure", task.exception)

                }
            }
    }

    private fun socialLoginUser(currentUser: FirebaseUser?) {

        if (currentUser != null) {
            viewModel.socialLoginUser(
                currentUser.email.toString(),
                "google",
                currentUser.providerId
            )
        } else {
            SnackBarUtil.showSnackBar(
                requireContext(),
                "Something went wrong. Please try again",
                true
            )
        }
    }
    // [END auth_with_google]

    //endregion

    //region PROGRESS BAR
    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }

    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }
    //endregion

    companion object {
        private const val TAG = "RegisterFragment"
        private const val RC_SIGN_IN = 9002

        @JvmStatic
        fun newInstance() =
            RegisterFragment()
    }


}