package com.nfc.tapmee.ui.activities

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.nfc.tapmee.R
import com.nfc.tapmee.base.BaseActivity
import com.nfc.tapmee.base.util.FragmentUtil
import com.nfc.tapmee.databinding.ActivityActivationBinding
import com.nfc.tapmee.ui.activation.ChooseProductFragment

class ActivationActivity : BaseActivity() {
    private lateinit var binding: ActivityActivationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_activation)
        init()
    }

    private fun init() {

        FragmentUtil(this).replaceBaseFragment(
            ChooseProductFragment.newInstance(),
            R.id.flActivation
        )

    }
}