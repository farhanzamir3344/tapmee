package com.nfc.tapmee.ui.profile

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.nfc.tapmee.R
import com.nfc.tapmee.arch.providers.profile.ProfileProviderFactory
import com.nfc.tapmee.arch.repositories.profile.ProfileRepository
import com.nfc.tapmee.arch.viewmodels.profile.ProfileViewModel
import com.nfc.tapmee.base.BaseFragment
import com.nfc.tapmee.base.util.FragmentUtil
import com.nfc.tapmee.common.models.CustomerDTO
import com.nfc.tapmee.common.preferences.PreferenceUtils
import com.nfc.tapmee.common.utils.Constants
import com.nfc.tapmee.common.utils.DateUtils
import com.nfc.tapmee.common.utils.KeyboardOp
import com.nfc.tapmee.common.utils.alerts.SnackBarUtil
import com.nfc.tapmee.common.utils.dialog.gender.GenderListener
import com.nfc.tapmee.common.utils.dialog.gender.GenderSelectionDialog
import com.nfc.tapmee.common.utils.generics.Resource
import com.nfc.tapmee.common.utils.views.EditTextAutoSizeUtility
import com.nfc.tapmee.databinding.FragmentEditProfileBinding
import com.nfc.tapmee.ui.activities.AddMeeHelpActivity
import com.nfc.tapmee.ui.activities.DashboardActivity
import com.nfc.tapmee.ui.profile.adapter.ProfileAdapter
import kotlinx.android.synthetic.main.fragment_edit_profile.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class ProfileFragment private constructor() : BaseFragment() {


    private lateinit var binding: FragmentEditProfileBinding
    private lateinit var viewModel: ProfileViewModel
    private var customerDTO: CustomerDTO? = null
    private var isRedirect = false

    private lateinit var dobDatePicker: DatePickerDialog

    private lateinit var socialMediaAdapter: ProfileAdapter
    private lateinit var contactAdapter: ProfileAdapter
    private lateinit var musicAdapter: ProfileAdapter
    private lateinit var paymentAdapter: ProfileAdapter
    private lateinit var otherAdapter: ProfileAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_edit_profile,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listeners()
        initViewModel()
        handleResponse()

    }

    //region VIEW-MODEL
    private fun initViewModel() {

        val viewModelProviderFactory =
            ProfileProviderFactory(requireActivity().application, ProfileRepository())
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory).get(ProfileViewModel::class.java)

        viewModel.getProfile()

    }
    //endregion

    //region HELPING METHODS
    private fun renderProfile() {

        socialMediaRV()
        contactRV()
        musicRV()
        paymentRV()
        otherRV()

        if (customerDTO != null) {

            EditTextAutoSizeUtility.setupAutoResize(binding.tvProfileName, requireContext())

            if (customerDTO!!.name.isNullOrEmpty() ||
                customerDTO!!.name == "null"
            )
                binding.tvProfileName.setText(customerDTO!!.username)
            else
                binding.tvProfileName.setText(customerDTO!!.name.toString())

            binding.tvProfileLink.text = "${Constants.APP_URL}${customerDTO!!.username}"

            if (!customerDTO!!.banner.isNullOrEmpty()) {

                Glide.with(requireActivity())  //2
                    .load(customerDTO!!.banner)
                    .into(binding.ivBanner)

            } else {
                Glide.with(requireActivity())  //2
                    .load(R.drawable.ic_profile_placeholder)
                    .into(binding.ivBanner)
            }

            binding.svProfile.isChecked = customerDTO!!.is_public == 1

            if (!customerDTO!!.bio.isNullOrEmpty())
                binding.etBio.setText(customerDTO!!.bio.toString())

            if (!customerDTO!!.dob.isNullOrEmpty())
                binding.btnDob.text = DateUtils.toFormat(
                    customerDTO!!.dob.toString(),
                    DateUtils.DATE_FORMAT_12,
                    DateUtils.DATE_FORMAT_11
                )

            if (!PreferenceUtils.isNewUser()) {
                when (customerDTO!!.gender) {
                    1 -> binding.btnGender.text = getString(R.string.lbl_option_male)
                    2 -> binding.btnGender.text = getString(R.string.lbl_option_female)
                    3 -> binding.btnGender.text = getString(R.string.lbl_option_prefer_not_to_say)
                    4 -> binding.btnGender.text = getString(R.string.lbl_option_custom)
                    else -> {
                        binding.btnGender.text = getString(R.string.lbl_heading_your_gender)
                    }
                }
            }

        }


    }

    private fun listeners() {

        binding.ivBack.setOnClickListener {
            handleRedirect()
        }

        binding.ivEditName.setOnClickListener {
            binding.tvProfileName.isEnabled = true
            binding.tvProfileName.requestFocus()
            binding.tvProfileName.setSelection(binding.tvProfileName.length())
            KeyboardOp.show(requireActivity(), binding.tvProfileName)
        }

        binding.tvProfileName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
            override fun afterTextChanged(p0: Editable?) {
                val name = binding.tvProfileName.text.toString()

                if (name.isNotEmpty()) {
                    binding.tvProfileNameDisplay.text = name
                }
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })

        binding.etBio.setOnTouchListener { view, event ->
            view.parent.requestDisallowInterceptTouchEvent(true)
            if ((event.action and MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                view.parent.requestDisallowInterceptTouchEvent(false)
            }
            return@setOnTouchListener false
        }

        //Handle Profile public/Lock here
        binding.svProfile.setOnCheckedChangeListener { switchView, isChecked ->

            try {
                isRedirect = false

                val gender = binding.btnGender.text.toString()
                var dob = binding.btnDob.text.toString()
                val bio = binding.etBio.text.toString()

                var genderINT = 0

                when (gender) {
                    getString(R.string.lbl_option_male) -> genderINT = 1
                    getString(R.string.lbl_option_female) -> genderINT = 2
                    getString(R.string.lbl_option_prefer_not_to_say) -> genderINT = 3
                    getString(R.string.lbl_option_custom) -> genderINT = 4
                }

                when {
                    dob.isEmpty() -> {
                        dob = ""
                    }
                    dob == getString(R.string.lbl_btn_text_dob) -> {
                        dob = ""
                    }
                    else -> {
                        DateUtils.toFormat(dob, DateUtils.DATE_FORMAT_11, DateUtils.DATE_FORMAT_12)
                    }
                }


                if (isChecked)
                    viewModel.updateProfile(customerDTO!!.name!!, bio, genderINT, 1, dob)
                else
                    viewModel.updateProfile(customerDTO!!.name!!, bio, genderINT, 0, dob)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

        }

        //Handle keyboard submit to trigger
        binding.tvProfileName.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
                if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {

                    binding.tvProfileName.isEnabled = false
                    binding.tvProfileName.clearFocus()

                    return true
                }
                return false
            }
        })

        //handle Gender
        binding.btnGender.setOnClickListener {

            val genderSelectionDialog = GenderSelectionDialog.newInstance(
                binding.btnGender.text.toString(),
                object : GenderListener {
                    override fun onGenderSelected(gender: String) {
                        binding.btnGender.text = gender
                    }
                }
            )

            val ft: FragmentTransaction =
                requireActivity().supportFragmentManager.beginTransaction()
            ft.add(genderSelectionDialog!!, "genderSelectionDialog")
            ft.commitAllowingStateLoss()

        }

        //Handle Date of birth
        handleDob()

        //Select Profile Banner
        binding.ivImagePicker.setOnClickListener{
            showImagePicker()
        }
        binding.ivBanner.setOnClickListener {
            showImagePicker()
        }


        //Handle Save Profile
        binding.btnSaveProfile.setOnClickListener {
            val name = binding.tvProfileName.text.toString()
            val gender = binding.btnGender.text.toString()
            var dob = binding.btnDob.text.toString()
            val bio = binding.etBio.text.toString()
            var isPublic = 1

            var genderINT = 0

            when (gender) {
                getString(R.string.lbl_option_male) -> genderINT = 1
                getString(R.string.lbl_option_female) -> genderINT = 2
                getString(R.string.lbl_option_prefer_not_to_say) -> genderINT = 3
                getString(R.string.lbl_option_custom) -> genderINT = 4
            }



            isPublic = if (binding.svProfile.isChecked)
                1
            else
                0

            dob = if (dob.isEmpty() ||
                dob == getString(R.string.lbl_btn_text_dob)
            )
                ""
            else
                DateUtils.toFormat(dob, DateUtils.DATE_FORMAT_11, DateUtils.DATE_FORMAT_12)

            //Call Update Profile API
            isRedirect = true
            viewModel.updateProfile(
                name = name,
                gender = genderINT,
                dob = dob,
                bio = bio,
                isPublic = isPublic
            )
        }

        //handle Contact Card
        binding.contactCardView.setOnClickListener {

            FragmentUtil(requireActivity() as AppCompatActivity).gotoNextFragment(
                ContactCardFragment.newInstance(customerDTO = customerDTO),
                R.id.flEditProfile
            )
        }

    }


    private fun showImagePicker(){
        ImagePicker.with(this)
            .crop(9F, 16F)
            .compress(1024)
            .galleryOnly()
            .galleryMimeTypes(  //Exclude gif images
                mimeTypes = arrayOf(
                    "image/png",
                    "image/jpg",
                    "image/jpeg"
                )
            )
            .start { resultCode, data ->
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        //Image Uri will not be null for RESULT_OK
                        val fileUri = data?.data


                        binding.ivBanner.setImageURI(fileUri)

                        //You can get File object from intent
                        val file: File = ImagePicker.getFile(data)!!

                        //You can also get File Path from intent
                        val filePath: String? = ImagePicker.getFilePath(data)

                        uploadBanner(file)
                    }
                    ImagePicker.RESULT_ERROR -> {
                        SnackBarUtil.showSnackBar(
                            requireContext(),
                            ImagePicker.getError(data),
                            true
                        )
                    }

                }
            }
    }

    private fun handleDob() {
        val dobDateCalendar = Calendar.getInstance()

        val dobDatePickerListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                dobDateCalendar[Calendar.YEAR] = year
                dobDateCalendar[Calendar.MONTH] = monthOfYear
                dobDateCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth

//                val month = monthOfYear + 1

                val calendar = Calendar.getInstance()
                calendar.set(year, monthOfYear, dayOfMonth)

                val format = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
                val selectedDate: String = format.format(calendar.time)

                binding.btnDob.text = selectedDate
            }

        dobDatePicker = DatePickerDialog(
            requireContext(),
            dobDatePickerListener,
            dobDateCalendar[Calendar.YEAR],
            dobDateCalendar[Calendar.MONTH],
            dobDateCalendar[Calendar.DAY_OF_MONTH]
        )

        dobDatePicker.datePicker.maxDate = System.currentTimeMillis()

        //Select Date of Birth
        binding.btnDob.setOnClickListener {
            dobDatePicker.show()
        }
    }

    private fun uploadBanner(file: File) {

        // create RequestBody instance from file
        val requestFile: RequestBody = file.asRequestBody("multipart/form-data".toMediaTypeOrNull())

        val body: MultipartBody.Part = MultipartBody.Part.createFormData(
            "banner",
            file.name,
            requestFile
        )

        viewModel.updateProfileBanner(file = body)
    }
    //endregion

    //region RENDER LISTS

    private fun socialMediaRV() {

        socialMediaAdapter = ProfileAdapter()
        rvSocialMediaProfiles.apply {
            layoutManager =
                GridLayoutManager(requireActivity(), 3, GridLayoutManager.VERTICAL, false)
//            addItemDecoration(SpacesItemDecoration(5))
            adapter = socialMediaAdapter
        }

        socialMediaAdapter.setOnItemClickListener {

            if (it.isPro == 0) {
                FragmentUtil(requireActivity() as AppCompatActivity).gotoNextFragment(
                    ProfileDetailFragment.newInstance(profileX = it),
                    R.id.flEditProfile
                )
            } else {
                //Handle Subscription here
            }

        }

    }

    private fun contactRV() {

        contactAdapter = ProfileAdapter()
        rvContactProfiles.apply {
            layoutManager =
                GridLayoutManager(requireActivity(), 3, GridLayoutManager.VERTICAL, false)
//            addItemDecoration(SpacesItemDecoration(5))
            adapter = contactAdapter
        }

        contactAdapter.setOnItemClickListener {
            if (it.isPro == 0) {
                FragmentUtil(requireActivity() as AppCompatActivity).gotoNextFragment(
                    ProfileDetailFragment.newInstance(profileX = it),
                    R.id.flEditProfile
                )
            } else {
                //Handle Subscription here
            }
        }

    }

    private fun musicRV() {

        musicAdapter = ProfileAdapter()
        rvMusicProfiles.apply {
            layoutManager =
                GridLayoutManager(requireActivity(), 3, GridLayoutManager.VERTICAL, false)
//            addItemDecoration(SpacesItemDecoration(5))
            adapter = musicAdapter
        }

        musicAdapter.setOnItemClickListener {
            if (it.isPro == 0) {
                FragmentUtil(requireActivity() as AppCompatActivity).gotoNextFragment(
                    ProfileDetailFragment.newInstance(profileX = it),
                    R.id.flEditProfile
                )
            } else {
                //Handle Subscription here
            }
        }

    }

    private fun paymentRV() {

        paymentAdapter = ProfileAdapter()
        rvPaymentProfiles.apply {
            layoutManager =
                GridLayoutManager(requireActivity(), 3, GridLayoutManager.VERTICAL, false)
//            addItemDecoration(SpacesItemDecoration(5))
            adapter = paymentAdapter
        }

        paymentAdapter.setOnItemClickListener {
            if (it.isPro == 0) {
                FragmentUtil(requireActivity() as AppCompatActivity).gotoNextFragment(
                    ProfileDetailFragment.newInstance(profileX = it),
                    R.id.flEditProfile
                )
            } else {
                //Handle Subscription here
            }
        }

    }

    private fun otherRV() {

        otherAdapter = ProfileAdapter()
        rvMoreProfiles.apply {
            layoutManager =
                GridLayoutManager(requireActivity(), 3, GridLayoutManager.VERTICAL, false)
//            addItemDecoration(SpacesItemDecoration(5))
            adapter = otherAdapter
        }

        otherAdapter.setOnItemClickListener {
            if (it.isPro == 0) {
                FragmentUtil(requireActivity() as AppCompatActivity).gotoNextFragment(
                    ProfileDetailFragment.newInstance(profileX = it),
                    R.id.flEditProfile
                )
            } else {
                //Handle Subscription here
            }
        }

    }
    //endregion

    //region HANDLE API RESPONSE
    private fun handleResponse() {
        viewModel.profileLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    customerDTO = response.data!!

                    renderProfile()

                    //Get All Social Profile
                    viewModel.getAllSocialProfile()
                }
                is Resource.Error -> {
                    hideProgressBar()
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })

        viewModel.profilesLiveData.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    response.data?.let { responseData ->

                        for (profile in responseData) {
                            if (profile.title == "Social Media") {
                                if (profile.profiles!!.isNotEmpty()) {
                                    socialMediaAdapter.differ.submitList(profile.profiles)
                                } else {
                                    binding.socialMediaContainer.visibility = View.GONE
                                }
                            } else if (profile.title == "Contact") {
                                if (profile.profiles!!.isNotEmpty()) {

                                    val profileList =
                                        profile.profiles.filter { it.profileCode != "contact-card" }

                                    contactAdapter.differ.submitList(profileList)
                                    binding.contactCardView.visibility = View.VISIBLE
                                } else {
                                    binding.contactCardView.visibility = View.GONE
                                    binding.contactContainer.visibility = View.GONE
                                }
                            } else if (profile.title == "Music") {
                                if (profile.profiles!!.isNotEmpty()) {
                                    musicAdapter.differ.submitList(profile.profiles)
                                } else {
                                    binding.musicContainer.visibility = View.GONE
                                }
                            } else if (profile.title == "Payment") {
                                if (profile.profiles!!.isNotEmpty()) {
                                    paymentAdapter.differ.submitList(profile.profiles)
                                } else {
                                    binding.paymentContainer.visibility = View.GONE
                                }
                            } else if (profile.title == "Other") {
                                if (profile.profiles!!.isNotEmpty()) {
                                    otherAdapter.differ.submitList(profile.profiles)
                                } else {
                                    binding.moreContainer.visibility = View.GONE
                                }
                            }
                        }


                    }

                }
                is Resource.Error -> {
                    hideProgressBar()
                    SnackBarUtil.showSnackBar(requireContext(), response.message!!, true)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        }


        viewModel.updateProfileLiveData.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    response.data?.let {

                        hideProgressBar()

                        if (isRedirect) {
                            handleRedirect()
                        }


                    }

                }
                is Resource.Error -> {
                    hideProgressBar()
                    SnackBarUtil.showSnackBar(requireContext(), response.message!!, true)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        }


        viewModel.profileBannerLiveData.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    response.data?.let {

                        hideProgressBar()

                    }

                }
                is Resource.Error -> {
                    hideProgressBar()
                    SnackBarUtil.showSnackBar(requireContext(), response.message!!, true)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        }

        viewModel.updateBasicProfileLiveData.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Success -> {
                    customerDTO = response.data!!

                }
                is Resource.Error -> {
                }
                is Resource.Loading -> {
                }
            }
        }
    }

    private fun handleRedirect() {
        if (PreferenceUtils.isNewUser()) {


            //Redirect How to AddMee Activity
            startActivity(
                Intent(
                    requireActivity(),
                    AddMeeHelpActivity::class.java
                )
            )
        } else {
            startActivity(
                Intent(
                    requireActivity(),
                    DashboardActivity::class.java
                )
            )
        }

        requireActivity().finish()
    }
    //endregion

    //region PROGRESS BAR
    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }
    //endregion

    companion object {

        @JvmStatic
        fun newInstance(): Fragment {

            return ProfileFragment()
        }

    }
}