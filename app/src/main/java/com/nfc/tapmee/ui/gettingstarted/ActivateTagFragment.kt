package com.nfc.tapmee.ui.gettingstarted

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.nfc.tapmee.R
import com.nfc.tapmee.base.BaseFragment
import com.nfc.tapmee.databinding.FragmentActivateTagBinding
import com.nfc.tapmee.ui.activities.ActivationActivity

class ActivateTagFragment : BaseFragment() {
    private lateinit var binding: FragmentActivateTagBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_activate_tag,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnActivate.setOnClickListener {

            startActivity(
                Intent(
                    requireActivity(),
                    ActivationActivity::class.java
                )
            )
            requireActivity().finish()
        }

        binding.ivCloseScreen.setOnClickListener {

            startActivity(
                Intent(
                    requireActivity(),
                    ActivationActivity::class.java
                )
            )
            requireActivity().finish()
        }
    }
}