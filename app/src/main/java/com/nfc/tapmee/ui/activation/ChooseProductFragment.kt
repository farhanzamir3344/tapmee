package com.nfc.tapmee.ui.activation

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.nfc.tapmee.R
import com.nfc.tapmee.base.BaseActivity
import com.nfc.tapmee.base.BaseFragment
import com.nfc.tapmee.base.util.FragmentUtil
import com.nfc.tapmee.common.preferences.PreferenceUtils
import com.nfc.tapmee.common.utils.Constants
import com.nfc.tapmee.databinding.FragmentChooseProductBinding
import com.nfc.tapmee.ui.activities.DashboardActivity
import java.util.*

class ChooseProductFragment private constructor() : BaseFragment() {

    private lateinit var binding: FragmentChooseProductBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_choose_product, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listeners()
    }

    private fun listeners() {

        binding.ivCloseScreen.setOnClickListener {
            if (PreferenceUtils.isNewUser()) {
                PreferenceUtils.saveNewUser(false)

                startActivity(
                    Intent(
                        requireActivity(),
                        DashboardActivity::class.java
                    )
                )
            }

            requireActivity().finish()


        }

        binding.nfcBuy.setOnClickListener {

            (requireActivity() as BaseActivity).gotoWeb(
                Constants.BUY_NFC,
                getString(R.string.lbl_heading_buy_nfc)
            )
        }

        binding.productNfcChip.setOnClickListener {
            FragmentUtil(requireActivity() as AppCompatActivity).gotoNextFragment(
                ProductActivationFragment.newInstance("Button"),
                R.id.flActivation
            )
        }

        binding.productKeychain.setOnClickListener {
            FragmentUtil(requireActivity() as AppCompatActivity).gotoNextFragment(
                ProductActivationFragment.newInstance("Keychain"),
                R.id.flActivation
            )
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            ChooseProductFragment()
    }
}