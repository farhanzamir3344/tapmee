package com.nfc.tapmee.ui.profile.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nfc.tapmee.R
import com.nfc.tapmee.common.models.Profile
import kotlinx.android.synthetic.main.layout_item_contact_card.view.*


class ContactCardAdapter :
    RecyclerView.Adapter<ContactCardAdapter.ContactViewHolder>() {

    inner class ContactViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val differConfig = object : DiffUtil.ItemCallback<Profile>() {

        override fun areItemsTheSame(oldItem: Profile, newItem: Profile): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Profile, newItem: Profile): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differConfig)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        return ContactViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.layout_item_contact_card,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val profile = differ.currentList[position]
        holder.itemView.apply {


            tvProfileName.text = profile.title
            tvProfileDesc.text = profile.profile_link_value

            Glide.with(context)  //2
                .load(profile.icon)
                .into(ivContactThumb)

            svContactCard.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked)
                    differ.currentList[position].added_to_contact_card = "yes"
                else
                    differ.currentList[position].added_to_contact_card = "no"

            }

            svContactCard.isChecked = profile.added_to_contact_card == "yes"


            setOnClickListener {
                onItemClickListener?.let { it(profile) }
            }


        }
    }


    override fun getItemCount(): Int {
        return differ.currentList.size
    }


    private var onItemClickListener: ((Profile) -> Unit)? = null

    fun setOnItemClickListener(listener: (Profile) -> Unit) {
        onItemClickListener = listener
    }


}