package com.nfc.tapmee.ui.activities

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.nfc.tapmee.R
import com.nfc.tapmee.base.BaseActivity
import com.nfc.tapmee.base.util.FragmentUtil
import com.nfc.tapmee.databinding.ActivitySettingsBinding
import com.nfc.tapmee.ui.settings.SettingsFragment

class SettingsActivity : BaseActivity() {
    private lateinit var binding: ActivitySettingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_settings)
        init()
    }

    private fun init() {

        FragmentUtil(this).replaceBaseFragment(
            SettingsFragment.newInstance(),
            R.id.flSettings
        )

    }
}