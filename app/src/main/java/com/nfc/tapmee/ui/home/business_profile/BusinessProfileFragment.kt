package com.nfc.tapmee.ui.home.business_profile

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.nfc.tapmee.R
import com.nfc.tapmee.arch.repositories.dashboard.DashboardRepository
import com.nfc.tapmee.common.database.AddMeeDatabase
import com.nfc.tapmee.common.models.BusinessInfo
import com.nfc.tapmee.common.models.CustomerDTO
import com.nfc.tapmee.databinding.FragmentBusinessProfileBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class BusinessProfileFragment : Fragment() {

    private lateinit var binding: FragmentBusinessProfileBinding

    private lateinit var dashboardRepository: DashboardRepository
    private var customerDTO: CustomerDTO? = null
    private var businessDTO: BusinessInfo? = null
    private val uiScope = CoroutineScope(Dispatchers.Main)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_business_profile,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
    }

    //region VIEW-MODEL
    private fun initViewModel() {
        val customerDao = AddMeeDatabase(requireActivity()).customerDAO
        val businessDao = AddMeeDatabase(requireActivity()).businessInfoDAO
        dashboardRepository = DashboardRepository(customerDao, businessDao)

        if (customerDTO == null)
            getCustomerInfo()

    }
    //endregion

    //region HELPING METHODS
    private fun getCustomerInfo() {

        //Fetch CustomerInfo from Room
        uiScope.launch {

            customerDTO = dashboardRepository.getCustomerInfo()
            businessDTO = dashboardRepository.getBusinessInfo()

            withContext(Dispatchers.Main) {

                if (customerDTO != null)
                    renderCustomerDetails()

                if (businessDTO != null)
                    renderBusinessDetails()
            }
        }
    }


    private fun renderCustomerDetails() {


        binding.tvProfileName.text = customerDTO!!.name
        binding.tvProfileLink.text = "addmee.app/${customerDTO!!.username}"

        updateOpenDirect()
//        listeners()
    }


    private fun renderBusinessDetails() {

        binding.containerNoBanner.visibility = View.VISIBLE
        binding.ivBanner.visibility = View.GONE

    }

    private fun updateOpenDirect() {

        if (customerDTO!!.open_direct == 0) {
            binding.btnDirectSwitch.backgroundTintList =
                ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.white))
            binding.btnDirectSwitch.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.black
                )
            )
            binding.btnDirectSwitch.text = getString(R.string.lbl_direct_off)
        } else {
            binding.btnDirectSwitch.backgroundTintList =
                ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.black))
            binding.btnDirectSwitch.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.white
                )
            )
            binding.btnDirectSwitch.text = getString(R.string.lbl_direct_on)
        }
    }
    //endregion


    companion object {
        @JvmStatic
        fun newInstance() =
            BusinessProfileFragment()
    }
}