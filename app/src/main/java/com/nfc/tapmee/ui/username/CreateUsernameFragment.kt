package com.nfc.tapmee.ui.username

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nfc.tapmee.R
import com.nfc.tapmee.arch.providers.username.UsernameProviderFactory
import com.nfc.tapmee.arch.repositories.username.UsernameRepository
import com.nfc.tapmee.arch.viewmodels.username.UsernameViewModel
import com.nfc.tapmee.base.BaseFragment
import com.nfc.tapmee.common.preferences.PreferenceUtils
import com.nfc.tapmee.common.preferences.SharedPreferences
import com.nfc.tapmee.common.utils.Constants
import com.nfc.tapmee.common.utils.dialog.AlertOP
import com.nfc.tapmee.common.utils.generics.Resource
import com.nfc.tapmee.common.utils.views.InputFilterUtils
import com.nfc.tapmee.databinding.FragmentCreateUsernameBinding
import com.nfc.tapmee.ui.activities.EditProfileActivity
import java.util.*


class CreateUsernameFragment private constructor() : BaseFragment() {

    lateinit var binding: FragmentCreateUsernameBinding
    private lateinit var viewModel: UsernameViewModel

    private var isValidUsername = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_create_username, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        initViewModel()
        handleApiResponse()
    }

    //region VIEW-MODEL
    private fun initViewModel() {


        val viewModelProviderFactory =
            UsernameProviderFactory(requireActivity().application, UsernameRepository())
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory)[UsernameViewModel::class.java]
    }
    //endregion

    //region HELPING METHODS
    private fun init() {

        //Disable special characters
        InputFilterUtils.setEditTextInhibitInputSpaChat(binding.etUsername)

        //Disable uppercase characters
//        binding.etUsername.filters = arrayOf<InputFilter>(object : InputFilter.AllCaps() {
//            override fun filter(
//                source: CharSequence?,
//                start: Int,
//                end: Int,
//                dest: Spanned?,
//                dstart: Int,
//                dend: Int
//            ) =
//                source.toString().toLowerCase()
//        })

        //call VerifyUsernameAPI every time user enters anything
        verifyUsername()

        //Create Username API
        binding.btnContinue.setOnClickListener {

            val username = binding.etUsername.text!!.trim().toString()

            if (isValidUsername && username.isNotEmpty())
                viewModel.updateUsername(username = username)
        }

    }

    private fun verifyUsername() {

        binding.etUsername.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                val username = binding.etUsername.text.toString()

                if (username.isNotEmpty()) {
                    binding.usernameLinkContainer.visibility = View.VISIBLE
                    binding.tvAvailability.visibility = View.VISIBLE
                    binding.tvUsernameLink.text = "${getString(R.string.lbl_my_url)}${username.lowercase(Locale.getDefault())}"
                    viewModel.verifyUsername(username.lowercase(Locale.getDefault()))
                } else {
                    binding.usernameLinkContainer.visibility = View.GONE
                    binding.tvAvailability.visibility = View.GONE
                    binding.btnContinue.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.bg_btn_disabled)
                    isValidUsername = false
                }
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })

    }
    //endregion

    //region API HANDLE RESPONSE
    private fun handleApiResponse() {

        viewModel.usernameLiveData.observe(viewLifecycleOwner) { response ->
            when (response) {
                is Resource.Success -> {
                    isValidUsername = true

                    binding.tvAvailability.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.valid_username
                        )
                    )
                    binding.tvAvailability.text = getString(R.string.lbl_txt_available)
                    binding.tvAvailability.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_valid_username,
                        0,
                        0,
                        0
                    )
                    binding.btnContinue.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.bg_btn_enabled)
                }
                is Resource.Error -> {
                    isValidUsername = false

                    binding.tvAvailability.setTextColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.invalid_username
                        )
                    )
                    binding.tvAvailability.text = getString(R.string.lbl_txt_invalid)
                    binding.tvAvailability.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        0,
                        R.drawable.ic_invalid_username,
                        0
                    )
                }
                is Resource.Loading -> {

                }
            }
        }


        viewModel.updateUsernameLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    SharedPreferences.getInstance()
                        .save(Constants.USERNAME, binding.etUsername.text!!.trim().toString())

                    SharedPreferences.getInstance().save(Constants.IS_REGISTER_CUSTOMER, false)
                    PreferenceUtils.saveIsLoginCustomer()

                    PreferenceUtils.saveNewUser(true)

                    startActivity(
                        Intent(
                            requireActivity(),
                            EditProfileActivity::class.java
                        )
                    )
                    requireActivity().finish()

                }
                is Resource.Error -> {
                    hideProgressBar()
                    AlertOP.showResponseAlertOK(requireContext(), "Alert", response.message)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })
    }
    //endregion

    //region PROGRESS BAR
    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }

    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }
    //endregion

    companion object {
        @JvmStatic
        fun newInstance() =
            CreateUsernameFragment()
    }
}