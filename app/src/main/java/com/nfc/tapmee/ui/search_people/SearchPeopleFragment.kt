package com.nfc.tapmee.ui.search_people

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nfc.tapmee.R
import com.nfc.tapmee.arch.providers.contact.ContactProviderFactory
import com.nfc.tapmee.arch.repositories.contact.ContactRepository
import com.nfc.tapmee.arch.viewmodels.contact.ContactViewModel
import com.nfc.tapmee.common.models.UserNote
import com.nfc.tapmee.common.utils.AddmeeRcAdaptor
import com.nfc.tapmee.common.utils.ItemLayoutManger
import com.nfc.tapmee.common.utils.alerts.SnackBarUtil
import com.nfc.tapmee.common.utils.generics.Resource
import kotlinx.android.synthetic.main.fragment_search_people.*


class SearchPeopleFragment private constructor() : Fragment(), ItemLayoutManger<UserNote> {

    private lateinit var viewModel: ContactViewModel

    private val adapter: AddmeeRcAdaptor<UserNote> by lazy {
        AddmeeRcAdaptor(requireContext(), this)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            SearchPeopleFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate( R.layout.fragment_search_people, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupList()
        initViewModel()
        handleResponse()

    }

    //region VIEW-MODEL
    private fun initViewModel() {

        val viewModelProviderFactory =
            ContactProviderFactory(requireActivity().application, ContactRepository())
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory).get(ContactViewModel::class.java)

        viewModel.getContactsList()

    }
    //endregion

    //region HELPING METHODS
    private fun setupList() {
        adapter.bindRecyclerView(rvContactsList)
    }


    override fun getRowLayoutId(position: Int): Int {
        return R.layout.layout_search_people_row
    }

    override fun bindRowView(view: View, item: UserNote) {
        val contactName = view.findViewById<TextView>(R.id.tvContactName)
        val contactNote = view.findViewById<TextView>(R.id.tvNote)
        val contactEmail = view.findViewById<TextView>(R.id.tvContactEmail)
        val contactMobileNumber = view.findViewById<TextView>(R.id.tvContactMobile)

        try {
            contactName.text = item.name
            contactNote.text = item.note
            contactEmail.text = item.email
            contactMobileNumber.text = item.phoneNo
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }
    //endregion

    //region HANDLE RESPONSE
    private fun handleResponse() {
        viewModel.notesLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    response.data?.let { response ->

                        hideProgressBar()

                        adapter.setItems(response)

                    }

                }
                is Resource.Error -> {
                    hideProgressBar()
                    SnackBarUtil.showSnackBar(requireContext(), response.message!!, true)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })
    }
    //endregion

    //region PROGRESS BAR
    private fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }
    //endregion

}