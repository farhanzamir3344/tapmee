package com.nfc.tapmee.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.RadioGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.nfc.tapmee.BuildConfig
import com.nfc.tapmee.R
import com.nfc.tapmee.arch.providers.dashboard.DashboardProviderFactory
import com.nfc.tapmee.arch.repositories.dashboard.DashboardRepository
import com.nfc.tapmee.arch.viewmodels.dashboard.DashboardViewModel
import com.nfc.tapmee.base.BaseActivity
import com.nfc.tapmee.base.util.FragmentUtil
import com.nfc.tapmee.common.database.AddMeeDatabase
import com.nfc.tapmee.common.models.CustomerDTO
import com.nfc.tapmee.common.utils.AppOP
import com.nfc.tapmee.common.utils.Constants
import com.nfc.tapmee.common.utils.dialog.AlertOP
import com.nfc.tapmee.common.utils.dialog.GeneralDialogListener
import com.nfc.tapmee.common.utils.generics.Resource
import com.nfc.tapmee.ui.home.HomeFragment
import com.nfc.tapmee.ui.nfc.NfcReadActivity
import com.nfc.tapmee.ui.qr_code.QrBottomSheetFragment
import com.nfc.tapmee.ui.search_people.SearchPeopleFragment
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.layout_item_profile.view.*
import vn.luongvo.widget.iosswitchview.SwitchView

class DashboardActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener,
    BottomNavigationView.OnNavigationItemSelectedListener {

    private lateinit var viewModel: DashboardViewModel

    private var customerDTO: CustomerDTO? = null

    private var pbar: RelativeLayout? = null
    private lateinit var profileOnOff: SwitchView
    private lateinit var tvUsername: TextView
    private lateinit var tvName: TextView
    private lateinit var ivProfilePicture: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        pbar = findViewById(R.id.progressBar)

        initViewModel()
        handleResponse()
    }

    //region VIEW-MODEL
    private fun initViewModel() {
        val customerDao = AddMeeDatabase(this).customerDAO
        val businessDao = AddMeeDatabase(this).businessInfoDAO
        val dashboardRepository = DashboardRepository(customerDao, businessDao)


        val viewModelProviderFactory =
            DashboardProviderFactory(application, dashboardRepository)
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory).get(DashboardViewModel::class.java)

        if (customerDTO == null)
            viewModel.getProfile()

    }
    //endregion

    //region HELPING METHODS
    private fun loadHomeFrag() {
        FragmentUtil(this).replaceBaseFragment(
            HomeFragment.newInstance(),
            R.id.dashboard_container
        )

    }

    private fun init() {

        tvVersionCode.text = getString(R.string.version_code, BuildConfig.VERSION_NAME)
        val headerView = nav_view.getHeaderView(0)
        tvUsername = headerView.findViewById<TextView>(R.id.tvUsername)
        tvName = headerView.findViewById<TextView>(R.id.tvName)
        profileOnOff = headerView.findViewById<SwitchView>(R.id.svProfile)
        ivProfilePicture = headerView.findViewById<ImageView>(R.id.ivProfileThumb)

        renderCustomerDetails()

        imgv_pro.setOnClickListener { //Show Subscription
        }

        ivShareApp.setOnClickListener {

            //share AddMee
            AppOP.shareApp(
                "Download AddMee now and start popping!\n",
                "https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}",
                this
            )
        }

        imv_hamburger.setOnClickListener { drawerLayout.openDrawer(GravityCompat.START) }
        nav_view.setNavigationItemSelectedListener(this)
        bottom_navigation.setOnNavigationItemSelectedListener(this)

        loadHomeFrag()
    }


    private fun renderCustomerDetails() {

        tvUsername.text = customerDTO!!.username
        tvName.text = customerDTO!!.name

        Glide.with(this)  //2
            .load(customerDTO!!.banner)
            .centerCrop()
            .placeholder(R.drawable.ic_profile_placeholder)
            .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
            .into(ivProfilePicture)

        profileOnOff.isChecked = customerDTO!!.is_public == 1

        profileOnOff.setOnCheckedChangeListener { _, checkedId -> changeProfileStatus(checkedId) }
    }


    private fun changeProfileStatus(isActive: Boolean) {

        val name = customerDTO!!.name.toString()
        val gender = customerDTO!!.gender

//        when (customerDTO!!.gender) {
//            1 -> gender = getString(R.string.lbl_option_male)
//            2 -> gender = getString(R.string.lbl_option_female)
//            3 -> gender = getString(R.string.lbl_option_prefer_not_to_say)
//            4 -> gender = getString(R.string.lbl_option_custom)
//        }

        val dob = customerDTO!!.dob.toString()
        val bio = customerDTO!!.bio.toString()

        if (isActive) {
            viewModel.updateProfile(name, bio, gender!!, 1, dob)
        } else {
            viewModel.updateProfile(name, bio, gender!!, 0, dob)
        }
    }


    private fun showQrBottomSheet() {
        val qrBotttomSheet = QrBottomSheetFragment.newInstance()
        qrBotttomSheet.show(supportFragmentManager, "")
    }

    //endregion


    //region API HANDLE RESPONSE
    private fun handleResponse() {

        viewModel.profileLiveData.observe(this, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideLoading()

                    customerDTO = response.data!!
                    viewModel.saveCustomerDto(response.data)

                    init()
                }
                is Resource.Error -> {
                    hideLoading()
                }
                is Resource.Loading -> {
                    showLoading()
                }
            }
        })

        viewModel.updateBasicProfileLiveData.observe(this, Observer { response ->
            when (response) {
                is Resource.Success -> {

                    customerDTO = response.data!!
                    viewModel.updateCustomerDto(response.data)

                    if (response.data.is_public == 0)
                        AlertOP.showResponseAlertOK(
                            this,
                            getString(R.string.lbl_profile_off),
                            getString(R.string.lbl_msg_profile_off)
                        )
                    else
                        AlertOP.showResponseAlertOK(
                            this,
                            getString(R.string.lbl_profile_on),
                            getString(R.string.lbl_msg_profile_on)
                        )


                }
                is Resource.Error -> {
                }
                is Resource.Loading -> {
                }
            }
        })


    }
    //endregion

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_person -> {


                if (FragmentUtil(this).getCurrentFragment(R.id.dashboard_container) !is HomeFragment) {
                    topBar.visibility = View.VISIBLE
                    FragmentUtil(this).replaceBaseFragment(
                        HomeFragment.newInstance(),
                        R.id.dashboard_container
                    )
                }

            }
            R.id.menu_nfc -> {
                startActivity(
                    Intent(
                        this,
                        NfcReadActivity::class.java
                    )
                )
//                showNfcDialog()
            }
            R.id.menu_qr_code -> {
                showQrBottomSheet()
            }
            R.id.menu_drawer_home -> {
                if (FragmentUtil(this).getCurrentFragment(R.id.dashboard_container) !is HomeFragment) {
                    topBar.visibility = View.VISIBLE
                    FragmentUtil(this).replaceBaseFragment(
                        HomeFragment.newInstance(),
                        R.id.dashboard_container
                    )
                }
            }
            R.id.menu_drawer_profile_edit_profile -> {
                startActivity(
                    Intent(
                        this,
                        EditProfileActivity::class.java
                    )
                )
                finish()
            }
            R.id.menu_people -> {

                if (FragmentUtil(this).getCurrentFragment(R.id.dashboard_container) !is SearchPeopleFragment) {
                    topBar.visibility = View.GONE
                    FragmentUtil(this).replaceBaseFragment(
                        SearchPeopleFragment.newInstance(),
                        R.id.dashboard_container
                    )
                }

            }
            R.id.menu_drawer_set_up_addmee -> {
                startActivity(
                    Intent(
                        this,
                        ActivationActivity::class.java
                    )
                )
            }
            R.id.menu_drawer_my_qr_code -> {
                showQrBottomSheet()
            }
            R.id.menu_drawer_buy_addmee -> {
                gotoWeb(Constants.BUY_NFC, getString(R.string.lbl_heading_buy_nfc))
            }
            R.id.menu_drawer_go_pro -> {
                startActivity(
                    Intent(
                        this,
                        AddMeeProActivity::class.java
                    )
                )
            }
            R.id.menu_drawer_how_to_pop -> {
                startActivity(
                    Intent(
                        this,
                        AddMeeHelpActivity::class.java
                    )
                )
            }
            R.id.menu_drawer_settings -> {
                startActivity(
                    Intent(
                        this,
                        SettingsActivity::class.java
                    )
                )
            }
            R.id.menu_drawer_logout -> {
                AlertOP.showAlert(
                    this,
                    getString(R.string.log_out),
                    getString(R.string.msg_logout),
                    getString(R.string.log_out),
                    getString(R.string.cancel),
                    object : GeneralDialogListener {
                        override fun onPositiveClick() {
                            AppOP.launchLoginActivityClearAll(this@DashboardActivity)
                        }

                        override fun onNegativeClick() {

                        }
                    })

            }
            else -> {
                return false
            }
        }

        drawerLayout.closeDrawer(GravityCompat.START)

        return true
    }

    //region GLOBAL PROGRESS BAR
    fun showLoading() {
        if (pbar != null)
            pbar!!.visibility = View.VISIBLE
    }

    fun hideLoading() {
        if (pbar != null)
            pbar!!.visibility = View.GONE
    }
    //endregion
}