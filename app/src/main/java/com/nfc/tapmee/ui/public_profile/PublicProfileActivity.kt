package com.nfc.tapmee.ui.public_profile

import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.github.furkankaplan.fkblurview.FKBlurView
import com.nfc.tapmee.R
import com.nfc.tapmee.arch.providers.profile.ProfileProviderFactory
import com.nfc.tapmee.arch.repositories.dashboard.DashboardRepository
import com.nfc.tapmee.arch.repositories.profile.ProfileRepository
import com.nfc.tapmee.arch.viewmodels.profile.ProfileViewModel
import com.nfc.tapmee.common.database.AddMeeDatabase
import com.nfc.tapmee.common.models.CustomerDTO
import com.nfc.tapmee.common.models.MyProfilesResponseDTO
import com.nfc.tapmee.common.utils.dialog.contact.ContactDialog
import com.nfc.tapmee.common.utils.generics.Resource
import com.nfc.tapmee.common.utils.profile.HandleOpenProfile
import com.nfc.tapmee.common.utils.views.SpacesItemDecoration
import com.nfc.tapmee.databinding.ActivityPublicProfileBinding
import com.nfc.tapmee.ui.public_profile.adapter.PublicProfileAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PublicProfileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPublicProfileBinding
    private lateinit var publicProfileAdapter: PublicProfileAdapter

    private lateinit var viewModel: ProfileViewModel
    private lateinit var profileRepository: ProfileRepository
    private lateinit var currentUserProfile: CustomerDTO

    private lateinit var blurViewContent: FKBlurView
    private lateinit var blurViewBanner: FKBlurView

    private var username: String? = null

    private val uiScope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_public_profile)

        blurViewContent = binding.fkBlurViewContent
        blurViewBanner = binding.fkBlurViewBanner

        if (intent.hasExtra("ExtraURL")) {
            val link = intent.getStringExtra("ExtraURL")

            username = link!!.substring(link.lastIndexOf("/") + 1)
        } else {
            val uri: Uri? = intent.data
            val url = uri.toString()

            if (url.contains("contact-card")) {
                val browserIntent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(url)
                )
                ContextCompat.startActivity(this, browserIntent, null)
            } else {
                username = url.substring(url.lastIndexOf("/") + 1)
            }

        }



        setupRecyclerView()
        initViewModel()
        handleResponse()

    }


    //region VIEW-MODEL
    private fun initViewModel() {
        val customerDAO = AddMeeDatabase(this).customerDAO

        profileRepository = ProfileRepository(customerDAO)

        binding.ivCloseScreen.setOnClickListener {
            finish()
        }

        val viewModelProviderFactory =
            ProfileProviderFactory(application, profileRepository)
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory).get(ProfileViewModel::class.java)

        if (!username.isNullOrEmpty())
            viewModel.getPublicProfiles(username = username!!)

        getCustomerInfo()

    }

    private fun getCustomerInfo() {

        //Fetch CustomerInfo from Room
        uiScope.launch {

            val customerDatabaseRecord = profileRepository.getCustomerInfo()

            withContext(Dispatchers.Main) {

                if (customerDatabaseRecord != null)
                    currentUserProfile = customerDatabaseRecord
            }
        }
    }

    //endregion

    //region HELPING METHODS
    private fun renderProfile(customerDTO: MyProfilesResponseDTO?) {

        if (customerDTO != null) {

            val user = customerDTO.user!!

            if (!user.banner.isNullOrEmpty()) {
                //  binding.containerNoBanner.visibility = View.GONE
//                binding.ivBanner.visibility = View.VISIBLE

                Glide.with(this)  //2
                    .load(user.banner)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            p0: GlideException?,
                            p1: Any?,
                            p2: Target<Drawable>?,
                            p3: Boolean,
                        ): Boolean {

                            return false
                        }

                        override fun onResourceReady(
                            p0: Drawable?,
                            p1: Any?,
                            p2: Target<Drawable>?,
                            p3: DataSource?,
                            p4: Boolean,
                        ): Boolean {
                            //do something when picture already loaded
                            if (user.is_public == 2) {
                                blurViewBanner.setBlur(this@PublicProfileActivity, blurViewBanner)
                            }
                            return false
                        }
                    })
                    .into(binding.ivBanner)


            }
//            else {
//                binding.containerNoBanner.visibility = View.VISIBLE
//                binding.ivBanner.visibility = View.GONE
//            }

            binding.tvProfileName.text = user.name
            binding.tvProfileLink.text = "addmee.app/${user.username}"

          //  if (!user.bio.isNullOrEmpty())
             //   binding.tvProfileBio.text = user.bio.toString()



            if (user.is_public == 1) {

                binding.profileOffContainer.visibility = View.GONE
                binding.btnContact.isEnabled = true

                if (user.open_direct == 0) {

                    val userProfiles = customerDTO.profiles.filter { it.is_business == 0 }

                    if (!userProfiles.isNullOrEmpty())
                        publicProfileAdapter.differ.submitList(userProfiles.toList())

                } else {
                    HandleOpenProfile.openProfileBaseURL(this, customerDTO.profiles[0])
                    finish()
                }

                binding.btnContact.setOnClickListener {

                    val contactDialog = ContactDialog.newInstance(
                        customerDTO = currentUserProfile,
                        contactPersonName = customerDTO.user.name.toString(),
                        otherPersonID = customerDTO.user.customerID!!
                    )

                    val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
                    ft.add(contactDialog!!, "contactDialog")
                    ft.commitAllowingStateLoss()
                }

            } else {
                binding.profileOffContainer.visibility = View.VISIBLE
                binding.btnContact.isEnabled = false
                binding.btnContact.setBackgroundColor(
                    ContextCompat.getColor(
                        this,
                        R.color.md_grey_300
                    )
                )
                binding.btnContact.setTextColor(ContextCompat.getColor(this, R.color.md_grey_400))

//                blurViewContent.visibility = View.VISIBLE
//                blurViewBanner.visibility = View.VISIBLE

                blurViewContent.setBlur(this@PublicProfileActivity, blurViewContent)
            }

        }
    }

//    private fun renderOffProfile(profile: Profile?) {
//
//        if (profile != null) {
//
//            binding.profileOffContainer.visibility = View.VISIBLE
//            binding.btnContact.isEnabled = false
//
//            binding.tvProfileOffText.text =
//                "Meet ${profile.name} in person to see\ntheir AddMee Profile"
//
//            binding.tvProfileName.text = profile.name
//            binding.tvProfileLink.text = "addmee.app/${profile.username}"
//
//            if (!profile.banner.isNullOrEmpty()) {
//                binding.containerNoBanner.visibility = View.GONE
////                binding.ivBanner.visibility = View.VISIBLE
//
//                Glide.with(this)  //2
//                    .load(profile.banner)
//                    .into(binding.ivBanner)
//
//            } else {
//                binding.containerNoBanner.visibility = View.VISIBLE
////                binding.ivBanner.visibility = View.GONE
//            }
//
//
//            if (!profile.bio.isNullOrEmpty())
//                binding.tvProfileBio.text = profile.bio.toString()
//
//
//        }
//    }

    private fun setupRecyclerView() {

        publicProfileAdapter = PublicProfileAdapter()
        binding.rvProfiles.apply {
            layoutManager =
                GridLayoutManager(this@PublicProfileActivity, 3, GridLayoutManager.VERTICAL, false)
            addItemDecoration(SpacesItemDecoration(14))
            adapter = publicProfileAdapter
        }

        publicProfileAdapter.setOnItemClickListener {

            HandleOpenProfile.openProfileBaseURL(this, it)

        }

    }

    //endregion

    //region HANDLE API RESPONSE
    private fun handleResponse() {

        viewModel.publicProfileLiveData.observe(this, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    renderProfile(response.data!!)


                }
                is Resource.Error -> {
//                    if (response.message!!.contains("email")) {
//                        val profileString = GsonBuilder().create()
//                            .fromJson(response.message.toString(), Profile::class.java)
//                        renderOffProfile(profileString)
//                    }


                    hideProgressBar()
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })
    }
    //endregion

    //region PROGRESS BAR
    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }
    //endregion
}