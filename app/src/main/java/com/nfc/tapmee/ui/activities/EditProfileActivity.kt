package com.nfc.tapmee.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.nfc.tapmee.R
import com.nfc.tapmee.base.util.FragmentUtil
import com.nfc.tapmee.databinding.ActivityEditProfileBinding
import com.nfc.tapmee.ui.profile.ProfileFragment

class EditProfileActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEditProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile)

        init()
    }

    private fun init() {

        FragmentUtil(this).replaceBaseFragment(
            ProfileFragment.newInstance(),
            R.id.flEditProfile
        )

    }
}