package com.nfc.tapmee.ui.home.personal_profile

import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.ernestoyaquello.dragdropswiperecyclerview.DragDropSwipeRecyclerView
import com.ernestoyaquello.dragdropswiperecyclerview.listener.OnItemDragListener
import com.nfc.tapmee.R
import com.nfc.tapmee.arch.providers.dashboard.DashboardProviderFactory
import com.nfc.tapmee.arch.repositories.dashboard.DashboardRepository
import com.nfc.tapmee.arch.viewmodels.dashboard.DashboardViewModel
import com.nfc.tapmee.base.BaseFragment
import com.nfc.tapmee.common.database.AddMeeDatabase
import com.nfc.tapmee.common.models.CustomerDTO
import com.nfc.tapmee.common.models.Profile
import com.nfc.tapmee.common.preferences.PreferenceUtils
import com.nfc.tapmee.common.preferences.SharedPreferences
import com.nfc.tapmee.common.utils.AppOP.copyToClipboard
import com.nfc.tapmee.common.utils.Constants
import com.nfc.tapmee.common.utils.dialog.AlertOP
import com.nfc.tapmee.common.utils.dialog.profiledialog.ProfileUpdateListener
import com.nfc.tapmee.common.utils.generics.Resource
import com.nfc.tapmee.common.utils.profile.HandleOpenProfile
import com.nfc.tapmee.databinding.FragmentPesonalProfileBinding
import com.nfc.tapmee.ui.activities.DashboardActivity
import com.nfc.tapmee.ui.activities.EditProfileActivity
import com.nfc.tapmee.ui.home.adapter.MyProfilesAdapter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PersonProfileFragment : BaseFragment() {


    private lateinit var binding: FragmentPesonalProfileBinding
    private lateinit var viewModel: DashboardViewModel
    private var myProfilesAdapter: MyProfilesAdapter? = null
    private var myProfiles: ArrayList<Profile> = arrayListOf()

    private lateinit var dashboardRepository: DashboardRepository
    private var customerDTO: CustomerDTO? = null
    private val uiScope = CoroutineScope(Dispatchers.Main)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_pesonal_profile,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        initViewModel()
        handleResponse()
    }

    //region VIEW-MODEL
    private fun initViewModel() {
        val customerDao = AddMeeDatabase(requireActivity()).customerDAO
        val businessDao = AddMeeDatabase(requireActivity()).businessInfoDAO
        dashboardRepository = DashboardRepository(customerDao, businessDao)

        if (customerDTO == null)
            getCustomerInfo()

        val viewModelProviderFactory =
            DashboardProviderFactory(requireActivity().application, dashboardRepository)
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory).get(DashboardViewModel::class.java)

        viewModel.getMyProfiles()

    }
    //endregion

    //region HELPING METHODS

    private fun listeners() {
        binding.btnEditProfile.setOnClickListener {
            gotoEditProfile()
        }

        binding.svDirectSwitch.setOnCheckedChangeListener { switchView, b ->
            if(b){
                viewModel.updateOpenDirect(customerDTO!!.customerID!!, 1)
                binding.tvDirectStatus.text = getString(R.string.lbl_direct_on)
            }else{
                viewModel.updateOpenDirect(customerDTO!!.customerID!!, 0)
                binding.tvDirectStatus.text = getString(R.string.lbl_direct_off)
            }
        }

        binding.addProfile.setOnClickListener {
            gotoEditProfile()
        }
    }

    private fun getCustomerInfo() {

        //Fetch CustomerInfo from Room
        uiScope.launch {

            customerDTO = dashboardRepository.getCustomerInfo()

            withContext(Dispatchers.Main) {

                if (customerDTO != null)
                    renderCustomerDetails()
            }
        }
    }


    private fun renderCustomerDetails() {


        SharedPreferences.getInstance().save(Constants.USERNAME, customerDTO!!.username.toString())
        SharedPreferences.getInstance().save(Constants.EMAIL, customerDTO!!.email.toString())

        if (customerDTO!!.name.isNullOrEmpty() ||
            customerDTO!!.name == "null"
        )
            binding.tvProfileName.text = customerDTO!!.username
        else {
            binding.tvProfileName.text = customerDTO!!.name
        }

        if (customerDTO!!.bio.isNullOrEmpty() ||
            customerDTO!!.bio == "null"
        )
            binding.tvProfileBio.visibility = View.GONE
        else
            binding.tvProfileBio.text = customerDTO!!.bio


        binding.tvProfileLink.text = "${Constants.APP_URL}${customerDTO!!.username}"

        binding.tvProfileLink.setOnClickListener {
            requireActivity().copyToClipboard("${Constants.APP_URL_WEB}${customerDTO!!.username}")
            Toast.makeText(
                requireContext(),
                getString(R.string.lbl_link_copied),
                Toast.LENGTH_SHORT
            ).show()
        }

        if (!customerDTO!!.banner.isNullOrEmpty()) {

            Glide.with(requireActivity())  //2
                .load(customerDTO!!.banner)
                .into(binding.ivBanner)

        } else {
            Glide.with(requireActivity())  //2
                .load(R.drawable.ic_profile_placeholder)
                .into(binding.ivBanner)
        }

        updateOpenDirect()
        listeners()
    }

    private fun updateOpenDirect() {

        if (customerDTO!!.open_direct == 0) {
            binding.svDirectSwitch.isChecked = false
            binding.tvDirectStatus.text = getString(R.string.lbl_direct_off)
        } else {
            binding.svDirectSwitch.isChecked = true
            binding.tvDirectStatus.text = getString(R.string.lbl_direct_on)
        }
    }

    private fun setupRecyclerView(profiles: List<Profile>) {

        myProfilesAdapter = MyProfilesAdapter(profiles, requireContext())

        binding.rvProfiles.layoutManager = GridLayoutManager(requireContext(), 3)
        binding.rvProfiles.adapter = myProfilesAdapter
        // Set this property if your grid can be scrolled vertically
        binding.rvProfiles.numOfColumnsPerRowInGridList = 3
        binding.rvProfiles.orientation =
            DragDropSwipeRecyclerView.ListOrientation.VERTICAL_LIST_WITH_UNCONSTRAINED_DRAGGING
        binding.rvProfiles.disableSwipeDirection(DragDropSwipeRecyclerView.ListOrientation.DirectionFlag.RIGHT)
        binding.rvProfiles.disableSwipeDirection(DragDropSwipeRecyclerView.ListOrientation.DirectionFlag.LEFT)
        binding.rvProfiles.reduceItemAlphaOnSwiping = true
        binding.rvProfiles.longPressToStartDragging = true


        myProfilesAdapter!!.setOnItemClickListener {
            when {
                it.profile_code == "contact-card" -> {
                    HandleOpenProfile.openProfileBaseURL(
                        requireContext(),
                        it
                    )
                }
                else -> {
                    AlertOP.showProfileDetailSheet(requireContext(), it,
                        object : ProfileUpdateListener {
                            override fun onProfileUpdate(isUpdateRequired: Boolean) {
                                if (isUpdateRequired) {
                                    viewModel.getMyProfiles()
                                }
                            }
                        })
                }
            }
        }


        val onDragListener = object : OnItemDragListener<Profile> {
            override fun onItemDragged(previousPosition: Int, newPosition: Int, item: Profile) {

            }

            override fun onItemDropped(
                initialPosition: Int,
                finalPosition: Int,
                item: Profile
            ) {

                //Clear local Profiles List as we need to update with Updated Sequence
                myProfiles.clear()

                var json = "{"
                val updatedSequence = myProfilesAdapter!!.getRefreshedList()

                //Add profile with Updated Sequence
                myProfiles.addAll(myProfilesAdapter!!.getRefreshedList() as MutableList<Profile>)

                for (index in updatedSequence.indices) {

                    if (updatedSequence[index].id != null)
                        json += "\"${updatedSequence[index].id}\":\"${index}\","
                }

                json = json.dropLast(1)

                json += "}"

                Log.d("DragDrop", json)

                viewModel.updateProfileSequence(json)
            }
        }

        binding.rvProfiles.dragListener = onDragListener


    }

    private fun gotoEditProfile() {
        startActivity(
            Intent(
                requireActivity(),
                EditProfileActivity::class.java
            )
        )
        requireActivity().finish()
    }
    //endregion


    //region API HANDLE RESPONSE
    private fun handleResponse() {

        viewModel.myProfilesLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    (activity as DashboardActivity?)!!.hideLoading()

                    myProfiles.clear()


                    //Filter only personal profiles here
                    val personalProfiles = response.data!!.profiles.filter { it.is_business == 0 }

                    if (!personalProfiles.isNullOrEmpty())
                        myProfiles.addAll(personalProfiles as MutableList<Profile>)


                    //Mark Profile list items as Enabled of Disabled based on OpenDirect 1/0
                    if (customerDTO!!.open_direct == 1) {

                        if (myProfiles.size > 1) {

                            for (item in myProfiles) {
                                item.isDisabled = true
                            }

                            myProfiles[0].isDisabled = false
                        }


                    }

                    if (customerDTO!!.open_direct!! == 1)
                        PreferenceUtils.saveIsDirectOn(true)
                    else
                        PreferenceUtils.saveIsDirectOn(false)

                    setupRecyclerView(myProfiles)
                }
                is Resource.Error -> {
                    (activity as DashboardActivity?)!!.hideLoading()
                }
                is Resource.Loading -> {

                    (activity as DashboardActivity?)!!.showLoading()
                }
            }
        })



        viewModel.updateDirectLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    (activity as DashboardActivity?)!!.hideLoading()


                    //Update profiles items and mark as disabled accordingly
                    if (response.data!!.open_direct == 0) {
                        PreferenceUtils.saveIsDirectOn(false)
                        //Enable >  Direct is OFF

                        for (item in myProfiles) {
                            item.isDisabled = false
                        }

                        myProfilesAdapter!!.refreshList()
                    } else {
                        //Disabled > Direct is ON
                        PreferenceUtils.saveIsDirectOn(true)

                        if (myProfiles.size > 1) {
                            for (item in myProfiles) {
                                item.isDisabled = true
                            }

                            myProfiles[0].isDisabled = false

                            myProfilesAdapter!!.refreshList()
                        }

                    }

                    customerDTO!!.open_direct = response.data.open_direct
                    viewModel.updateCustomerDto(response.data)


                }
                is Resource.Error -> {
                    updateOpenDirect()
                    (activity as DashboardActivity?)!!.hideLoading()
                }
                is Resource.Loading -> {

                    (activity as DashboardActivity?)!!.showLoading()
                }
            }
        })
    }
    //endregion

    companion object {
        @JvmStatic
        fun newInstance() =
            PersonProfileFragment()
    }
}