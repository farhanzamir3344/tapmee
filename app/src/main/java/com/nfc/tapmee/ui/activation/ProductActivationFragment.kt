package com.nfc.tapmee.ui.activation

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.nfc.tapmee.R
import com.nfc.tapmee.base.BaseActivity
import com.nfc.tapmee.base.BaseFragment
import com.nfc.tapmee.base.util.FragmentUtil
import com.nfc.tapmee.common.preferences.PreferenceUtils
import com.nfc.tapmee.common.preferences.SharedPreferences
import com.nfc.tapmee.common.utils.Constants
import com.nfc.tapmee.common.utils.views.TextViewUtils
import com.nfc.tapmee.databinding.FragmentProductActivationBinding
import com.nfc.tapmee.ui.activities.DashboardActivity

class ProductActivationFragment : BaseFragment() {

    private lateinit var binding: FragmentProductActivationBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_product_activation,
                container,
                false
            )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        renderProduct()
        renderLink()
        listeners()

    }

    private fun renderProduct() {

        when (productType) {
            "Button" -> {
                binding.imgButton.visibility = View.VISIBLE
            }
            "Keychain" -> {
                binding.imgKeychain.visibility = View.VISIBLE
            }
        }
    }

    private fun renderLink() {
        val username = SharedPreferences.getInstance().read(Constants.USERNAME, "")
//        binding.tvUsernameLink.text = "addmee.app/${username}"
        binding.tvUsernameLink.text = username
    }

    private fun listeners() {
        binding.ivCloseScreen.setOnClickListener {

            if (PreferenceUtils.isNewUser()) {
                PreferenceUtils.saveNewUser(false)

                startActivity(
                    Intent(
                        requireActivity(),
                        DashboardActivity::class.java
                    )
                )
            }

            requireActivity().finish()
        }

        binding.ivBack.setOnClickListener {
            FragmentUtil(requireActivity() as AppCompatActivity).goBackFragment()
        }

        binding.btnActivate.setOnClickListener {
            startActivity(
                Intent(
                    requireContext(),
                    NfcWriteActivity::class.java
                )
            )
        }

        TextViewUtils.makeTextLink(
            binding.tvBuyAddMee,
            getString(R.string.lbl_btn_txt_buy),
            true,
            Color.BLACK
        ) {
            (requireActivity() as BaseActivity).gotoWeb(
                Constants.BUY_NFC,
                getString(R.string.lbl_heading_buy_nfc)
            )
        }

    }

    companion object {
        private var productType: String? = null

        @JvmStatic
        fun newInstance(productTypeString: String): Fragment {
            this.productType = productTypeString
            return ProductActivationFragment()
        }
    }
}