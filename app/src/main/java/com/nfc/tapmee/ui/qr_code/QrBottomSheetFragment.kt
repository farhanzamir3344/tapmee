package com.nfc.tapmee.ui.qr_code

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface.OnShowListener
import android.graphics.Color
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.util.TypedValue
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidmads.library.qrgenearator.QRGContents
import androidmads.library.qrgenearator.QRGEncoder
import androidx.annotation.NonNull
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.zxing.WriterException
import com.nfc.tapmee.R
import com.nfc.tapmee.common.preferences.SharedPreferences
import com.nfc.tapmee.common.utils.AppOP
import com.nfc.tapmee.common.utils.Constants
import kotlinx.android.synthetic.main.fragment_qr_bottom_sheet.*


class QrBottomSheetFragment : BottomSheetDialogFragment() {


    @NonNull
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog: Dialog = super.onCreateDialog(savedInstanceState)
        dialog.setOnShowListener(OnShowListener { dialogInterface ->
            val bottomSheetDialog = dialogInterface as BottomSheetDialog
            setupFullHeight(bottomSheetDialog)
        })
        return dialog
    }


    private fun setupFullHeight(bottomSheetDialog: BottomSheetDialog) {
        val bottomSheet =
            bottomSheetDialog.findViewById<View>(R.id.design_bottom_sheet) as FrameLayout?
        val behavior: BottomSheetBehavior<*> = BottomSheetBehavior.from<FrameLayout?>(bottomSheet!!)
        val layoutParams = bottomSheet.layoutParams
        val windowHeight = getWindowHeight()
        if (layoutParams != null) {
            layoutParams.height = windowHeight
        }
        bottomSheet.layoutParams = layoutParams
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun getWindowHeight(): Int {
        // Calculate window height for fullscreen use
        val displayMetrics = DisplayMetrics()
        (context as Activity?)!!.windowManager.defaultDisplay
            .getMetrics(displayMetrics)
        return displayMetrics.heightPixels
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_qr_bottom_sheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()

    }

    private fun init() {

        val username = SharedPreferences.getInstance().read("username", "")
        val userProfileLink = "${Constants.APP_URL}${username}"
        tvProfileLink.text = userProfileLink

        setupBackPressListener()

        imgv_close.setOnClickListener { dismiss() }

        btn_share.setOnClickListener { shareLink("${Constants.APP_URL_WEB}$username") }

        btn_add_to_wallet.setOnClickListener { }

        createQrCode("${Constants.APP_URL_WEB}$username")
    }
    // Helpers
    private fun setupBackPressListener() {
        this.view?.isFocusableInTouchMode = true
        this.view?.requestFocus()
        this.view?.setOnKeyListener { _, keyCode, _ ->
            keyCode == KeyEvent.KEYCODE_BACK
        }
    }

    private fun createQrCode(profileLink: String) {

        val qrgEncoder = QRGEncoder(profileLink, null, QRGContents.Type.TEXT, 600)
        qrgEncoder.colorBlack = Color.BLACK
        qrgEncoder.colorWhite = Color.WHITE
        try {
            // Getting QR-Code as Bitmap
            val bitmap = qrgEncoder.bitmap
            // Setting Bitmap to ImageView
            ivQrCode.setImageBitmap(bitmap)
        } catch (e: WriterException) {
            Log.v("TAG", e.toString())
        }
    }

    private fun shareLink(profileLink: String) {
        AppOP.shareApp(profileLink, "", requireContext())
    }

    // extension method to convert pixels to dp
    fun Int.toDp(context: Context): Int = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), context.resources.displayMetrics
    ).toInt()

    companion object {
        @JvmStatic
        fun newInstance() =
            QrBottomSheetFragment()
    }
}