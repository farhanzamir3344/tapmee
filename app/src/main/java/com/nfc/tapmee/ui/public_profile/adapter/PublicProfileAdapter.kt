package com.nfc.tapmee.ui.public_profile.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nfc.tapmee.R
import com.nfc.tapmee.common.models.Profile
import kotlinx.android.synthetic.main.item_public_profile.view.*
import java.util.*

class PublicProfileAdapter() :
    RecyclerView.Adapter<PublicProfileAdapter.PublicProfileViewHolder>() {

    inner class PublicProfileViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val differConfig = object : DiffUtil.ItemCallback<Profile>() {

        override fun areItemsTheSame(oldItem: Profile, newItem: Profile): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Profile, newItem: Profile): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differConfig)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PublicProfileViewHolder {
        return PublicProfileViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_public_profile,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: PublicProfileViewHolder, position: Int) {
        val profile = differ.currentList[position]
        holder.itemView.apply {


            if (Locale.getDefault().language == "de")
                tvProfileName.text = profile.title_de
            else
                tvProfileName.text = profile.title


            if (!profile.icon.isNullOrEmpty()) {
                Glide.with(context)  //2
                    .load(profile.icon)
                    .into(ivProfileThumb)
            }

            setOnClickListener {
                onItemClickListener?.let { it(profile) }
            }

        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }


    private var onItemClickListener: ((Profile) -> Unit)? = null

    fun setOnItemClickListener(listener: (Profile) -> Unit) {
        onItemClickListener = listener
    }


}