package com.nfc.tapmee.ui.activation

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.ActivityInfo
import android.nfc.NdefMessage
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.Ndef
import android.nfc.tech.NdefFormatable
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.nfc.tapmee.R
import com.nfc.tapmee.base.BaseActivity
import com.nfc.tapmee.common.preferences.SharedPreferences
import com.nfc.tapmee.common.utils.BeepHelper
import com.nfc.tapmee.common.utils.Constants
import com.nfc.tapmee.common.utils.dialog.AlertOP
import com.nfc.tapmee.common.utils.dialog.GeneralDialogListener
import com.nfc.tapmee.common.utils.nfc.NFCManager
import com.nfc.tapmee.common.utils.nfc.WriteNfcListener
import kotlinx.android.synthetic.main.activity_nfc_write.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class NfcWriteActivity : BaseActivity() {
    private val Error_Detected = "Error occurred! Try reading Addmee again"
    private val No_NFC_Detected = "This device does not support NFC"

    private var vibrate: Vibrator? = null

    // NFC adapter for checking NFC state in the device
    private var nfcAdapter: NfcAdapter? = null

    // Pending intent for NFC intent foreground dispatch.
    // Used to read all NDEF tags while the app is running in the foreground.
    private var nfcPendingIntent: PendingIntent? = null
    private var techList: Array<Array<String>>? = null
    private var intentFiltersArray: Array<IntentFilter>? = null

    private var nfcManager: NFCManager? = null
    private var profileLinkNdef: NdefMessage? = null
    private var tag: Tag? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        setOrientation()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nfc_write)


        val username = SharedPreferences.getInstance().read("username", "")
        val userProfileLink = "${Constants.APP_URL}${username}"

        nfcManager = NFCManager()

        profileLinkNdef = nfcManager!!.createUriMessage(userProfileLink, "https://")

        init()
    }

    // Method
    private fun setOrientation() {
        requestedOrientation =
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED else ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    private fun init() {

        vibrate = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

        btnCancel.setOnClickListener {
            finish()
        }

        nfcAdapter = NfcAdapter.getDefaultAdapter(this)

        if (nfcAdapter != null) {
            if (!nfcAdapter?.isEnabled!!) {
                AlertOP.showAlert(
                    this,
                    getString(R.string.lbl_nfc_off),
                    getString(R.string.msg_turn_on_nfc),
                    getString(R.string.lbl_dialog_ok),
                    null,
                    object : GeneralDialogListener {
                        override fun onPositiveClick() {

                            finish()
                        }

                        override fun onNegativeClick() {

                        }
                    })
            } else {
                val nfcIntent = Intent(this, javaClass)
                nfcIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)

                // Read all tags when app is running and in the foreground
                // Create a generic PendingIntent that will be deliver to this activity. The NFC stack
                // will fill in the intent with the details of the discovered tag before delivering to
                // this activity.
                nfcPendingIntent = PendingIntent.getActivity(this, 0, nfcIntent, 0)

                intentFiltersArray = arrayOf<IntentFilter>()
                techList = arrayOf(
                    arrayOf(
                        Ndef::class.java.name
                    ), arrayOf(NdefFormatable::class.java.name)
                )
            }


        } else {
            tvHeading.visibility = View.GONE
            tvDescription.text = No_NFC_Detected
            ivScan.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_read_failed
                )
            )
        }
    }

    override fun onResume() {
        super.onResume()

        // Get all NDEF discovered intents
        // Makes sure the app gets all discovered NDEF messages as long as it's in the foreground.
        try {
            if (nfcAdapter != null && nfcAdapter!!.isEnabled)
                nfcAdapter?.enableForegroundDispatch(
                    this,
                    nfcPendingIntent,
                    intentFiltersArray,
                    techList
                )
        } catch (ex: Exception) {
            ex.printStackTrace()
        }


    }

    override fun onPause() {
        super.onPause()
        // Disable foreground dispatch, as this activity is no longer in the foreground
        nfcAdapter?.disableForegroundDispatch(this)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        Log.d("Nfc", "New intent")

        if (intent != null) {
            // It is the time to write the tag
            tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG)

            if (profileLinkNdef != null) {
                nfcManager!!.writeTag(tag,
                    profileLinkNdef,
                    object : WriteNfcListener {
                        override fun onWriteSuccess() {
                            vibrate()
                            tvDescription.text = getString(R.string.msg_tag_activated_success)
                            ivScan.setImageDrawable(
                                ContextCompat.getDrawable(
                                    this@NfcWriteActivity,
                                    R.drawable.ic_read_success
                                )
                            )

                            //Add Delay for 3 seconds and finish the screen
                            lifecycleScope.launch {
                                delay(3000L)
                                finish()
                            }
                        }

                        override fun onWriteFailed() {

                            vibrate()

                            tvDescription.text = Error_Detected
                            ivScan.setImageDrawable(
                                ContextCompat.getDrawable(
                                    this@NfcWriteActivity,
                                    R.drawable.ic_read_failed
                                )
                            )
                        }
                    })


            } else {
                // Handle intent

            }
        }

    }

    private fun vibrate() {
        BeepHelper.beep(100)

        try {
            // Vibrate for 500 milliseconds
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrate?.vibrate(
                    VibrationEffect.createOneShot(
                        500,
                        VibrationEffect.DEFAULT_AMPLITUDE
                    )
                );
            } else {
                //deprecated in API 26
                vibrate?.vibrate(500);
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

    }

}