package com.nfc.tapmee.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nfc.tapmee.R
import com.nfc.tapmee.arch.providers.profile.ProfileProviderFactory
import com.nfc.tapmee.arch.repositories.profile.ProfileRepository
import com.nfc.tapmee.arch.viewmodels.profile.ProfileViewModel
import com.nfc.tapmee.base.BaseFragment
import com.nfc.tapmee.base.util.FragmentUtil
import com.nfc.tapmee.common.models.CustomerDTO
import com.nfc.tapmee.common.utils.alerts.SnackBarUtil
import com.nfc.tapmee.common.utils.dialog.AlertOP
import com.nfc.tapmee.common.utils.dialog.GeneralDialogListener
import com.nfc.tapmee.common.utils.generics.Resource
import com.nfc.tapmee.databinding.FragmentContactCardBinding
import com.nfc.tapmee.ui.profile.adapter.ContactCardAdapter
import kotlinx.android.synthetic.main.fragment_contact_card.*

class ContactCardFragment private constructor() : BaseFragment() {

    private lateinit var binding: FragmentContactCardBinding
    private lateinit var viewModel: ProfileViewModel
    private lateinit var contactAdapter: ContactCardAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_contact_card,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        listeners()
        setupRecyclerView()
        initViewModel()
        handleResponse()
    }

    //region VIEW-MODEL
    private fun initViewModel() {

        val viewModelProviderFactory =
            ProfileProviderFactory(requireActivity().application, ProfileRepository())
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory).get(ProfileViewModel::class.java)

        viewModel.getMyProfiles()

    }
    //endregion

    //region HELPING METHODS
    private fun listeners() {
        binding.ivCloseScreen.setOnClickListener {
            FragmentUtil(requireActivity() as AppCompatActivity).goBackFragment()
        }

        binding.btnSaveContactCard.setOnClickListener {
            var profileIDs = ""

            val contactsList = contactAdapter.differ.currentList

            for (card in contactsList) {
                if (card.added_to_contact_card == "yes")
                    profileIDs += "${card.id},"
            }

            profileIDs = profileIDs.dropLast(1)

            viewModel.updateContactCard(profileIDs, "no")
        }
    }

    private fun setupRecyclerView() {

        contactAdapter = ContactCardAdapter()
        rvContactCards.apply {
            layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
            adapter = contactAdapter
        }

        contactAdapter.setOnItemClickListener {
            AlertOP.showAlert(requireContext(),
                it.title,
                it.profile_link,
                getString(R.string.lbl_dialog_ok),
                null,
                object : GeneralDialogListener {
                    override fun onPositiveClick() {

                    }

                    override fun onNegativeClick() {

                    }
                })
        }

    }
    //endregion

    //region HANDLE API RESPONSE
    private fun handleResponse() {
        viewModel.myProfilesLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    response.data?.let { response ->
                        if (response.profiles.isNotEmpty()) {
                            val contactCardsToShow =
                                response.profiles.filter { it.is_business == 0 && it.profile_code != "contact-card" }

                            contactAdapter.differ.submitList(contactCardsToShow.toList())
                        }

                    }
                }

                is Resource.Error -> {
                    hideProgressBar()
                    SnackBarUtil.showSnackBar(requireContext(), response.message!!, true)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })

        viewModel.updateContactCardLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    FragmentUtil(requireActivity() as AppCompatActivity).goBackFragment()

                }
                is Resource.Error -> {
                    hideProgressBar()
                    SnackBarUtil.showSnackBar(requireContext(), response.message!!, true)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })
    }
    //endregion

    //region PROGRESS BAR
    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }
    //endregion

    companion object {
        private var customerDTO: CustomerDTO? = null

        @JvmStatic
        fun newInstance(customerDTO: CustomerDTO?): Fragment {
            this.customerDTO = customerDTO
            return ContactCardFragment()
        }

    }
}