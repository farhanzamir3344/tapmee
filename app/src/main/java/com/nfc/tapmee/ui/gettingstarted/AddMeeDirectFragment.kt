package com.nfc.tapmee.ui.gettingstarted

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.nfc.tapmee.R
import com.nfc.tapmee.base.BaseFragment
import com.nfc.tapmee.common.preferences.PreferenceUtils
import com.nfc.tapmee.databinding.FragmentAddmeeDirectBinding
import com.nfc.tapmee.ui.activities.AddMeeHelpActivity
import com.nfc.tapmee.ui.activities.DashboardActivity

class AddMeeDirectFragment : BaseFragment() {

    private lateinit var binding: FragmentAddmeeDirectBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_addmee_direct,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnContinue.setOnClickListener {

            (requireActivity() as AddMeeHelpActivity).selectPageIndex(2)
        }

        binding.ivCloseScreen.setOnClickListener {
            if (PreferenceUtils.isNewUser()) {

                PreferenceUtils.saveNewUser(false)

                startActivity(
                    Intent(
                        requireActivity(),
                        DashboardActivity::class.java
                    )
                )
            }

            requireActivity().finish()
        }
    }
}