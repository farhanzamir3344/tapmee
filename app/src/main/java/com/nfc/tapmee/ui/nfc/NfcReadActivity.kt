package com.nfc.tapmee.ui.nfc

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.nfc.NdefMessage
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import com.nfc.tapmee.R
import com.nfc.tapmee.base.BaseActivity
import com.nfc.tapmee.common.utils.BeepHelper
import com.nfc.tapmee.common.utils.dialog.AlertOP
import com.nfc.tapmee.common.utils.dialog.GeneralDialogListener
import com.nfc.tapmee.ui.public_profile.PublicProfileActivity
import kotlinx.android.synthetic.main.fragment_nfc_bottom_sheet.*


class NfcReadActivity : BaseActivity() {
    private val Error_Detected = "Error occurred! Try reading Addmee again"
    private val No_NFC_Detected = "This device does not support NFC"

    private var vibrate: Vibrator? = null

    // NFC adapter for checking NFC state in the device
    private var nfcAdapter: NfcAdapter? = null

    // Pending intent for NFC intent foreground dispatch.
    // Used to read all NDEF tags while the app is running in the foreground.
    private var nfcPendingIntent: PendingIntent? = null
    private var tag: Tag? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setOrientation()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nfc_read)

        init()
    }

    // Method
    private fun setOrientation() {
        requestedOrientation =
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED else ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        logMessage("Found intent in onNewIntent", intent?.action.toString())
        // If we got an intent while the app is running, also check if it's a new NDEF message
        // that was discovered
        if (intent != null) readNFC(intent)
    }


    override fun onResume() {
        super.onResume()
        // Get all NDEF discovered intents
        // Makes sure the app gets all discovered NDEF messages as long as it's in the foreground.
        try {
            if (nfcAdapter != null && nfcAdapter!!.isEnabled)
                nfcAdapter?.enableForegroundDispatch(this, nfcPendingIntent, null, null)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        // Alternative: only get specific HTTP NDEF intent
        //nfcAdapter?.enableForegroundDispatch(this, nfcPendingIntent, nfcIntentFilters, null);
    }

    override fun onPause() {
        super.onPause()
        // Disable foreground dispatch, as this activity is no longer in the foreground
        nfcAdapter?.disableForegroundDispatch(this)
    }

    private fun init() {

        vibrate = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

        btn_profile.setOnClickListener {
//            finish()

            startActivity(
                Intent(
                    this,
                    PublicProfileActivity::class.java
                ).putExtra("ExtraURL", "https://tap-mee.com/farhanzamir")
            )
            finish()
        }

        nfcAdapter = NfcAdapter.getDefaultAdapter(this)


        if (nfcAdapter != null) {
            if (!nfcAdapter?.isEnabled!!) {
                AlertOP.showAlert(this,
                    getString(R.string.lbl_nfc_off),
                    getString(R.string.msg_turn_on_nfc),
                    getString(R.string.lbl_dialog_ok),
                    null,
                    object : GeneralDialogListener {
                        override fun onPositiveClick() {
                            finish()
                        }

                        override fun onNegativeClick() {

                        }
                    })

            } else {
                // Read all tags when app is running and in the foreground
                // Create a generic PendingIntent that will be deliver to this activity. The NFC stack
                // will fill in the intent with the details of the discovered tag before delivering to
                // this activity.
                nfcPendingIntent = PendingIntent.getActivity(
                    this, 0,
                    Intent(this, javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0
                )

                // Optional: Setup an intent filter from code for a specific NDEF intent
                // Use this code if you are only interested in a specific intent and don't want to
                // interfere with other NFC tags.
                // In this example, the code is commented out so that we get all NDEF messages,
                // in order to analyze different NDEF-formatted NFC tag contents.
//            val ndef = IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED)
//            ndef.addCategory(Intent.CATEGORY_DEFAULT)
//            ndef.addDataScheme("https")
//            ndef.addDataAuthority("*.addme.app", null)
//            ndef.addDataPath("/", PatternMatcher.PATTERN_PREFIX)

                // More information: https://stackoverflow.com/questions/30642465/nfc-tag-is-not-discovered-for-action-ndef-discovered-action-even-if-it-contains
//            nfcIntentFilters = arrayOf(ndef)

                if (intent != null) {
                    // Check if the app was started via an NDEF intent
                    Log.d("Found intent", intent.action.toString())
                    readNFC(intent = intent)
                }
            }


        }
    }


    private fun readNFC(intent: Intent) {
        val action = intent.action

        if (action == NfcAdapter.ACTION_NDEF_DISCOVERED) {

            logMessage("New NDEF intent", intent.toString())

            // Retrieve the raw NDEF message from the tag
            val rawMessages = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES)
            logMessage("Raw messages", rawMessages!!.size.toString())


//             Simple variant: assume we have 1x URI record
            if (!rawMessages.isNullOrEmpty()) {
                val ndefMsg = rawMessages[0] as NdefMessage
                if (ndefMsg.records != null && ndefMsg.records.isNotEmpty()) {
                    val ndefRecord = ndefMsg.records[0]
                    if (ndefRecord.toUri() != null && ndefRecord.toUri().toString()
                            .contains("tap-mee.com")
                    ) {

                        vibrate()

                        logMessage("URI detected", ndefRecord.toUri().toString())

                        ivScan.setImageDrawable(
                            ContextCompat.getDrawable(
                                this,
                                R.drawable.ic_read_success
                            )
                        )

//                        gotoWeb(ndefRecord.toUri().toString(), "AddMee")
                        startActivity(
                            Intent(
                                this,
                                PublicProfileActivity::class.java
                            ).putExtra("ExtraURL", ndefRecord.toUri().toString())
                        )
                        finish()
                    }
                }
            } else {
                tvHeading.visibility = View.GONE
                tvDescription.text = Error_Detected
                ivScan.setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_read_failed
                    )
                )
            }

        }
    }

    private fun vibrate() {
        BeepHelper.beep(100)

        try {
            // Vibrate for 500 milliseconds
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrate?.vibrate(
                    VibrationEffect.createOneShot(
                        500,
                        VibrationEffect.DEFAULT_AMPLITUDE
                    )
                );
            } else {
                //deprecated in API 26
                vibrate?.vibrate(500);
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    /**
     * Log a message to the debug text view.
     * @param header title text of the message, printed in bold
     * @param text optional parameter containing details about the message. Printed in plain text.
     */
    private fun logMessage(header: String, text: String?) {
        Log.d(header, text.toString())
    }

}