package com.nfc.tapmee.ui.activities

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.nfc.tapmee.R
import com.nfc.tapmee.base.BaseActivity
import com.nfc.tapmee.base.util.FragmentUtil
import com.nfc.tapmee.databinding.ActivityRegisterAndLoginBinding
import com.nfc.tapmee.ui.auth.LoginRegisterHomeFragment
import com.nfc.tapmee.ui.auth.login.LoginFragment

class LoginAndRegisterActivity : BaseActivity() {
    private lateinit var binding: ActivityRegisterAndLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register_and_login)
        init()
    }

    private fun init() {

        FragmentUtil(this).replaceBaseFragment(
            LoginFragment.newInstance(),
            R.id.lt_login_register_container
        )

//        if(PreferenceUtils.isRegisterCustomer()){
//            FragmentUtil(this).gotoNextFragment(
//                CreateUsernameFragment.newInstance(), R.id.lt_login_register_container
//            )

    }

    override fun onBackPressed() {
        if (FragmentUtil(this).getBackstackCount() == 1)
            finish()
        else
            super.onBackPressed()
    }
}