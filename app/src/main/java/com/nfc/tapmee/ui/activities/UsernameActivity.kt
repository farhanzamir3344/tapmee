package com.nfc.tapmee.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.nfc.tapmee.R
import com.nfc.tapmee.base.util.FragmentUtil
import com.nfc.tapmee.databinding.ActivityUsernameBinding
import com.nfc.tapmee.ui.username.CreateUsernameFragment

class UsernameActivity : AppCompatActivity() {

    private lateinit var binding: ActivityUsernameBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_username)
        init()
    }

    private fun init() {

        FragmentUtil(this).replaceBaseFragment(
            CreateUsernameFragment.newInstance(),
            R.id.lt_username
        )

    }

}