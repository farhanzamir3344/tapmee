package com.nfc.tapmee.ui.activities

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.nfc.tapmee.R
import com.nfc.tapmee.base.BaseActivity
import com.nfc.tapmee.databinding.ActivityAddMeeProBinding

class AddMeeProActivity : BaseActivity() {
    private lateinit var binding: ActivityAddMeeProBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_mee_pro)

        listeners()
    }

    private fun listeners() {
        binding.ivCloseScreen.setOnClickListener {
            finish()
        }
    }

}