package com.nfc.tapmee.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.nfc.tapmee.R
import com.nfc.tapmee.common.utils.viewpager.PagerAdapter
import com.nfc.tapmee.ui.gettingstarted.ActivateTagFragment
import com.nfc.tapmee.ui.gettingstarted.AddMeeDirectFragment
import com.nfc.tapmee.ui.gettingstarted.SettingUpFragment
import kotlinx.android.synthetic.main.activity_add_mee_help.*

class AddMeeHelpActivity : AppCompatActivity() {
    private val pagerAdapter: PagerAdapter by lazy {
        PagerAdapter(supportFragmentManager)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_mee_help)

        //Render ViewPager/TabLayout
        init()
    }

    private fun init() {
        pagerAdapter.clear()
        pagerAdapter.addFragment(SettingUpFragment(), "SettingUpFragment")
        pagerAdapter.addFragment(AddMeeDirectFragment(), "AddMeeDirectFragment")
        pagerAdapter.addFragment(ActivateTagFragment(), "ActivateTagFragment")

        vpAddMeeHelp.adapter = pagerAdapter
        dotsIndicator.setViewPager(vpAddMeeHelp)

    }

    fun selectPageIndex(index: Int) {
        vpAddMeeHelp.currentItem = index
    }

}