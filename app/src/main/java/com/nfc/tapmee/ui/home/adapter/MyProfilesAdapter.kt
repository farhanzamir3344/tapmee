package com.nfc.tapmee.ui.home.adapter

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.ernestoyaquello.dragdropswiperecyclerview.DragDropSwipeAdapter
import com.nfc.tapmee.R
import com.nfc.tapmee.common.models.Profile
import com.nfc.tapmee.common.preferences.PreferenceUtils
import java.util.*

class MyProfilesAdapter(dataSet: List<Profile>, private val context: Context) :
    DragDropSwipeAdapter<Profile, MyProfilesAdapter.ViewHolder>(dataSet) {

    private var list: List<Profile> = this.dataSet
    private var isListenerAdded = false
    override fun getViewHolder(itemView: View) = ViewHolder(itemView)


    override fun onBindViewHolder(
        item: Profile,
        viewHolder: ViewHolder,
        position: Int
    ) {
        if (dataSet[position].isDisabled)
            viewHolder.disableView.visibility = View.VISIBLE
        else
            viewHolder.disableView.visibility = View.GONE


        viewHolder.itemText.text = dataSet[position].title

        if (!dataSet[position].icon.isNullOrEmpty()) {
            Glide.with(context)  //2
                .load(dataSet[position].icon)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .into(viewHolder.profileThumb)
        } else {
            viewHolder.profileThumb.setImageResource(dataSet[position].icon_local!!)
        }

        viewHolder.profileThumb.setOnClickListener {

            onItemClickListener?.let {
                it(dataSet[position])
            }
        }

    }


    inner class ViewHolder(itemView: View) : DragDropSwipeAdapter.ViewHolder(itemView) {
        val itemText: TextView = itemView.findViewById(R.id.tvProfileName)
        val dragIcon: CardView = itemView.findViewById(R.id.ivDragIcon)
        val profileThumb: ImageView = itemView.findViewById(R.id.ivProfileThumb)
        val disableView: View = itemView.findViewById(R.id.viewDisabled)
        val rootView: LinearLayout = itemView.findViewById(R.id.rootView)

        init {

//            profileThumb.setOnClickListener {
//                val position: Int = adapterPosition
//                onItemClickListener?.let {
//                    it(list[position])
//                }
//            }

        }

    }

    private var onItemClickListener: ((Profile) -> Unit)? = null

    fun setOnItemClickListener(listener: (Profile) -> Unit) {
        onItemClickListener = listener
    }

    override fun getViewToTouchToStartDraggingItem(
        item: Profile,
        viewHolder: ViewHolder,
        position: Int
    ): View? {

        return viewHolder.profileThumb
    }


    override fun onDragFinished(item: Profile, viewHolder: ViewHolder) {
        super.onDragFinished(item, viewHolder)
        Log.d("DragDropAdapter", item.title!!)
//        if (SharedPreferences.getInstance().read("direct", 0) == 1) {
//            if (list.size > 2) {
//                for (item in list) {
//                    item.isDisabled = true
//                }
//
//                list[0].isDisabled = false
//                list[list.size - 1].isDisabled = false
//            }
//
//        }

        notifyDataSetChanged()
    }


    override fun canBeDragged(item: Profile, viewHolder: ViewHolder, position: Int): Boolean {
        return when {
            PreferenceUtils.isDirectOn() -> false
            position == list.size - 1 -> false
            item.isDisabled -> false
            else -> super.canBeDragged(item, viewHolder, position)
        }
    }

    fun refreshList() {
        notifyDataSetChanged()
    }

    fun getRefreshedList() = list

}