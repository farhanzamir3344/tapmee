package com.nfc.tapmee.ui.profile

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.nfc.tapmee.R
import com.nfc.tapmee.arch.providers.profile.ProfileProviderFactory
import com.nfc.tapmee.arch.repositories.profile.ProfileRepository
import com.nfc.tapmee.arch.viewmodels.profile.ProfileViewModel
import com.nfc.tapmee.base.BaseFragment
import com.nfc.tapmee.base.util.FragmentUtil
import com.nfc.tapmee.common.models.ProfileX
import com.nfc.tapmee.common.utils.KeyboardOp
import com.nfc.tapmee.common.utils.alerts.SnackBarUtil
import com.nfc.tapmee.common.utils.dialog.AlertOP
import com.nfc.tapmee.common.utils.dialog.GeneralDialogListener
import com.nfc.tapmee.common.utils.generics.Resource
import com.nfc.tapmee.common.utils.profile.HandleOpenProfile
import com.nfc.tapmee.databinding.FragmentProfileDetailBinding


class ProfileDetailFragment private constructor() : BaseFragment() {

    private lateinit var binding: FragmentProfileDetailBinding
    private lateinit var viewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_profile_detail,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listeners()
        renderProfile()
        initViewModel()
        handleResponse()
    }

    //region VIEW-MODEL
    private fun initViewModel() {

        val viewModelProviderFactory =
            ProfileProviderFactory(requireActivity().application, ProfileRepository())
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory).get(ProfileViewModel::class.java)


    }
    //endregion

    //region HELPING METHODS
    private fun listeners() {

        binding.ccpProfileNumber.registerPhoneNumberTextView(binding.etLinkProfileNumber)

        binding.ivCloseScreen.setOnClickListener {
            FragmentUtil(requireActivity() as AppCompatActivity).goBackFragment()
        }

        binding.btnOpenProfile.setOnClickListener {
            if (profileX!!.profileLinks!!.isNotEmpty()) {
                val profileDetail = profileX!!.profileLinks!![0]
                HandleOpenProfile.openProfile(
                    profileDetail.profileLink.toString(),
                    requireContext(),
                    profileX!!.title!!
                )
            }

        }

        binding.btnSaveProfile.setOnClickListener {
            var link = ""

            link = if (profileX!!.hint == "number") {
                binding.ccpProfileNumber.fullNumberWithPlus
            } else
                binding.etLinkProfile.text.toString()


            if (link.isNotEmpty()) {
                KeyboardOp.hide(requireActivity())

                if (profileX!!.profileLinks.isNullOrEmpty())
                    viewModel.addProfile(link, profileX!!.profileCode!!, "no")
                else
                    viewModel.updateSocialProfile(
                        link = link,
                        profileID = profileX!!.profileLinks!![0].id.toString(),
                        isBusiness = "no"
                    )
            }
        }


        binding.ivDelete.setOnClickListener {

            AlertOP.showAlert(requireContext(),
                getString(R.string.lbl_delete_link),
                "${getString(R.string.lbl_delete_msg)} ${profileX!!.title}?",
                getString(R.string.lbl_dialog_yes),
                getString(R.string.cancel),
                object : GeneralDialogListener {
                    override fun onPositiveClick() {
                        viewModel.deleteSocialProfile(profileID = profileX!!.profileLinks!![0].id.toString())
                    }

                    override fun onNegativeClick() {

                    }
                })
        }
    }

    private fun renderProfile() {

        Glide.with(requireContext())
            .load(profileX!!.icon)
            .into(
                binding.ivProfileThumb

            )

        binding.tvTitle.text = profileX!!.title


        if (!profileX!!.hint.isNullOrEmpty()) {
            when (profileX!!.hint) {
                "url" -> {
                    binding.etLinkProfile.hint =
                        "${profileX!!.title} ${getString(R.string.lbl_profile_link)}"
                    binding.etLinkProfile.inputType = InputType.TYPE_TEXT_VARIATION_URI
                }
                "username" -> {
                    binding.etLinkProfile.hint =
                        "${profileX!!.title} ${getString(R.string.lbl_username)}"
                    binding.etLinkProfile.inputType = InputType.TYPE_CLASS_TEXT
                }
                "number" -> {
//                    binding.etLinkProfile.hint =
//                        "${profileX!!.title} ${getString(R.string.lbl_number)}"
//                    binding.etLinkProfile.inputType = InputType.TYPE_CLASS_PHONE

                    binding.profileDataContainer.visibility = View.GONE
                    binding.profileNumberContainer.visibility = View.VISIBLE

                }
                "other" -> {
                    binding.etLinkProfile.hint =
                        "${getString(R.string.lbl_link_to)} ${profileX!!.title}"
                    binding.etLinkProfile.inputType = InputType.TYPE_CLASS_TEXT
                }
                else -> {
                    binding.etLinkProfile.hint =
                        "${getString(R.string.lbl_link_to)} ${profileX!!.title}"
                    binding.etLinkProfile.inputType = InputType.TYPE_CLASS_TEXT
                }
            }

            when (profileX!!.profileCode) {
                "email" -> {
                    binding.etLinkProfile.hint = getString(R.string.lbl_hint_email)
                    binding.etLinkProfile.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
                }
            }
        } else {
            binding.etLinkProfile.hint = "${getString(R.string.lbl_link_to)} ${profileX!!.title}"
            binding.etLinkProfile.inputType = InputType.TYPE_CLASS_TEXT
        }

        if (profileX!!.profileLinks!!.isNotEmpty()) {

            if (profileX!!.hint == "number") {
                binding.profileDataContainer.visibility = View.GONE
                binding.profileNumberContainer.visibility = View.VISIBLE
                binding.ccpProfileNumber.fullNumber = profileX!!.profileLinks!![0].profileLink

            } else {
                binding.etLinkProfile.setText(profileX!!.profileLinks!![0].profileLink)
            }

            binding.ivDelete.visibility = View.VISIBLE
            binding.btnSaveProfile.text = getString(R.string.lbl_btn_txt_update)

        } else {
            binding.btnSaveProfile.text = getString(R.string.lbl_btn_txt_save)
            binding.ivDelete.visibility = View.GONE
        }

        binding.btnAddAnother.text = "Add Another ${profileX!!.title}"
    }

    private fun EditText.stickPrefix(prefix: String) {
        this.addTextChangedListener(afterTextChanged = {
            if (!it.toString().startsWith(prefix) && it?.isNotEmpty() == true) {
                this.setText(prefix + this.text)
                this.setSelection(this.length())

            }
        })
    }
    //endregion

    //region HANDLE API RESPONSE
    private fun handleResponse() {

        viewModel.addProfileLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    response.data?.let { response ->

                        hideProgressBar()

                        FragmentUtil(requireActivity() as AppCompatActivity).goBackFragment()

                    }

                }
                is Resource.Error -> {
                    hideProgressBar()
                    SnackBarUtil.showSnackBar(requireContext(), response.message!!, true)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })

        viewModel.updateSocialProfileLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    response.data?.let { response ->

                        hideProgressBar()

                        FragmentUtil(requireActivity() as AppCompatActivity).goBackFragment()

                    }

                }
                is Resource.Error -> {
                    hideProgressBar()
                    SnackBarUtil.showSnackBar(requireContext(), response.message!!, true)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })

        viewModel.deleteSocialProfileLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    response.data?.let { response ->

                        hideProgressBar()

                        FragmentUtil(requireActivity() as AppCompatActivity).goBackFragment()

                    }

                }
                is Resource.Error -> {
                    hideProgressBar()
                    SnackBarUtil.showSnackBar(requireContext(), response.message!!, true)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })
    }
    //endregion

    //region PROGRESS BAR
    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }
    //endregion

    companion object {
        private var profileX: ProfileX? = null

        @JvmStatic
        fun newInstance(profileX: ProfileX): Fragment {
            this.profileX = profileX
            return ProfileDetailFragment()
        }

    }
}