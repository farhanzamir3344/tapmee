package com.nfc.tapmee.ui.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.nfc.tapmee.R
import com.nfc.tapmee.common.utils.viewpager.PagerAdapter
import com.nfc.tapmee.databinding.FragmentLoginRegisterHomeBinding
import com.nfc.tapmee.ui.auth.login.LoginFragment
import com.nfc.tapmee.ui.auth.register.RegisterFragment

class LoginRegisterHomeFragment private constructor() : Fragment() {
    private lateinit var binding: FragmentLoginRegisterHomeBinding
    private val pagerAdapter: PagerAdapter by lazy {
        PagerAdapter(childFragmentManager)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_login_register_home,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        pagerAdapter.clear()
        pagerAdapter.addFragment(
            LoginFragment.newInstance(),
            getString(R.string.lbl_btn_txt_log_in)
        )
        pagerAdapter.addFragment(
            RegisterFragment.newInstance(),
            getString(R.string.lbl_btn_txt_register)
        )
        binding.viewpager.adapter = pagerAdapter
        binding.tabLayout.setupWithViewPager(binding.viewpager)

    }

    fun selectLoginTab() {
        binding.viewpager.currentItem = 0
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            LoginRegisterHomeFragment()
    }
}