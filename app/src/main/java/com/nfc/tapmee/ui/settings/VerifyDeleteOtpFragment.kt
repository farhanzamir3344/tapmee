package com.nfc.tapmee.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nfc.tapmee.R
import com.nfc.tapmee.arch.providers.auth.LoginViewModelProviderFactory
import com.nfc.tapmee.arch.repositories.auth.LoginRepository
import com.nfc.tapmee.arch.viewmodels.auth.LoginViewModel
import com.nfc.tapmee.base.BaseFragment
import com.nfc.tapmee.base.util.FragmentUtil
import com.nfc.tapmee.common.preferences.SharedPreferences
import com.nfc.tapmee.common.utils.Constants
import com.nfc.tapmee.common.utils.dialog.AlertOP
import com.nfc.tapmee.common.utils.generics.Resource
import com.nfc.tapmee.databinding.FragmentDeleteVerifyOtpBinding
import com.nfc.tapmee.ui.auth.forgotPassword.PasswordResetFragment
import com.poovam.pinedittextfield.PinField

class VerifyDeleteOtpFragment private constructor() : BaseFragment() {

    private lateinit var binding: FragmentDeleteVerifyOtpBinding
    private lateinit var viewModel: LoginViewModel

    private lateinit var email: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        email = SharedPreferences.getInstance().read(Constants.EMAIL, "").toString()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_delete_verify_otp, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        initViewModel()

    }

    //region VIEW-MODEL
    private fun initViewModel() {

        val viewModelProviderFactory = LoginViewModelProviderFactory(
            requireActivity().application,
            LoginRepository()
        )
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory).get(LoginViewModel::class.java)


        handleApiResponse()
    }
    //endregion

    private fun init() {

        validate()

        binding.tvCancel.setOnClickListener {
            FragmentUtil(requireActivity() as AppCompatActivity).goBackFragment()
        }

        binding.btnVerify.setOnClickListener {

            if (binding.squareField.text.toString().isNotEmpty())
                viewModel.verifyDeleteOTP(
                    reason = "Delete Account",
                    otpCode = binding.squareField.text.toString()
                )
        }
    }

    //region HELPING METHODS
    private fun validate() {

        val listener = object : PinField.OnTextCompleteListener {
            override fun onTextComplete(enteredText: String): Boolean {
                binding.btnVerify.background =
                    ContextCompat.getDrawable(requireContext(), R.drawable.bg_btn_enabled)
                viewModel.verifyDeleteOTP(reason = "Delete Account", otpCode = enteredText)

                return@onTextComplete true
            }

        }


        binding.squareField.onTextCompleteListener = listener

    }


    private fun navigateToResetPassword() {
        FragmentUtil(requireActivity() as AppCompatActivity).gotoNextFragment(
            PasswordResetFragment.newInstance(email), R.id.lt_login_register_container
        )
    }
    //endregion


    //region API HANDLE RESPONSE
    private fun handleApiResponse() {

        viewModel.verifyOtpLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    viewModel.verifyOtpLiveData.postValue(null)

                    navigateToResetPassword()

                }
                is Resource.Error -> {
                    hideProgressBar()
                    AlertOP.showResponseAlertOK(requireContext(), "Error", response.message)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })
    }
    //endregion


    //region PROGRESS BAR
    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }

    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }
    //endregion

    companion object {
        @JvmStatic
        fun newInstance() =
            VerifyDeleteOtpFragment()
    }
}