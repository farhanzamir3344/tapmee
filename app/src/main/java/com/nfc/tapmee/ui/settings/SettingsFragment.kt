package com.nfc.tapmee.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nfc.tapmee.BuildConfig
import com.nfc.tapmee.R
import com.nfc.tapmee.arch.providers.auth.LoginViewModelProviderFactory
import com.nfc.tapmee.arch.repositories.auth.LoginRepository
import com.nfc.tapmee.arch.viewmodels.auth.LoginViewModel
import com.nfc.tapmee.base.BaseActivity
import com.nfc.tapmee.base.BaseFragment
import com.nfc.tapmee.base.util.FragmentUtil
import com.nfc.tapmee.common.utils.AppOP
import com.nfc.tapmee.common.utils.dialog.AlertOP
import com.nfc.tapmee.common.utils.dialog.GeneralDialogListener
import com.nfc.tapmee.common.utils.generics.Resource
import com.nfc.tapmee.databinding.FragmentSettingsBinding

class SettingsFragment private constructor() : BaseFragment() {
    private lateinit var binding: FragmentSettingsBinding
    private lateinit var viewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listeners()
        initViewModel()
        handleApiResponse()
    }

    //region VIEW-MODEL
    private fun initViewModel() {

        val viewModelProviderFactory = LoginViewModelProviderFactory(
            requireActivity().application,
            LoginRepository()
        )
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory).get(LoginViewModel::class.java)
    }
    //endregion

    private fun listeners() {
        binding.ivShareApp.setOnClickListener {

            //share AddMee
            AppOP.shareApp(
                "Download AddMee now and start popping!\n",
                "https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}",
                requireContext()
            )
        }

        binding.imvBack.setOnClickListener {

            requireActivity().finish()

        }


        binding.tvTerms.setOnClickListener {

            (activity as BaseActivity).gotoWeb(
                getString(R.string.lbl_terms_url),
                getString(R.string.lbl_terms)
            )

        }

        binding.tvDataProtection.setOnClickListener {

            (activity as BaseActivity).gotoWeb(
                getString(R.string.lbl_privacy_policy_url),
                getString(R.string.lbl_privacy_policy)
            )
        }

        binding.tvLegal.setOnClickListener {

            (activity as BaseActivity).gotoWeb(
                getString(R.string.lbl_legal_notice_url),
                getString(R.string.lbl_legal_notice)
            )

        }

        binding.tvDeleteAccount.setOnClickListener {
            AlertOP.showAlert(
                requireContext(),
                getString(R.string.lbl_alert_delete_account),
                getString(R.string.msg_alert_delete_account),
                getString(R.string.lbl_btn_delete_account),
                getString(R.string.cancel),
                object : GeneralDialogListener {
                    override fun onPositiveClick() {
                        viewModel.deleteRequest("Delete Account")
                    }

                    override fun onNegativeClick() {

                    }
                }
            )
        }
    }

    private fun navigateToVerifyDeleteAccount() {
        FragmentUtil(requireActivity() as AppCompatActivity).gotoNextFragment(
            VerifyDeleteOtpFragment.newInstance(),
            R.id.flSettings
        )
    }

    //region API HANDLE RESPONSE
    private fun handleApiResponse() {

        viewModel.deleteRequestLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    viewModel.deleteRequestLiveData.postValue(null)

                    navigateToVerifyDeleteAccount()

                }
                is Resource.Error -> {
                    hideProgressBar()
                    AlertOP.showResponseAlertOK(requireContext(), "Error", response.message)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })
    }
    //endregion

    //region PROGRESS BAR
    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }

    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }
    //endregion

    companion object {
        @JvmStatic
        fun newInstance() =
            SettingsFragment()
    }
}