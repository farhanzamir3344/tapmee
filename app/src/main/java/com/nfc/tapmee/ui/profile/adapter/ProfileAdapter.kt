package com.nfc.tapmee.ui.profile.adapter

import android.content.Context
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.nfc.tapmee.R
import com.nfc.tapmee.common.models.ProfileX
import kotlinx.android.synthetic.main.layout_item_profile.view.*
import java.util.*

class ProfileAdapter() :
    RecyclerView.Adapter<ProfileAdapter.ProfileViewHolder>() {

    inner class ProfileViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val differConfig = object : DiffUtil.ItemCallback<ProfileX>() {

        override fun areItemsTheSame(oldItem: ProfileX, newItem: ProfileX): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: ProfileX, newItem: ProfileX): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differConfig)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileViewHolder {
        return ProfileViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.layout_item_profile,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ProfileViewHolder, position: Int) {
        val profile = differ.currentList[position]
        holder.itemView.apply {

            if (profile.title!!.contains("Contact Card") ||
                profile.profileCode!!.contains("contact-card")
            ) {
                visibility = View.GONE

            } else {

                if (Locale.getDefault().language == "de")
                    tvProfileName.text = profile.titleDe
                else
                    tvProfileName.text = profile.title

                Glide.with(context)  //2
                    .load(profile.icon)
                    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .into(ivProfileThumb)

                if (profile.isAdded == 1) {
                    ivAddedThumb.visibility = View.VISIBLE
                    cardView.strokeWidth = 4.toDp(context)
                    cardView.strokeColor = ContextCompat.getColor(context, R.color.green)
                }



                setOnClickListener {
                    onItemClickListener?.let { it(profile) }
                }


                if (profile.isPro == 1)
                    ivProThumb.visibility = View.VISIBLE
            }


        }
    }

    // extension method to convert pixels to dp
    fun Int.toDp(context: Context): Int = TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP, this.toFloat(), context.resources.displayMetrics
    ).toInt()

    override fun getItemCount(): Int {
        return differ.currentList.size
    }


    private var onItemClickListener: ((ProfileX) -> Unit)? = null

    fun setOnItemClickListener(listener: (ProfileX) -> Unit) {
        onItemClickListener = listener
    }

}