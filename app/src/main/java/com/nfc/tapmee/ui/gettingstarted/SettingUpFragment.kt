package com.nfc.tapmee.ui.gettingstarted

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.nfc.tapmee.R
import com.nfc.tapmee.base.BaseFragment
import com.nfc.tapmee.databinding.FragmentSettingupAddmeeBinding
import com.nfc.tapmee.ui.activities.AddMeeHelpActivity

class SettingUpFragment : BaseFragment() {
    private lateinit var binding: FragmentSettingupAddmeeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_settingup_addmee,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.btnContinue.setOnClickListener {

            (requireActivity() as AddMeeHelpActivity).selectPageIndex(1)
        }

    }
}