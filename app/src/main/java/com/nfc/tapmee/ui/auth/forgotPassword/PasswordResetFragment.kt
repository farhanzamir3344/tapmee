package com.nfc.tapmee.ui.auth.forgotPassword

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nfc.tapmee.R
import com.nfc.tapmee.arch.providers.auth.LoginViewModelProviderFactory
import com.nfc.tapmee.arch.repositories.auth.LoginRepository
import com.nfc.tapmee.arch.viewmodels.auth.LoginViewModel
import com.nfc.tapmee.base.BaseFragment
import com.nfc.tapmee.base.util.FragmentUtil
import com.nfc.tapmee.common.utils.AppOP
import com.nfc.tapmee.common.utils.Constants.EMAIL
import com.nfc.tapmee.common.utils.KeyboardOp
import com.nfc.tapmee.common.utils.dialog.AlertOP
import com.nfc.tapmee.common.utils.dialog.GeneralDialogListener
import com.nfc.tapmee.common.utils.generics.Resource
import com.nfc.tapmee.databinding.FragmentPasswordResetBinding


class PasswordResetFragment private constructor() : BaseFragment() {

    private lateinit var binding: FragmentPasswordResetBinding
    private lateinit var viewModel: LoginViewModel
    private var isValidInputs = false
    private lateinit var email: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            email = it.getString(EMAIL, "")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_password_reset, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        initViewModel()
        handleApiResponse()
    }


    //region VIEW-MODEL
    private fun initViewModel() {

        val viewModelProviderFactory =
            LoginViewModelProviderFactory(requireActivity().application, LoginRepository())
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory).get(LoginViewModel::class.java)
    }
    //endregion

    private fun init() {

        validate()

        binding.btnReset.setOnClickListener {
            KeyboardOp.hide(requireActivity(), binding.confirmPassword)

            val password = binding.loginPassword.text.toString()

            if (isValidInputs) {
                viewModel.resetPassword(email = email, password = password)
            }

        }
        binding.tvGoHome.setOnClickListener {
            navigatBackHome()
        }

    }


    //region HELPING METHODS
    private fun validate() {


        binding.loginPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {

                val password = binding.loginPassword.text.toString()
                val confirmPassword = binding.confirmPassword.text.toString()

                if (password.isNotEmpty() &&
                    confirmPassword.isNotEmpty() &&
                    password == confirmPassword
                ) {

                    isValidInputs = true
                    binding.btnReset.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.bg_btn_enabled)
                } else {
                    isValidInputs = false
                    binding.btnReset.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.bg_btn_disabled)
                }
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })


        binding.confirmPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {

                val password = binding.loginPassword.text.toString()
                val confirmPassword = binding.confirmPassword.text.toString()

                if (password.isNotEmpty() &&
                    confirmPassword.isNotEmpty() &&
                    password == confirmPassword
                ) {

                    isValidInputs = true
                    binding.btnReset.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.bg_btn_enabled)
                } else {
                    isValidInputs = false
                    binding.btnReset.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.bg_btn_disabled)
                }
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })
    }

    private fun navigatBackHome() {
        FragmentUtil(requireActivity() as AppCompatActivity).goBackFragment(3)
    }
    //endregion

    //region API HANDLE RESPONSE
    private fun handleApiResponse() {

        viewModel.setPasswordLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    AlertOP.run {
                        showResponseAlertOK(
                            requireContext(),
                            getString(R.string.lbl_reset_password),
                            getString(R.string.msg_reset_password),
                            object : GeneralDialogListener {
                                override fun onPositiveClick() {

                                    AppOP.launchLoginActivityClearAll(requireActivity())
                                }

                                override fun onNegativeClick() {

                                }
                            }
                        )
                    }
                }
                is Resource.Error -> {
                    hideProgressBar()

                    AlertOP.showResponseAlertOK(requireContext(), "Error", response.message)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })
    }
    //endregion

    //region PROGRESS BAR
    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }

    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }
    //endregion


    companion object {
        @JvmStatic
        fun newInstance(email: String) =
            PasswordResetFragment()
                .apply {
                    val bundle = Bundle()
                    bundle.putString(EMAIL, email)
                    arguments = bundle
                }
    }
}