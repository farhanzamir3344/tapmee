package com.nfc.tapmee.ui.auth.forgotPassword

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nfc.tapmee.R
import com.nfc.tapmee.arch.providers.auth.LoginViewModelProviderFactory
import com.nfc.tapmee.arch.repositories.auth.LoginRepository
import com.nfc.tapmee.arch.viewmodels.auth.LoginViewModel
import com.nfc.tapmee.base.BaseFragment
import com.nfc.tapmee.base.util.FragmentUtil
import com.nfc.tapmee.common.utils.KeyboardOp
import com.nfc.tapmee.common.utils.dialog.AlertOP
import com.nfc.tapmee.common.utils.generics.Resource
import com.nfc.tapmee.databinding.FragmentForgotPasswordBinding


class ForgotPasswordFragment private constructor() : BaseFragment() {

    private lateinit var binding: FragmentForgotPasswordBinding
    private lateinit var viewModel: LoginViewModel
    private var isValidInputs = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_password, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        initViewModel()
        handleApiResponse()
    }


    //region VIEW-MODEL
    private fun initViewModel() {

        val viewModelProviderFactory =
            LoginViewModelProviderFactory(requireActivity().application, LoginRepository())
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory).get(LoginViewModel::class.java)
    }
    //endregion

    private fun init() {

        validate()

        binding.btnSendCode.setOnClickListener {

            KeyboardOp.hide(requireActivity(), binding.loginEmail)
            val email = binding.loginEmail.text!!.trim().toString()

            if (isValidInputs)
                viewModel.forgotPassword(email)
        }

        binding.tvGoHome.setOnClickListener {
            navigateBack()
        }
    }

    //region HELPING METHODS
    private fun validate() {


        binding.loginEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                val email = binding.loginEmail.text.toString()

                if (email.isNotEmpty() &&
                    Patterns.EMAIL_ADDRESS.matcher(email).matches()
                ) {
                    isValidInputs = true
                    binding.btnSendCode.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.bg_btn_enabled)
                } else {
                    isValidInputs = false
                    binding.btnSendCode.background =
                        ContextCompat.getDrawable(requireContext(), R.drawable.bg_btn_disabled)
                }
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })


    }

    private fun navigateBack() {
        FragmentUtil(requireActivity() as AppCompatActivity).goBackFragment()
    }

    private fun navigateToVerifyOtp() {
        FragmentUtil(requireActivity() as AppCompatActivity).gotoNextFragment(
            VerifyOtpFragment.newInstance(binding.loginEmail.text.toString()),
            R.id.lt_login_register_container
        )
    }
    //endregion


    //region API HANDLE RESPONSE
    private fun handleApiResponse() {

        viewModel.forgotPasswordLiveData.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()

                    navigateToVerifyOtp()

                }
                is Resource.Error -> {
                    hideProgressBar()
                    AlertOP.showResponseAlertOK(requireContext(), "Error", response.message)
                }
                is Resource.Loading -> {
                    showProgressBar()
                }
            }
        })
    }
    //endregion


    //region PROGRESS BAR
    private fun hideProgressBar() {
        binding.progressBar.visibility = View.GONE
    }

    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }
    //endregion


    companion object {
        @JvmStatic
        fun newInstance() =
            ForgotPasswordFragment()
    }
}