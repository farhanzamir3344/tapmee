package com.nfc.tapmee.ui.home

import android.animation.ArgbEvaluator
import android.animation.ObjectAnimator
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import androidx.core.animation.addListener
import androidx.databinding.DataBindingUtil
import com.nfc.tapmee.R
import com.nfc.tapmee.base.BaseFragment
import com.nfc.tapmee.common.utils.viewpager.AddmeePagerAdaptor
import com.nfc.tapmee.common.utils.viewpager.OnPageScrolled
import com.nfc.tapmee.common.utils.views.VerticalFlipTransformation
import com.nfc.tapmee.databinding.FragmentHomeBinding
import com.nfc.tapmee.ui.home.business_profile.BusinessProfileFragment
import com.nfc.tapmee.ui.home.personal_profile.PersonProfileFragment


class HomeFragment private constructor() : BaseFragment(), OnPageScrolled {
    private lateinit var binding: FragmentHomeBinding
    private lateinit var backFadAnimation: ObjectAnimator
    private val pagerAdaptor: AddmeePagerAdaptor by lazy {
        AddmeePagerAdaptor(childFragmentManager, this)
    }

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {

        val personProfileFragment = PersonProfileFragment()
        val businessProfileFragment = BusinessProfileFragment()
        pagerAdaptor.addFragment(personProfileFragment, "Personal")
//        pagerAdaptor.addFragment(businessProfileFragment, "Business")
        val pagerVerticalFlip = VerticalFlipTransformation()
        binding.profilePager.setPageTransformer(true, pagerVerticalFlip)
        binding.profilePager.adapter = pagerAdaptor
        binding.profilePager.addOnPageChangeListener(pagerAdaptor)

//        setRightLoadClick()
//        setLeftLoadClick()
//        createProfilIndicatorBgAnimation()
    }

    private fun setRightLoadClick() {
        binding.imgvRightLoad.setOnClickListener {
            binding.profilePager.currentItem = 1
        }
    }

    private fun setLeftLoadClick() {
        binding.imgvLeftLoad.setOnClickListener {
            binding.profilePager.currentItem = 0
        }
    }

    private fun createProfilIndicatorBgAnimation() {
        val fromColor = Color.BLACK
        val toColor = Color.TRANSPARENT
        backFadAnimation = ObjectAnimator.ofObject(
            view,
            "backgroundTint",
            ArgbEvaluator(),
            fromColor,
            toColor
        )

        backFadAnimation.interpolator = DecelerateInterpolator(2f)
        backFadAnimation.duration = 700
//        backFadAnimation.target = binding.tvProfileIndicator
        backFadAnimation.addListener({
            binding.tvProfileIndicator.visibility = View.GONE
        }, {
            binding.tvProfileIndicator.visibility = View.VISIBLE
        })
        backFadAnimation.addUpdateListener {
            val animatedValue = it.animatedValue as Int
            binding.tvProfileIndicator.backgroundTintList = ColorStateList.valueOf(animatedValue)
        }
    }


    private fun setProfileIndicator(title: String) {
        binding.tvProfileIndicator.visibility = View.VISIBLE
        binding.tvProfileIndicator.text = title
        backFadAnimation.start()
    }

    var isStarted = false
    var lastPosition = -1
    override fun onPageScrolled(position: Int) {
        if (!isStarted) {
            isStarted = true
            return
        }
        if (lastPosition != position) {
            lastPosition = position
        } else {
            return
        }

        val title = pagerAdaptor.getPageTitle(position).toString()

//        setProfileIndicator(title)

//        if (position == 0) {
//            binding.imgvLeftLoad.visibility = View.GONE
//            binding.imgvRightLoad.visibility = View.VISIBLE
//        } else {
//            binding.imgvLeftLoad.visibility = View.VISIBLE
//            binding.imgvRightLoad.visibility = View.GONE
//        }
    }
}