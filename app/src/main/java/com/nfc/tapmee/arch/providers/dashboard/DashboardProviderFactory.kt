package com.nfc.tapmee.arch.providers.dashboard

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nfc.tapmee.arch.repositories.dashboard.DashboardRepository
import com.nfc.tapmee.arch.viewmodels.dashboard.DashboardViewModel

class DashboardProviderFactory(
    val app: Application,
    private val dashboardRepository: DashboardRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return DashboardViewModel(app, dashboardRepository) as T
    }
}