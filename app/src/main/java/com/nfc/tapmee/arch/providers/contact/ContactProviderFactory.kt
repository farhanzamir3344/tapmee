package com.nfc.tapmee.arch.providers.contact

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nfc.tapmee.arch.repositories.contact.ContactRepository
import com.nfc.tapmee.arch.viewmodels.contact.ContactViewModel

class ContactProviderFactory(
    val app: Application,
    private val contactRepository: ContactRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return ContactViewModel(app, contactRepository) as T
    }
}