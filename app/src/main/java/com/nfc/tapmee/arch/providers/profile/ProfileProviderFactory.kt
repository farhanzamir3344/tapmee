package com.nfc.tapmee.arch.providers.profile

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nfc.tapmee.arch.repositories.profile.ProfileRepository
import com.nfc.tapmee.arch.viewmodels.profile.ProfileViewModel

class ProfileProviderFactory(
    val app: Application,
    private val profileRepository: ProfileRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return ProfileViewModel(app, profileRepository) as T
    }
}