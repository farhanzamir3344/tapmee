package com.nfc.tapmee.arch.viewmodels.contact

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.nfc.tapmee.arch.repositories.contact.ContactRepository
import com.nfc.tapmee.arch.viewmodels.BaseViewModel
import com.nfc.tapmee.common.models.ContactResponseDTO
import com.nfc.tapmee.common.models.GenericResponseDto
import com.nfc.tapmee.common.models.SuccessDTO
import com.nfc.tapmee.common.models.UserNote
import com.nfc.tapmee.common.utils.generics.Resource
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class ContactViewModel(app: Application, private val contactRepository: ContactRepository) :
    BaseViewModel(app = app) {
    val notesLiveData: MutableLiveData<Resource<ArrayList<UserNote>>> = MutableLiveData()
    val addNoteLiveData: MutableLiveData<Resource<SuccessDTO>> = MutableLiveData()

    fun getContactsList() = viewModelScope.launch {
        userNotesCall()
    }

    fun addNote(
        userId: Int,
        name: String,
        email: String,
        phoneNo: String,
        note: String
    ) = viewModelScope.launch {
        addNoteCall(
            name = name,
            email = email,
            phoneNo = phoneNo,
            note = note,
            userId = userId
        )
    }

    private suspend fun userNotesCall() {

        notesLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    contactRepository.getContactsList()

                notesLiveData.postValue(handleUserNotesResponse(response))
            } else {

                notesLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> notesLiveData.postValue(Resource.Error("Network Failure"))
                else -> notesLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    private suspend fun addNoteCall(
        userId: Int,
        name: String,
        email: String,
        phoneNo: String,
        note: String
    ) {

        addNoteLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    contactRepository.addNotes(
                        name = name,
                        email = email,
                        phoneNo = phoneNo,
                        note = note,
                        userId = userId
                    )

                addNoteLiveData.postValue(handleAddNoteResponse(response))
            } else {

                addNoteLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> addNoteLiveData.postValue(Resource.Error("Network Failure"))
                else -> addNoteLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    /**
     * Handle MyProfiles API response and return [ContactResponseDTO] model and Response status
     */
    private fun handleUserNotesResponse(response: Response<GenericResponseDto<ContactResponseDTO>>): Resource<ArrayList<UserNote>> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                if (resultResponse.success == true) {
                    val notesResponse = resultResponse.data!!.userNotes as ArrayList<UserNote>


                    return Resource.Success(notesResponse)
                }

            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }


    /**
     * Handle AddNote API response and return [SuccessDTO] model and Response status
     */
    private fun handleAddNoteResponse(response: Response<SuccessDTO>): Resource<SuccessDTO> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                if (resultResponse.success == true) {

                    return Resource.Success(resultResponse)
                }

            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }
}