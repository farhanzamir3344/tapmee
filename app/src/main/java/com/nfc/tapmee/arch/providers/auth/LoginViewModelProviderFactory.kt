package com.nfc.tapmee.arch.providers.auth

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nfc.tapmee.arch.repositories.auth.LoginRepository
import com.nfc.tapmee.arch.viewmodels.auth.LoginViewModel

class LoginViewModelProviderFactory(
    val app: Application,
    private val loginRepository: LoginRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return LoginViewModel(app, loginRepository) as T
    }
}