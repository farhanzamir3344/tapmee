package com.nfc.tapmee.arch.viewmodels.profile

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.nfc.tapmee.arch.repositories.profile.ProfileRepository
import com.nfc.tapmee.arch.viewmodels.BaseViewModel
import com.nfc.tapmee.common.models.*
import com.nfc.tapmee.common.utils.generics.Resource
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import retrofit2.Response
import java.io.IOException

class
ProfileViewModel(app: Application, private val profileRepository: ProfileRepository) :
    BaseViewModel(app = app) {

    val profileLiveData: MutableLiveData<Resource<CustomerDTO>> = MutableLiveData()
    val myProfilesLiveData: MutableLiveData<Resource<MyProfilesResponseDTO>> = MutableLiveData()
    val publicProfileLiveData: MutableLiveData<Resource<MyProfilesResponseDTO>> = MutableLiveData()
    val profilesLiveData: MutableLiveData<Resource<ArrayList<ProfileType>>> = MutableLiveData()
    val contactCardLiveData: MutableLiveData<Resource<ArrayList<ContactCard>>> = MutableLiveData()
    val profileBannerLiveData: MutableLiveData<Resource<SuccessDTO>> = MutableLiveData()
    val updateProfileLiveData: MutableLiveData<Resource<CustomerDTO>> = MutableLiveData()
    val updateSocialProfileLiveData: MutableLiveData<Resource<SuccessDTO>> = MutableLiveData()
    val deleteSocialProfileLiveData: MutableLiveData<Resource<SuccessDTO>> = MutableLiveData()
    val updateContactCardLiveData: MutableLiveData<Resource<SuccessDTO>> = MutableLiveData()
    val addProfileLiveData: MutableLiveData<Resource<SuccessDTO>> = MutableLiveData()
    val updateBasicProfileLiveData: MutableLiveData<Resource<CustomerDTO>> = MutableLiveData()


    fun getProfile() = viewModelScope.launch {
        getProfileCall()
    }

    fun getMyProfiles() = viewModelScope.launch {
        myProfilesCall()
    }

    fun getPublicProfiles(username: String) = viewModelScope.launch {
        publicProfilesCall(username = username)
    }

    fun getAllSocialProfile() = viewModelScope.launch {
        getProfilesCall()
    }

    fun getContactCard(isBusiness: String) = viewModelScope.launch {
        getContactCardCall(isBusiness = isBusiness)
    }

    fun addProfile(
        link: String,
        code: String,
        isBusiness: String
    ) = viewModelScope.launch {
        addProfileCall(
            code = code,
            link = link,
            isBusiness = isBusiness
        )
    }

    fun updateSocialProfile(
        link: String,
        profileID: String,
        isBusiness: String
    ) = viewModelScope.launch {
        updateSocialProfileCall(
            profileID = profileID,
            link = link,
            isBusiness = isBusiness
        )
    }

    fun deleteSocialProfile(
        profileID: String
    ) = viewModelScope.launch {
        deleteSocialProfileCall(
            profileID = profileID
        )
    }

    fun updateProfile(
        name: String,
        bio: String,
        gender: Int,
        isPublic: Int,
        dob: String
    ) = viewModelScope.launch {
        updateProfileCall(
            name = name,
            bio = bio,
            gender = gender,
            dob = dob,
            isPublic = isPublic
        )
    }


    fun updateContactCard(
        profileIDs: String,
        isBusiness: String
    ) = viewModelScope.launch {
        updateContactCardCall(
            profileIDs = profileIDs,
            isBusiness = isBusiness
        )
    }

    fun updateProfileBanner(file: MultipartBody.Part) = viewModelScope.launch {
        updateProfileBannerCall(file)
    }


    private suspend fun getProfileCall() {

        profileLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    profileRepository.getProfileResponse()

                profileLiveData.postValue(handleUpdateProfileResponse(response))
            } else {
                profileLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> profileLiveData.postValue(Resource.Error("Network Failure"))
                else -> profileLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    private suspend fun publicProfilesCall(username: String) {

        publicProfileLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    profileRepository.getPublicProfileResponse(username = username)

                publicProfileLiveData.postValue(handlePublicProfileResponse(response))
            } else {
                publicProfileLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> publicProfileLiveData.postValue(Resource.Error("Network Failure"))
                else -> publicProfileLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    private suspend fun myProfilesCall() {

        myProfilesLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    profileRepository.getMyProfilesResponse(type = "all")

                myProfilesLiveData.postValue(handleMyProfileResponse(response))
            } else {
                myProfilesLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> myProfilesLiveData.postValue(Resource.Error("Network Failure"))
                else -> myProfilesLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    private suspend fun getProfilesCall() {

        profilesLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response = profileRepository.getAllProfiles()

                profilesLiveData.postValue(handleProfilesListResponse(response))
            } else {
                profilesLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> profilesLiveData.postValue(Resource.Error("Network Failure"))
                else -> profilesLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }
    }

    private suspend fun getContactCardCall(isBusiness: String) {

        contactCardLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response = profileRepository.getContactCard(isBusiness = isBusiness)

                contactCardLiveData.postValue(handleContactCardResponse(response))
            } else {
                contactCardLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> contactCardLiveData.postValue(Resource.Error("Network Failure"))
                else -> contactCardLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }
    }

    private suspend fun addProfileCall(
        link: String,
        code: String,
        isBusiness: String
    ) {

        addProfileLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    profileRepository.addProfile(
                        code = code,
                        link = link,
                        isBusiness = isBusiness
                    )

                addProfileLiveData.postValue(handleSuccessResponse(response))
            } else {
                addProfileLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> addProfileLiveData.postValue(Resource.Error("Network Failure"))
                else -> addProfileLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    private suspend fun updateSocialProfileCall(
        link: String,
        profileID: String,
        isBusiness: String
    ) {

        updateSocialProfileLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    profileRepository.updateSocialProfile(
                        profileID = profileID,
                        link = link,
                        isBusiness = isBusiness
                    )

                updateSocialProfileLiveData.postValue(handleSuccessResponse(response))
            } else {
                updateSocialProfileLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> updateSocialProfileLiveData.postValue(Resource.Error("Network Failure"))
                else -> updateSocialProfileLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    private suspend fun updateContactCardCall(
        profileIDs: String,
        isBusiness: String
    ) {

        updateContactCardLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    profileRepository.updateContactCard(
                        profileIDs = profileIDs,
                        isBusiness = isBusiness
                    )

                updateContactCardLiveData.postValue(handleSuccessResponse(response))
            } else {
                updateContactCardLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> updateContactCardLiveData.postValue(Resource.Error("Network Failure"))
                else -> updateContactCardLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    private suspend fun deleteSocialProfileCall(
        profileID: String
    ) {

        deleteSocialProfileLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    profileRepository.deleteSocialProfile(
                        profileID = profileID
                    )

                deleteSocialProfileLiveData.postValue(handleSuccessResponse(response))
            } else {
                deleteSocialProfileLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> deleteSocialProfileLiveData.postValue(Resource.Error("Network Failure"))
                else -> deleteSocialProfileLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }




    private suspend fun updateProfileCall(
        name: String,
        bio: String,
        gender: Int,
        isPublic: Int,
        dob: String
    ) {

        updateProfileLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    profileRepository.updateProfile(
                        name = name,
                        bio = bio,
                        gender = gender,
                        dob = dob,
                        isPublic = isPublic
                    )

                updateProfileLiveData.postValue(handleUpdateProfileResponse(response))
            } else {
                updateProfileLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> updateProfileLiveData.postValue(Resource.Error("Network Failure"))
                else -> updateProfileLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }


    private suspend fun updateProfileBannerCall(file: MultipartBody.Part) {

        profileBannerLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response = profileRepository.updateProfileBanner(file)

                profileBannerLiveData.postValue(handleSuccessResponse(response))
            } else {
                profileBannerLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> profileBannerLiveData.postValue(Resource.Error("Network Failure"))
                else -> profileBannerLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }
    }


    /**
     * Handle MyProfiles API response and return [MyProfilesResponseDTO] model and Response status
     */
    private fun handleMyProfileResponse(response: Response<GenericResponseDto<MyProfilesResponseDTO>>): Resource<MyProfilesResponseDTO> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                if (resultResponse.success == true) {
                    val loginUserResponse = resultResponse.data as MyProfilesResponseDTO


                    return Resource.Success(loginUserResponse)
                }

            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }


    private fun handlePublicProfileResponse(response: Response<GenericResponseDto<MyProfilesResponseDTO>>): Resource<MyProfilesResponseDTO> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                if (resultResponse.success == true) {
                    val loginUserResponse = resultResponse.data as MyProfilesResponseDTO


                    return Resource.Success(loginUserResponse)
                }

            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }


    /**
     * Handle Update Basic Profile API response and return [CustomerDTO] model and Response status
     */
    private fun handleUpdateProfileResponse(response: Response<GenericResponseDto<UpdateProfileResponseDTO>>): Resource<CustomerDTO> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                if (resultResponse.success == true) {
                    return Resource.Success(resultResponse.data!!.user as CustomerDTO)
                }

            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }

    /**
     * Handle Get All Profiles API response and return [ProfileResponseDTO] model and Response status
     */
    private fun handleProfilesListResponse(response: Response<GenericResponseDto<ProfileResponseDTO>>): Resource<ArrayList<ProfileType>> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                if (resultResponse.success == true) {
                    val profilesResponse =
                        resultResponse.data!!.profileTypes as ArrayList<ProfileType>

                    return Resource.Success(profilesResponse)
                }

            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }


    /**
     * Handle Get Contact Card API response and return [ContactCardResponseDTO] model and Response status
     */
    private fun handleContactCardResponse(response: Response<GenericResponseDto<ContactCardResponseDTO>>): Resource<ArrayList<ContactCard>> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                if (resultResponse.success == true) {
                    val contactCardResponse =
                        resultResponse.data!!.contactCard as ArrayList<ContactCard>

                    return Resource.Success(contactCardResponse)
                }

            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }

    /**
     * Handle Success response and return [SuccessDTO] model and Response status
     */
    private fun handleSuccessResponse(response: Response<SuccessDTO>): Resource<SuccessDTO> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                if (resultResponse.success == true) {

                    return Resource.Success(resultResponse)
                }

            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }
}