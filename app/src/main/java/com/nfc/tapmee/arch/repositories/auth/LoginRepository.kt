package com.nfc.tapmee.arch.repositories.auth

import android.content.Context
import com.nfc.tapmee.common.models.GenericResponseDto
import com.nfc.tapmee.common.models.LoginResponseDTO
import com.nfc.tapmee.common.models.RegisterResponseDTO
import com.nfc.tapmee.common.network.NetworkApiClient
import com.nfc.tapmee.common.utils.AppOP
import com.nfc.tapmee.common.utils.Constants
import com.nfc.tapmee.common.webservices.*
import retrofit2.Response

class LoginRepository() {

    //Login
    suspend fun getUserLoginResponse(
        email: String,
        password: String
    ): Response<GenericResponseDto<LoginResponseDTO>> {


        return NetworkApiClient.getClientInstance().create(LoginService::class.java)
            .doLogin(email, password, "fcm_token")
    }

    //Social Login
    suspend fun getUserSocialLoginResponse(
        email: String,
        provider: String,
        providerID: String
    ): Response<GenericResponseDto<LoginResponseDTO>> {


        return NetworkApiClient.getClientInstance().create(SocialLoginService::class.java)
            .doSocialLogin(email, provider, providerID, "fcm_token")
    }

    //Register
    suspend fun getUserRegisterResponse(
        email: String,
        password: String,
        allowDataUsage: Int,
        mContext: Context
    ): Response<RegisterResponseDTO> {


        return NetworkApiClient.getClientInstance().create(RegisterService::class.java)
            .doRegister(
                email,
                password,
                password,
                "fcm_token",
                allowDataUsage = allowDataUsage,
                deviceType = Constants.DEVICE_TYPE,
                deviceID = AppOP.deviceID(mContext)
            )
    }


    //Forgot Password
    suspend fun getForgotPasswordResponse(
        email: String
    ) = NetworkApiClient.getClientInstance().create(ForgotPasswordService::class.java)
        .sendOtp(email)


    //Verify OTP
    suspend fun verifyOtpResponse(
        email: String,
        otpCode: String
    ) = NetworkApiClient.getClientInstance().create(VerifyOtpService::class.java)
        .verifyOtp(email = email, otp = otpCode)

    //Delete Request
    suspend fun deleteRequestResponse(
        reason: String
    ) = NetworkApiClient.getClientInstance().create(DeleteRequestService::class.java)
        .deleteRequest(reason = reason)

    //Verify Delete OTP
    suspend fun verifyDeleteOtpResponse(
        reason: String,
        otpCode: String
    ) = NetworkApiClient.getClientInstance().create(VerifyDeleteOtpService::class.java)
        .verifyDeleteOtp(reason = reason, otp = otpCode)

    //set new password
    suspend fun setPasswordResponse(
        email: String,
        password: String
    ) = NetworkApiClient.getClientInstance().create(PasswordResetService::class.java)
        .doReset(email = email, password = password, confirmPassword = password)
}