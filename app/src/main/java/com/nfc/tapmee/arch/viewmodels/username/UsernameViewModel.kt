package com.nfc.tapmee.arch.viewmodels.username

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.nfc.tapmee.arch.repositories.username.UsernameRepository
import com.nfc.tapmee.arch.viewmodels.BaseViewModel
import com.nfc.tapmee.common.models.GenericResponseDto
import com.nfc.tapmee.common.models.UsernameResponseDTO
import com.nfc.tapmee.common.utils.generics.Resource
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class UsernameViewModel(app: Application, private val usernameRepository: UsernameRepository) :
    BaseViewModel(app) {

    val usernameLiveData: MutableLiveData<Resource<UsernameResponseDTO>> = MutableLiveData()
    val updateUsernameLiveData: MutableLiveData<Resource<UsernameResponseDTO>> = MutableLiveData()

    fun verifyUsername(username: String) = viewModelScope.launch {
        verifyUsernameCall(username = username)
    }

    fun updateUsername(username: String) = viewModelScope.launch {
        updateUsernameCall(username = username)
    }

    private suspend fun verifyUsernameCall(username: String) {

        usernameLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response = usernameRepository.getUsernameResponse(username = username)

                usernameLiveData.postValue(handleUsernameResponse(response))
            } else {
                usernameLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> usernameLiveData.postValue(Resource.Error("Network Failure"))
                else -> usernameLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    private suspend fun updateUsernameCall(username: String) {

        updateUsernameLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response = usernameRepository.getUpdateUsernameResponse(username = username)

                updateUsernameLiveData.postValue(handleUsernameResponse(response))
            } else {
                updateUsernameLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> updateUsernameLiveData.postValue(Resource.Error("Network Failure"))
                else -> updateUsernameLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    /**
     * Handle Verify/Update Username API response and return [UsernameResponseDTO] model and Response status
     */
    private fun handleUsernameResponse(response: Response<GenericResponseDto<UsernameResponseDTO>>): Resource<UsernameResponseDTO> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                if (resultResponse.success == true) {
                    val loginUserResponse = resultResponse.data as UsernameResponseDTO


                    return Resource.Success(loginUserResponse)
                }

            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }
}