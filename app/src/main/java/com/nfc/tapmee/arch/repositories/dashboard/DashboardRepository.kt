package com.nfc.tapmee.arch.repositories.dashboard

import com.nfc.tapmee.common.database.dao.BusinessDAO
import com.nfc.tapmee.common.database.dao.CustomerDAO
import com.nfc.tapmee.common.models.BusinessInfo
import com.nfc.tapmee.common.models.CustomerDTO
import com.nfc.tapmee.common.models.GenericResponseDto
import com.nfc.tapmee.common.models.UpdateProfileResponseDTO
import com.nfc.tapmee.common.network.NetworkApiClient
import com.nfc.tapmee.common.webservices.*
import retrofit2.Response

class DashboardRepository(
    private val customerDao: CustomerDAO,
    private val businessDao: BusinessDAO
) {

    suspend fun insert(customerDTO: CustomerDTO) =
        customerDao.saveCustomerDto(customerDTO)

    suspend fun insert(businessInfo: BusinessInfo) =
        businessDao.saveBusinessDto(businessInfo)

    suspend fun getCustomerInfo() = customerDao.getCustomerDto()

    suspend fun getBusinessInfo() = businessDao.getBusinessInfo()

    suspend fun updateCustomerDTO(customerDTO: CustomerDTO) =
        customerDao.update(customerDTO)

    //Get Profile
    suspend fun getProfileResponse() =
        NetworkApiClient.getClientInstance().create(GetUserProfileService::class.java)
            .userProfile()

    //Get My Added Profiles
    suspend fun getMyProfilesResponse(
        type: String
    ) = NetworkApiClient.getClientInstance().create(MyProfilesService::class.java)
        .getMyProfiles(type = type)


    //Update Open/Off Direct Profile
    suspend fun updateOpenDirectProfile(
        profileID: Int,
        openDirectFlag: Int
    ) = NetworkApiClient.getClientInstance().create(UpdateOpenDirectService::class.java)
        .updateOpenDirect(profileID = profileID, isDirect = openDirectFlag)


    //Update Profile
    suspend fun updateProfile(
        name: String,
        bio: String,
        gender: Int,
        isPublic: Int,
        dob: String
    ): Response<GenericResponseDto<UpdateProfileResponseDTO>> {

//        var genderValue = 0
//
//        genderValue = when (gender) {
//            "Male" -> 1
//            "Female" -> 2
//            "Prefer not to share" -> 3
//            "Custom" -> 4
//            else -> 0
//        }

        return NetworkApiClient.getClientInstance().create(UpdateProfileService::class.java)
            .updateProfile(
                name = name,
                bio = bio,
                gender = gender,
                dob = dob,
                isPublic = isPublic
            )
    }

    //Update My Profile Sequence
    suspend fun updateSequence(
        sequence: String
    ) = NetworkApiClient.getClientInstance().create(UpdateProfileSequenceService::class.java)
        .updateSequence(sequence = sequence)
}