package com.nfc.tapmee.arch.viewmodels.dashboard

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.nfc.tapmee.arch.repositories.dashboard.DashboardRepository
import com.nfc.tapmee.arch.viewmodels.BaseViewModel
import com.nfc.tapmee.common.models.*
import com.nfc.tapmee.common.utils.generics.Resource
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class DashboardViewModel(app: Application, private val dashboardRepository: DashboardRepository) :
    BaseViewModel(app = app) {
    val myProfilesLiveData: MutableLiveData<Resource<MyProfilesResponseDTO>> = MutableLiveData()
    val updateDirectLiveData: MutableLiveData<Resource<CustomerDTO>> = MutableLiveData()
    val updateBasicProfileLiveData: MutableLiveData<Resource<CustomerDTO>> = MutableLiveData()
    val profileLiveData: MutableLiveData<Resource<CustomerDTO>> = MutableLiveData()
    val updateSequenceLiveData: MutableLiveData<Resource<SuccessDTO>> = MutableLiveData()

    fun getProfile() = viewModelScope.launch {
        getProfileCall()
    }

    fun getMyProfiles() = viewModelScope.launch {
        myProfilesCall()
    }

    fun updateOpenDirect(profileID: Int, isDirectOpenFlag: Int) = viewModelScope.launch {
        updateOpenDirectCall(profileID, isDirectOpenFlag)
    }


    fun updateProfile(
        name: String,
        bio: String,
        gender: Int,
        isPublic: Int,
        dob: String
    ) = viewModelScope.launch {
        updateProfileCall(
            name = name,
            bio = bio,
            gender = gender,
            dob = dob,
            isPublic = isPublic
        )
    }

    fun updateProfileSequence(sequence: String) = viewModelScope.launch {
        updateProfileSequenceCall(sequence)
    }

    private suspend fun getProfileCall() {

        profileLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    dashboardRepository.getProfileResponse()

                profileLiveData.postValue(handleUpdateProfileResponse(response))
            } else {
                profileLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> profileLiveData.postValue(Resource.Error("Network Failure"))
                else -> profileLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    private suspend fun myProfilesCall() {

        myProfilesLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    dashboardRepository.getMyProfilesResponse(type = "all")

                myProfilesLiveData.postValue(handleMyProfileResponse(response))
            } else {
                myProfilesLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> myProfilesLiveData.postValue(Resource.Error("Network Failure"))
                else -> myProfilesLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }


    private suspend fun updateOpenDirectCall(profileID: Int, isDirectOpenFlag: Int) {

        updateDirectLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    dashboardRepository.updateOpenDirectProfile(
                        profileID = profileID,
                        openDirectFlag = isDirectOpenFlag
                    )

                updateDirectLiveData.postValue(handleUpdateOpenDirectResponse(response))
            } else {
                updateDirectLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> updateDirectLiveData.postValue(Resource.Error("Network Failure"))
                else -> updateDirectLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }


    private suspend fun updateProfileCall(
        name: String,
        bio: String,
        gender: Int,
        isPublic: Int,
        dob: String
    ) {

        updateBasicProfileLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    dashboardRepository.updateProfile(
                        name = name,
                        bio = bio,
                        gender = gender,
                        dob = dob,
                        isPublic = isPublic
                    )

                updateBasicProfileLiveData.postValue(handleUpdateProfileResponse(response))
            } else {
                updateBasicProfileLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> updateBasicProfileLiveData.postValue(Resource.Error("Network Failure"))
                else -> updateBasicProfileLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }


    private suspend fun updateProfileSequenceCall(sequence: String) {

        updateSequenceLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    dashboardRepository.updateSequence(sequence = sequence)

                updateSequenceLiveData.postValue(handleProfileSequenceResponse(response))
            } else {
                updateSequenceLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> updateSequenceLiveData.postValue(Resource.Error("Network Failure"))
                else -> updateSequenceLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }


    /**
     * Handle MyProfiles API response and return [MyProfilesResponseDTO] model and Response status
     */
    private fun handleMyProfileResponse(response: Response<GenericResponseDto<MyProfilesResponseDTO>>): Resource<MyProfilesResponseDTO> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                if (resultResponse.success == true) {
                    val loginUserResponse = resultResponse.data as MyProfilesResponseDTO


                    return Resource.Success(loginUserResponse)
                }

            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }


    /**
     * Handle Update OpenDirect API response and return [CustomerDTO] model and Response status
     */
    private fun handleUpdateOpenDirectResponse(response: Response<GenericResponseDto<UpdateOpenDirectResponseDTO>>): Resource<CustomerDTO> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                if (resultResponse.success == true) {
                    return Resource.Success(resultResponse.data!!.profile as CustomerDTO)
                }

            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }


    /**
     * Handle Update Basic Profile API response and return [CustomerDTO] model and Response status
     */
    private fun handleUpdateProfileResponse(response: Response<GenericResponseDto<UpdateProfileResponseDTO>>): Resource<CustomerDTO> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                if (resultResponse.success == true) {
                    return Resource.Success(resultResponse.data!!.user as CustomerDTO)
                }

            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }


    /**
     * Handle Update Profile Sequence API response and return [SuccessDTO] model and Response status
     */
    private fun handleProfileSequenceResponse(response: Response<SuccessDTO>): Resource<SuccessDTO> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                if (resultResponse.success == true) {
                    return Resource.Success(resultResponse)
                }

            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }


    /**
     * Save [CustomerDTO] to Room Database
     */
    fun saveCustomerDto(customerDTO: CustomerDTO) = viewModelScope.launch {
        dashboardRepository.insert(customerDTO)
    }


    /**
     * Save [BusinessInfo] to Room Database
     */
    fun saveBusinessInfo(businessInfo: BusinessInfo) = viewModelScope.launch {
        dashboardRepository.insert(businessInfo)
    }


    /**
     * Update [CustomerDTO] to Room Database
     */
    fun updateCustomerDto(customerDTO: CustomerDTO) = viewModelScope.launch {
        dashboardRepository.updateCustomerDTO(customerDTO)
    }
}