package com.nfc.tapmee.arch.viewmodels.auth

import android.app.Application
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.nfc.tapmee.arch.repositories.auth.LoginRepository
import com.nfc.tapmee.arch.viewmodels.BaseViewModel
import com.nfc.tapmee.common.models.*
import com.nfc.tapmee.common.utils.generics.Resource
import kotlinx.coroutines.launch
import retrofit2.Response
import java.io.IOException

class LoginViewModel(private val app: Application, private val loginRepository: LoginRepository) :
    BaseViewModel(app) {

    val userLoginLiveData: MutableLiveData<Resource<LoginResponseDTO>> = MutableLiveData()
    val userSocialLoginLiveData: MutableLiveData<Resource<LoginResponseDTO>> = MutableLiveData()
    val registerLiveData: MutableLiveData<Resource<RegisterDTO>> = MutableLiveData()
    val forgotPasswordLiveData: MutableLiveData<Resource<CustomerDTO>> = MutableLiveData()
    val verifyOtpLiveData: MutableLiveData<Resource<CustomerDTO>> = MutableLiveData()
    val verifyDeleteOtpLiveData: MutableLiveData<Resource<CustomerDTO>> = MutableLiveData()
    val deleteRequestLiveData: MutableLiveData<Resource<SuccessDTO>> = MutableLiveData()
    val setPasswordLiveData: MutableLiveData<Resource<SuccessDTO>> = MutableLiveData()

    fun loginUser(email: String, password: String) = viewModelScope.launch {
        loginUserCall(email, password)
    }

    fun socialLoginUser(email: String, provider: String, providerID: String) =
        viewModelScope.launch {
            socialLoginUserCall(email, provider, providerID)
        }

    fun registerUser(email: String, password: String, allowDataUsage: Int) = viewModelScope.launch {
        registerUserCall(email, password, allowDataUsage)
    }

    fun forgotPassword(email: String) = viewModelScope.launch {
        forgotPasswordCall(email)
    }

    fun verifyOTP(email: String, otpCode: String) = viewModelScope.launch {
        verifyOTPCall(email, otpCode)
    }

    fun deleteRequest(reason: String) = viewModelScope.launch {
        deleteRequestCall(reason)
    }

    fun verifyDeleteOTP(reason: String, otpCode: String) = viewModelScope.launch {
        verifyDeleteOTPCall(reason, otpCode)
    }

    fun resetPassword(email: String, password: String) = viewModelScope.launch {
        setPasswordCall(email, password)
    }

    private suspend fun loginUserCall(email: String, password: String) {

        userLoginLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    loginRepository.getUserLoginResponse(email = email, password = password)

                userLoginLiveData.postValue(handleLoginUserResponse(response))
            } else {
                userLoginLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> userLoginLiveData.postValue(Resource.Error("Network Failure"))
                else -> userLoginLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    private suspend fun socialLoginUserCall(email: String, provider: String, providerID: String) {

        userSocialLoginLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    loginRepository.getUserSocialLoginResponse(
                        email = email,
                        provider = provider,
                        providerID = providerID
                    )

                userSocialLoginLiveData.postValue(handleLoginUserResponse(response))
            } else {
                userSocialLoginLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> userSocialLoginLiveData.postValue(Resource.Error("Network Failure"))
                else -> userSocialLoginLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    private suspend fun registerUserCall(email: String, password: String, allowDataUsage: Int) {

        registerLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    loginRepository.getUserRegisterResponse(
                        email = email,
                        password = password,
                        allowDataUsage = allowDataUsage,
                        mContext = app
                    )

                registerLiveData.postValue(handleRegisterUserResponse(response))
            } else {
                registerLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> registerLiveData.postValue(Resource.Error("Network Failure"))
                else -> registerLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    private suspend fun forgotPasswordCall(email: String) {

        forgotPasswordLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response = loginRepository.getForgotPasswordResponse(email = email)

                forgotPasswordLiveData.postValue(handleForgotPasswordResponse(response))
            } else {
                forgotPasswordLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> forgotPasswordLiveData.postValue(Resource.Error("Network Failure"))
                else -> forgotPasswordLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    private suspend fun deleteRequestCall(reason: String) {

        deleteRequestLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response = loginRepository.deleteRequestResponse(reason = reason)

                deleteRequestLiveData.postValue(handleResponse(response))
            } else {
                deleteRequestLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> deleteRequestLiveData.postValue(Resource.Error("Network Failure"))
                else -> deleteRequestLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    private suspend fun verifyDeleteOTPCall(reason: String, otpCode: String) {

        verifyDeleteOtpLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    loginRepository.verifyDeleteOtpResponse(reason = reason, otpCode = otpCode)

                verifyDeleteOtpLiveData.postValue(handleForgotPasswordResponse(response))
            } else {
                verifyDeleteOtpLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> verifyDeleteOtpLiveData.postValue(Resource.Error("Network Failure"))
                else -> verifyDeleteOtpLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    private suspend fun verifyOTPCall(email: String, otpCode: String) {

        verifyOtpLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response = loginRepository.verifyOtpResponse(email = email, otpCode = otpCode)

                verifyOtpLiveData.postValue(handleForgotPasswordResponse(response))
            } else {
                verifyOtpLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> verifyOtpLiveData.postValue(Resource.Error("Network Failure"))
                else -> verifyOtpLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }


    private suspend fun setPasswordCall(email: String, password: String) {

        setPasswordLiveData.postValue(Resource.Loading())

        try {
            if (hasInternetConnection()) {
                val response =
                    loginRepository.setPasswordResponse(email = email, password = password)

                setPasswordLiveData.postValue(handleResponse(response))
            } else {
                setPasswordLiveData.postValue(Resource.Error("No internet connection"))
            }
        } catch (t: Throwable) {
            t.printStackTrace()
            when (t) {
                is IOException -> setPasswordLiveData.postValue(Resource.Error("Network Failure"))
                else -> setPasswordLiveData.postValue(Resource.Error("Conversion Error"))
            }
        }

    }

    /**
     * Handle Login API response and return [LoginResponseDTO] model and Response status
     */
    private fun handleLoginUserResponse(response: Response<GenericResponseDto<LoginResponseDTO>>): Resource<LoginResponseDTO> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                if (resultResponse.success == true) {
                    val loginUserResponse = resultResponse.data as LoginResponseDTO

                    return Resource.Success(loginUserResponse)
                }

            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }


    /**
     * Handle Register API response and return [LoginResponseDTO] model and Response status
     */
    private fun handleRegisterUserResponse(response: Response<RegisterResponseDTO>): Resource<RegisterDTO> {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->

                if (resultResponse.success) {
                    return Resource.Success(resultResponse.data)
                }

            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }


    /**
     * Handle SendOTP (ForgotPassword) API response and return [CustomerDTO] model and Response status
     */
    private fun handleForgotPasswordResponse(response: Response<GenericResponseDto<CustomerDTO>>): Resource<CustomerDTO> {
        if (response.isSuccessful) {
            response.body()?.let { response ->

                if (response.success == true) {
                    val forgotPasswordResponse = response.data as CustomerDTO


                    return Resource.Success(forgotPasswordResponse)
                }

            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }


    /**
     * Handle ResetPassword (ForgotPassword) API response and return [SuccessDTO] model and Response status
     */
    private fun handleResponse(response: Response<SuccessDTO>): Resource<SuccessDTO> {
        if (response.isSuccessful) {
            response.body()?.let { response ->

                if (response.success == true)
                    return Resource.Success(response)
            }
        }

        return Resource.Error(getErrorResponse(response.errorBody()!!.string()))
    }


}