package com.nfc.tapmee.arch.providers.username

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nfc.tapmee.arch.repositories.username.UsernameRepository
import com.nfc.tapmee.arch.viewmodels.username.UsernameViewModel

class UsernameProviderFactory(
    val app: Application,
    private val usernameRepository: UsernameRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return UsernameViewModel(app, usernameRepository) as T
    }
}