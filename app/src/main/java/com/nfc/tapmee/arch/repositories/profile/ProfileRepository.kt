package com.nfc.tapmee.arch.repositories.profile

import com.nfc.tapmee.common.database.dao.CustomerDAO
import com.nfc.tapmee.common.models.GenericResponseDto
import com.nfc.tapmee.common.models.UpdateProfileResponseDTO
import com.nfc.tapmee.common.network.NetworkApiClient
import com.nfc.tapmee.common.webservices.*
import okhttp3.MultipartBody
import retrofit2.Response

class ProfileRepository(private val customerDao: CustomerDAO? = null) {

    suspend fun getCustomerInfo() = customerDao?.getCustomerDto()

    //Get Profile
    suspend fun getProfileResponse() =
        NetworkApiClient.getClientInstance().create(GetUserProfileService::class.java)
            .userProfile()

    //Get My Added Profiles
    suspend fun getMyProfilesResponse(
        type: String
    ) = NetworkApiClient.getClientInstance().create(MyProfilesService::class.java)
        .getMyProfiles(type = type)

    //Get My Added Profiles
    suspend fun getPublicProfileResponse(
        username: String
    ) = NetworkApiClient.getClientInstance().create(PublicProfileService::class.java)
        .getProfile(username = username)

    suspend fun getAllProfiles() =
        NetworkApiClient.getClientInstance().create(AllProfilesService::class.java)
            .getAllProfiles()


    //Update Profile
    suspend fun updateProfile(
        name: String,
        bio: String,
        gender: Int,
        isPublic: Int,
        dob: String
    ): Response<GenericResponseDto<UpdateProfileResponseDTO>> {

//        var genderValue = 0
//
//        genderValue = when (gender) {
//            "Male" -> 1
//            "Female" -> 2
//            "Prefer not to share" -> 3
//            else -> 0
//        }

        return NetworkApiClient.getClientInstance().create(UpdateProfileService::class.java)
            .updateProfile(
                name = name,
                bio = bio,
                gender = gender,
                dob = dob,
                isPublic = isPublic
            )
    }

    //Update Banner
    suspend fun updateProfileBanner(file: MultipartBody.Part) =
        NetworkApiClient.getClientInstance().create(UpdateProfileBanner::class.java)
            .uploadBanner(file = file)

    //Add Social Profile
    suspend fun addProfile(
        link: String,
        code: String,
        isBusiness: String
    ) = NetworkApiClient.getClientInstance().create(AddProfileService::class.java)
        .addProfile(
            profileLink = link,
            profileCode = code,
            businessProfile = isBusiness
        )


    //Update Social Profile
    suspend fun updateSocialProfile(
        link: String,
        profileID: String,
        isBusiness: String
    ) = NetworkApiClient.getClientInstance().create(UpdateSocialProfileService::class.java)
        .updateSocialProfile(
            profileLink = link,
            profileID = profileID,
            businessProfile = isBusiness
        )

    //Delete Social Profile
    suspend fun deleteSocialProfile(
        profileID: String
    ) = NetworkApiClient.getClientInstance().create(DeleteSocialProfileService::class.java)
        .deleteSocialProfile(
            profileID = profileID
        )

    //Get Contact Cards
    suspend fun getContactCard(isBusiness: String) =
        NetworkApiClient.getClientInstance().create(GetContactCardService::class.java)
            .contactCards(isBusiness = isBusiness)

    //Update Contact Cards
    suspend fun updateContactCard(
        profileIDs: String,
        isBusiness: String
    ) = NetworkApiClient.getClientInstance().create(UpdateContactCardService::class.java)
        .updateContactCard(
            profileIDs = profileIDs,
            isBusiness = isBusiness
        )
}