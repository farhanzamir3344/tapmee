package com.nfc.tapmee.arch.repositories.username
import com.nfc.tapmee.common.network.NetworkApiClient
import com.nfc.tapmee.common.webservices.UpdateUsernameService
import com.nfc.tapmee.common.webservices.VerifyUsernameService
class UsernameRepository {

    suspend fun getUsernameResponse(
        username: String
    ) = NetworkApiClient.getClientInstance().create(VerifyUsernameService::class.java)
        .verify(username = username)

    suspend fun getUpdateUsernameResponse(
        username: String
    ) = NetworkApiClient.getClientInstance().create(UpdateUsernameService::class.java)
        .updateUsername(username = username)
}