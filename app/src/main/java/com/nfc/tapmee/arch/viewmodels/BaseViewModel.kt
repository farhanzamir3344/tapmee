package com.nfc.tapmee.arch.viewmodels

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.provider.ContactsContract
import androidx.lifecycle.AndroidViewModel
import com.google.gson.GsonBuilder
import com.nfc.tapmee.common.models.ErrorDto
import com.nfc.tapmee.main.TapMeeApplication
import org.json.JSONObject

abstract class BaseViewModel(app: Application) : AndroidViewModel(app) {

    fun getErrorResponse(error: String? = ""): String {

        return try {
            val errorObj = JSONObject(error)
            val errorDto =
                GsonBuilder().create().fromJson(errorObj.toString(), ErrorDto::class.java)

            if (!errorDto.errors.isNullOrEmpty())
                errorDto.errors[0].error
            else
                errorDto.message
        } catch (ex: Exception) {
            ex.printStackTrace()
            "Unexpected error occurred while parsing response"
        }

    }

    fun getPublicErrorResponse(error: String? = ""): String {

        return try {
            val publicProfileResponse = JSONObject(error)
            if (publicProfileResponse.getString("message") == "This is personal profile.") {
                val data = publicProfileResponse.getJSONObject("data")
                data.getJSONObject("profile").toString()

//                GsonBuilder().create().fromJson(profile.toString(), Profile::class.java)
            } else {
                publicProfileResponse.getString("message")
            }


        } catch (ex: Exception) {
            ex.printStackTrace()
            "Unexpected error occurred while parsing response"
        }

    }

    fun hasInternetConnection(): Boolean {
        val connectivityManager = getApplication<TapMeeApplication>().getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val activeNetwork = connectivityManager.activeNetwork ?: return false
            val capabilities =
                connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
            return when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectivityManager.activeNetworkInfo?.run {
                return when (type) {
                    ConnectivityManager.TYPE_WIFI -> true
                    ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE -> true
                    ConnectivityManager.TYPE_ETHERNET -> true
                    else -> false
                }
            }
        }
        return false
    }
}