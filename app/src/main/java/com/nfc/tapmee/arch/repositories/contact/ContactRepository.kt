package com.nfc.tapmee.arch.repositories.contact

import com.nfc.tapmee.common.network.NetworkApiClient
import com.nfc.tapmee.common.webservices.AddNoteService
import com.nfc.tapmee.common.webservices.ContactsListService

class ContactRepository {

    suspend fun getContactsList() =
        NetworkApiClient.getClientInstance().create(ContactsListService::class.java)
            .getUserNotes()

    suspend fun addNotes(
        userId: Int,
        name: String,
        email: String,
        phoneNo: String,
        note: String
    ) = NetworkApiClient.getClientInstance().create(AddNoteService::class.java)
        .addNote(
            name = name,
            email = email,
            phoneNo = phoneNo,
            note = note,
            userId = userId

        )
}