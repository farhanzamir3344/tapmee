package com.nfc.tapmee.base.util

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.nfc.tapmee.common.utils.KeyboardOp


class FragmentUtil(_activity: AppCompatActivity?) {
    val TAG = "Fragment Util Debug"
    private var fm: FragmentManager? = null
    private var activity: AppCompatActivity? = null

    init {
        activity = _activity
        fm = activity!!.supportFragmentManager
    }

    fun gotoNextFragment(
        fragment: Fragment?,
        frameId: Int) {

        KeyboardOp.hide(activity as Activity)

        val ft = fm!!.beginTransaction()
        ft.replace(frameId, fragment!!)
        ft.addToBackStack(null)
        ft.commitAllowingStateLoss()
    }



    fun goBackFragment() {
        goBackFragment(1)
    }

    fun goBackFragment(count: Int) {

        KeyboardOp.hide(activity as Activity)


        if (getBackstackCount() < count) {
            activity!!.finish()
            return
        }
        // if count greater than zero popbackstack
        try {
            if (count > 1) {
                for (i in 0 until count) {
                    fm!!.popBackStackImmediate()
                }
            } else {
                fm!!.popBackStackImmediate()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    fun getBackstackCount(): Int {
        return fm!!.backStackEntryCount
    }

    fun getCurrentFragment(frameId: Int): Fragment? {
        return try {
            activity!!.supportFragmentManager.findFragmentById(frameId)
        } catch (e: Exception) {
            null
        }
    }


    fun removeAllFragments() {
        try {
            fm!!.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        } catch (e: Exception) {
        }
    }

    fun removeCurrentFragment() {
        try {
            fm!!.popBackStack()
        } catch (e: Exception) {
        }
    }



    fun replaceBaseFragment(
        fragment: Fragment?,
        frameId: Int) {

        if (getBackstackCount() > 0)
            removeAllFragments()

        val ft = fm!!.beginTransaction()
        ft.replace(frameId, fragment!!)
        ft.commitAllowingStateLoss()
    }


}