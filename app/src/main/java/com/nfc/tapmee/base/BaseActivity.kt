package com.nfc.tapmee.base

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.nfc.tapmee.common.webview.TITLE
import com.nfc.tapmee.common.webview.WEB_URL
import com.nfc.tapmee.common.webview.WebActivity

open class BaseActivity : AppCompatActivity() {

    fun gotoWeb(url: String, title: String) {
        startActivity(
            Intent(
                this,
                WebActivity::class.java
            ).putExtra(WEB_URL, url).putExtra(
                TITLE, title
            )
        )
    }
}