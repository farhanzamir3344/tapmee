package com.nfc.tapmee.main

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import com.nfc.tapmee.common.preferences.SharedPreferences
class TapMeeApplication : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: TapMeeApplication? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }


    override fun onCreate() {
        super.onCreate()
        init()
    }

    private fun init() {

        // Use ApplicationContext.
        // example: SharedPreferences etc...
        val context: Context = applicationContext()

        //Forcefully disabled Dark mode
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        SharedPreferences.initialize(context)

    }
}