package com.nfc.tapmee.main

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Animation
import android.view.animation.AnimationSet
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.nfc.tapmee.R
import com.nfc.tapmee.base.BaseActivity
import com.nfc.tapmee.common.preferences.PreferenceUtils
import com.nfc.tapmee.common.utils.AnimationUtil
import com.nfc.tapmee.common.utils.AnimationUtil.createAlphaAnim
import com.nfc.tapmee.common.utils.AnimationUtil.createScaleAnim
import com.nfc.tapmee.common.utils.AnimationUtil.createTranslateAnim
import com.nfc.tapmee.common.utils.AnimationUtil.getAnimSet
import com.nfc.tapmee.common.utils.Constants
import com.nfc.tapmee.ui.activities.DashboardActivity
import com.nfc.tapmee.ui.activities.LoginAndRegisterActivity
import com.nfc.tapmee.ui.activities.UsernameActivity

class SplashActivity : BaseActivity() {

    private val ltWelcome: ConstraintLayout by lazy { findViewById<ConstraintLayout>(
        R.id.lt_welcome
    ) }
    private val bntProfile: Button by lazy { findViewById<Button>(R.id.btn_profile) }
    private val bntOrder: TextView by lazy { findViewById<Button>(R.id.btn_share) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({ init() }, 100)
    }

    private fun init() {
        bntProfile.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    LoginAndRegisterActivity::class.java
                )
            )
            finish()
        }

        bntOrder.setOnClickListener {
            gotoWeb(Constants.BUY_NFC, getString(R.string.lbl_heading_buy_nfc))
        }

        animateRingAndLogoViews().setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
                // do nothing
            }

            override fun onAnimationEnd(animation: Animation?) {
                when {
                    PreferenceUtils.isLoginCustomer() -> {
                        startActivity(
                            Intent(
                                this@SplashActivity,
                                DashboardActivity::class.java
                            )
                        )
                        finish()
                    }
                    PreferenceUtils.isRegisterCustomer() -> {
                        startActivity(
                            Intent(
                                this@SplashActivity,
                                UsernameActivity::class.java
                            )
                        )
                        finish()
                    }
                    else -> {
                        makeMessageAndButtonsVisible()
                        animateMessageAndButtons()
                    }
                }

            }

            override fun onAnimationStart(animation: Animation?) {
                // do nothing
            }
        })
    }


    // Animations
    private fun makeMessageAndButtonsVisible() {
        ltWelcome.visibility = View.VISIBLE
        bntProfile.visibility = View.VISIBLE
        bntOrder.visibility = View.VISIBLE
    }

    private fun animateMessageAndButtons() {
        animateViewsRTL(ltWelcome, bntProfile)
        animateViewLTR(bntOrder)
    }

    private fun animateViewsRTL(ltWelcome: ViewGroup, bntProfile: View) {
        val ltWelcomeAnimTranslate =
            createTranslateAnim(ltWelcome.width.toFloat() + ltWelcome.left, 0f, 0f, 0f, 800, 0)
        ltWelcomeAnimTranslate.interpolator = AccelerateDecelerateInterpolator()

        val texViewProfileAnim =
            createTranslateAnim(ltWelcome.width.toFloat() + ltWelcome.left, 0f, 0f, 0f, 800, 300)
        texViewProfileAnim.interpolator = AccelerateDecelerateInterpolator()
        bntProfile.startAnimation(texViewProfileAnim)

        ltWelcome.startAnimation(ltWelcomeAnimTranslate)
    }

    private fun animateViewLTR(tvBuy: View) {

        val tvBuyProfileAnimTranslate =
            createTranslateAnim(-(tvBuy.width.toFloat() + tvBuy.left), 0f, 0f, 0f, 1300, 0)
        tvBuyProfileAnimTranslate.interpolator = AccelerateDecelerateInterpolator()
        tvBuy.startAnimation(tvBuyProfileAnimTranslate)
    }

    private fun animateRingAndLogoViews(): AnimationSet {
        val logoView = findViewById<ImageView>(R.id.imv_logo)
//        val deltaY = getDeltaY(logoView)

        val view2AnimScale1 = createScaleAnim(1f, 0.8f, 1f, 0.8f, 0, 0)
        val view2AnimScale2 = createScaleAnim(0.8f, 1.1f, 0.4f, 1.1f, 800, 100)
        val view2AnimScale3 = createScaleAnim(1.1f, 1f, 1.1f, 1f, 800, 1000)
//        val view2AnimTranslate = createTranslateAnim(0f, 0f, 0f, deltaY, 1000, 1000)

        val view2AnimSet = getAnimSet(this)
        view2AnimSet.addAnimation(view2AnimScale1)
        view2AnimSet.addAnimation(view2AnimScale2)
        view2AnimSet.addAnimation(view2AnimScale3)
//        view2AnimSet.addAnimation(view2AnimTranslate)

        view2AnimSet.fillAfter = true
        view2AnimSet.interpolator = AccelerateDecelerateInterpolator()
        logoView.visibility = View.VISIBLE
        logoView.startAnimation(view2AnimSet)
        return view2AnimSet
    }


    // get view Dimensions
    private fun getDeltaY(v1: View): Float {
        val v1Dimensions = AnimationUtil.getDimension(v1)
        val heightV1 = v1Dimensions.top
        val heightV2 = 350
        var deltaY = (heightV2 - heightV1).toFloat()
        deltaY -= heightV2 / 2.28f
        return deltaY
    }


}