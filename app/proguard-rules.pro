-keepparameternames
#-renamesourcefileattribute SourceFile
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable,EnclosingMethod
-printmapping mappings.map
 #-dontobfuscate

# Preserve all annotations.
-keepattributes *Annotation*

# Preserve all public classes, and their public and protected fields and methods.
#-keep public class * {
#    public protected *;
#}

# Preserve all .class method names.
-keepclassmembernames class * {
 java.lang.Class class$(java.lang.String);
 java.lang.Class class$(java.lang.String, boolean);
}

# Preserve all native method names and the names of their classes.
 -keepclasseswithmembernames class * {
    native <methods>;
 }

# Preserve the special static methods that are required in all enumeration classes.
 -keepclassmembers class * extends java.lang.Enum {
 <fields>;
     public static **[] values();
     public static ** valueOf(java.lang.String);
 }


# Explicitly preserve all serialization members. The Serializable interface
# is only a marker interface, so it wouldn't save them.
# You can comment this out if your library doesn't use serialization.
# If your code contains serializable classes that have to be backward
# compatible, please refer to the manual.
 -keepclassmembers class * implements java.io.Serializable {
     static final long serialVersionUID;
     static final java.io.ObjectStreamField[] serialPersistentFields;
     private void writeObject(java.io.ObjectOutputStream);
     private void readObject(java.io.ObjectInputStream);
   java.lang.Object writeReplace();
     java.lang.Object readResolve();
 }

#Retrofit2
-keepclassmembers,allowshrinking,allowobfuscation interface * { @retrofit2.http.** <methods>; }

#okHttp
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-dontwarn com.squareup.okhttp.**
-dontwarn okio.**

#okHttp3
-keepattributes Signature
-keepattributes *Annotation*
-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**

#Room
-keep class * extends androidx.room.RoomDatabase
-dontwarn androidx.room.paging.**


#GLIDE
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep class * extends com.bumptech.glide.module.AppGlideModule {
 <init>(...);
}
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
-keep class com.bumptech.glide.load.data.ParcelFileDescriptorRewinder$InternalRewinder {
  *** rewind();
}



#Models
-keep class com.nfc.tapmee.common.models.** { <fields>; }
-keep class com.nfc.tapmee.common.webservices.** { <fields>; }